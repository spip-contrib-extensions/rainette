<?php
/**
 * Ce fichier contient les fonctions internes destinées à standardiser les données météorologiques.
 *
 * @package SPIP\RAINETTE\MASHUP
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_RAINETTE_REGEXP_LIEU_IP')) {
	/**
	 * Regexp permettant de reconnaître un lieu au format adresse IP.
	 */
	define('_RAINETTE_REGEXP_LIEU_IP', '#(?:\d{1,3}\.){3}\d{1,3}#');
}
if (!defined('_RAINETTE_REGEXP_LIEU_COORDONNEES')) {
	/**
	 * Regexp permettant de reconnaître un lieu au format coordonnées géographiques latitude,longitude.
	 */
	define('_RAINETTE_REGEXP_LIEU_COORDONNEES', '#([\-\+]?\d+(?:\.\d+)?)\s*,\s*([\-\+]?\d+(?:\.\d+)?)#');
}
if (!defined('_RAINETTE_REGEXP_LIEU_WEATHER_ID')) {
	/**
	 * Regexp permettant de reconnaître un lieu au format Weather ID.
	 */
	define('_RAINETTE_REGEXP_LIEU_WEATHER_ID', '#[a-zA-Z]{4}\d{4}#i');
}
if (!defined('_RAINETTE_REGEXP_LIEU_INSEE_ID')) {
	/**
	 * Regexp permettant de reconnaître un lieu au format Weather ID.
	 */
	define('_RAINETTE_REGEXP_LIEU_INSEE_ID', '#[0-9ABab]{5}#i');
}
if (!defined('_RAINETTE_REGEXP_LIEU_PWSID')) {
	/**
	 * Regexp permettant de reconnaître un lieu au format coordonnées géographiques latitude,longitude.
	 */
	define('_RAINETTE_REGEXP_LIEU_PWSID', '#PWS_[\w]+#');
}

/**
 * Normalise les données issues du service dans un tableau standard aux index prédéfinis pour chaque mode.
 *
 * @param array    $flux                  Le tableau brut des données météorologiques issu de l'appel au service.
 * @param string   $langue                Code de langue spip indiquant langue choisie pour afficher les données météo.
 *                                        Suivant le service il est possible que cette langue ne soit pas disponible nativement.
 * @param array    $configuration_mode    Configuration technique des données pour le mode concerné
 * @param array    $configuration_service Configuration statique et utilisateur du service ayant retourné le flux de données.
 * @param null|int $periode               Valeur de 0 à n pour indiquer qu'on traite les données de prévisions d'une période horaire donnée
 *                                        ou -1 pour indiquer que l'on traite les données jour. La valeur maximale n dépend de la périodicité
 *                                        des prévisions, par exemple, elle vaut 0 pour une périodicité de 24h, 1 pour 12h...
 *
 * @return array Le tableau standardisé des données météorologiques du service pour la période spécifiée.
 */
function meteo_normaliser(array $flux, string $langue, array $configuration_mode, array $configuration_service, ?int $periode = -1) : array {
	$tableau = [];

	include_spip('inc/filtres');
	if ($flux) {
		// Le service a renvoyé des données, on boucle sur les clés du tableau normalisé
		// Néanmoins, en fonction de la période fournie en argument on filtre les données uniquement
		// utiles à cette période:
		// - si période = -1 on traite les données jour
		// - si période > -1 on traite les données heure
		foreach (array_keys($configuration_mode) as $_donnee) {
			if (
				(
					($periode === -1)
					and (
						empty($configuration_mode[$_donnee]['rangement'])
						or ($configuration_mode[$_donnee]['rangement'] === 'jour')
					)
				)
				or (
					($periode > -1)
					and ($configuration_mode[$_donnee]['rangement'] === 'heure')
				)
			) {
				if ($cle_service = $configuration_service['donnees'][$_donnee]['cle']) {
					// La donnée est normalement fournie par le service car elle possède une configuration de clé
					// On traite le cas où le nom de la clé varie suivant le système d'unité choisi ou la langue.
					// La clé de base peut être vide, le suffixe contenant dès lors toute la clé.
					if (!empty($configuration_service['donnees'][$_donnee]['suffixe_unite'])) {
						$systeme_unite = $configuration_service['unite'];
						$id_suffixee = $configuration_service['donnees'][$_donnee]['suffixe_unite']['id_cle'];
						$cle_service[$id_suffixee] .= $configuration_service['donnees'][$_donnee]['suffixe_unite'][$systeme_unite];
					} elseif (!empty($configuration_service['donnees'][$_donnee]['suffixe_langue'])) {
						$langue_service = langue_service_determiner($langue, $configuration_service);
						$id_suffixee = $configuration_service['donnees'][$_donnee]['suffixe_langue']['id_cle'];
						$cle_service[$id_suffixee] .= $langue_service;
					}

					// On utilise donc la clé pour calculer la valeur du service.
					// Si la valeur est disponible on la stocke sinon on met la donnée à null pour
					// montrer l'indisponibilité temporaire.
					$donnee = null;
					$valeur_service = empty($cle_service)
						? $flux
						: table_valeur($flux, implode('/', $cle_service));
					if ($valeur_service !== '') {
						$typer = donnee_typer($configuration_mode[$_donnee]['type_php']);
						$valeur_typee = $typer ? $typer($valeur_service) : $valeur_service;

						// Vérification de la donnée en cours de traitement si une fonction idoine existe
						$verifier = "donnee_verifier_{$_donnee}";
						if (
							!function_exists($verifier)
							or (
								function_exists($verifier)
								and $verifier($valeur_typee)
							)
						) {
							$donnee = $valeur_typee;
						}
					}
				} elseif (!empty($configuration_service['donnees'][$_donnee]['calcul'])) {
					// La données météo est calculée à posteriori à partir d'une donnée fournie par le service
					// On l'initialise temporairement à null.
					$donnee = null;
				} else {
					// La donnée météo n'est jamais ni fournie par le service, ni calculée. On la positionne à vide pour
					// la distinguer avec une donnée null qui indique une indisponibilité temporaire.
					$donnee = '';
				}

				// Stockage de la donnée formatée
				$tableau[$_donnee] = $donnee;
			}
		}
	}

	return $tableau;
}

/**
 * Détermine, en fonction du type PHP configuré, la fonction à appliquer à la valeur d'une donnée.
 *
 * @param string $type_php Le type PHP ou chaine vide sinon
 *
 * @return string La fonction PHP (floatval, intval...) ou spécifique à appliquer à la valeur de la donnée.
 */
function donnee_typer(string $type_php) : string {
	// Si aucun type PHP précisé on renvoie aucune fonction à appliquer.
	$fonction = '';

	if ($type_php) {
		// Détermination de la fonction à appliquer
		switch ($type_php) {
			case 'float':
				$fonction = 'floatval';
				break;
			case 'int':
				$fonction = 'intval';
				break;
			case 'string':
				$fonction = 'strval';
				break;
			case 'int_string':
				$fonction = 'donnee_formater_int_string';
				break;
			case 'date':
				$fonction = 'donnee_formater_date';
				break;
			case 'heure':
				$fonction = 'donnee_formater_heure';
				break;
			case 'mixed':
				$fonction = '';
				break;
			default:
				$fonction = '';
		}
	}

	return $fonction;
}

/**
 * Formate une donnée qui peut soit être une chaine soit un nombre.
 * C'est le cas du code méteo.
 *
 * @param string $donnee Correspond à un index du tableau associatif standardisé à formater en date standard.
 *
 * @return int|string Donnée nombre ou chaine.
 */
function donnee_formater_int_string($donnee) {
	if (is_numeric($donnee)) {
		$donnee = (int) $donnee;
	} else {
		$donnee = (string) $donnee;
	}

	return $donnee;
}

/**
 * Formate une date numérique ou sous une autre forme en une date au format `Y-m-d H:i:s`.
 *
 * @param string $donnee Correspond à un index du tableau associatif standardisé à formater en date standard.
 *
 * @return string Date au format `Y-m-d H:i:s`.
 */
function donnee_formater_date(string $donnee) : string {
	// Si le format n'est pas connu, on renvoie la donnée sans traitement
	$date = $donnee;

	if (is_numeric($donnee)) {
		// on est en présence d'un timestamp
		$date = date('Y-m-d H:i:s', $donnee);
	}

	return $date;
}

/**
 * Formate une heure numérique ou sous une autre forme en une heure au format `H:i`.
 *
 * @param string $donnee Correspond à un index du tableau associatif standardisé à formater en heure standard.
 *
 * @return string Heure au format `H:i`.
 */
function donnee_formater_heure(string $donnee) : string {
	// Si le format n'est pas connu, on renvoie la donnée sans traitement
	$heure = $donnee;

	if (is_numeric($donnee)) {
		$taille = strlen($donnee);
		if ($taille < 3) {
			// On a un nombre à 1 ou 2 chiffres : on considère que c'est un nombre d'heures
			$m = '00';
			$h = $donnee;
			$heure = "{$h}:{$m}";
		} elseif ($taille < 5) {
			// On a un nombre à 3 ou 4 chiffres : on considère que c'est heure et minutes
			$m = substr($donnee, -2);
			$h = strlen($donnee) === 3
				? substr($donnee, 0, 1)
				: substr($donnee, 0, 2);
			$heure = "{$h}:{$m}";
		} else {
			// On a un nombre de plus de 5 caractères : on considère que c'est un timestamp
			$heure = date('H:i', $donnee);
		}
	} elseif (is_string($donnee)) {
		// On considère que c'est une date soit sous la forme hh:mm:ss soit un date complète
		if (strlen($donnee) < 9) {
			// Heure sans date
			$heure = substr($donnee, 0, 5);
		} else {
			// C'est une date complète, on la formate comme une heure H:i
			$heure = date('H:i', strtotime($donnee));
		}
	}

	return $heure;
}

/**
 * Vérifie que la valeur de l'indice UV est acceptable.
 *
 * @param float $donnee Valeur de l'indice UV à vérifier. Un indice UV est toujours compris entre 0 et 16, bornes comprises.
 *
 * @return bool `true` si la valeur est acceptable, `false` sinon.
 */
function donnee_verifier_indice_uv(float $donnee) : bool {
	$est_valide = true;
	if (($donnee < 0) or ($donnee > 16)) {
		$est_valide = false;
	}

	return $est_valide;
}

/**
 * Complète et normalise la partie additionnelle du bloc d'erreur.
 *
 * @param array      $erreur        Bloc d'erreur brut fourni par la fonction de chargement
 * @param string     $lieu_id       L'id normalisé du lieu concerné par la méteo exprimé selon les critères requis par le service.
 * @param string     $mode          Le type de données météorologiques demandé.
 * @param string     $service       Le nom abrégé du service.
 * @param int        $periodicite   La périodicité horaire des prévisions uniquement (0 si le mode est différent).
 * @param null|array $configuration Configuration du service
 *
 * @return array Bloc d'erreur complété
 */
function erreur_normaliser_extras($erreur, $lieu_id, $mode, $service, $periodicite, $configuration = []) {
	$extras = [
		'credits' => [],
		'config'  => [
			'nom_service' => $service
		],
		'lieu'              => $lieu_id,
		'mode'              => $mode,
		'periodicite_cache' => $periodicite,
		'service'           => $service,
		'erreur'            => $erreur
	];

	// On complète les informations extras uniquement si le service est connu, actif ou non.
	if ($erreur['type'] !== 'service_inconnu') {
		if (!$configuration) {
			// Acquérir la configuration statique du service (periode, format, données...)
			$configuration = configuration_service_lire($service, $mode);
		}

		// On prépare un contexte extras pour traiter les erreurs du modèle de façon standard comme celles
		// renvoyée par le chargement des données.
		$extras['credits'] = $configuration['credits'];
		$extras['config'] = array_merge(
			parametrage_service_normaliser($service, $configuration['defauts']),
			configuration_donnees_normaliser($mode, $configuration['donnees']),
			['nom_service' => $configuration['nom']]
		);

		// On enlève de ces extras les informations d'inscription
		unset($extras['config']['inscription']);
		if (isset($extras['config']['secret'])) {
			unset($extras['config']['secret']);
		}
	}

	return $extras;
}

/**
 * Formate le texte d'erreur affiché dans la boite d'alerte.
 *
 * @param array  $erreur      Bloc d'erreur brut fourni par la fonction de chargement
 * @param string $lieu_id     L'id normalisé du lieu concerné par la méteo exprimé selon les critères requis par le service.
 * @param string $mode        Le type de données météorologiques demandé.
 * @param string $service     Le nom abrégé du service.
 * @param string $modele      Le modèle utilisé pour afficher la météo.
 * @param string $nom_service Nom littéral du service
 *
 * @return array Texte de l'erreur pour affichage sous la forme d'un tableau à 3 index, `service`, `principal` et `conseil`.
 */
function erreur_formater_texte(array $erreur, string $lieu_id, string $mode, string $service, string $modele, string $nom_service) : array {
	$texte = ['principal' => '', 'conseil' => '', 'service' => ''];

	$type_erreur = $erreur['type'];
	switch ($type_erreur) {
		// Cas d'erreur lors du traitement de la requête par le plugin
		case 'url_indisponible':
		case 'analyse_xml':
		case 'analyse_json':
			// Cas d'erreur où le service renvoie aucune donnée sans pour autant monter une erreur.
		case 'aucune_donnee':
			// Cas d'erreur où le nombre de requêtes maximal a été atteint.
			$texte['principal'] .= _T("rainette:erreur_{$type_erreur}", ['service' => $nom_service]);
			$texte['conseil'] .= _T('rainette:erreur_conseil_equipe');
			break;
			// Cas d'erreur renvoyé par le service lui-même
		case 'reponse_service':
			if (!empty($erreur['service']['code'])) {
				$texte['service'] .= $erreur['service']['code'];
			}
			if (!empty($erreur['service']['message'])) {
				$texte['service'] .= ($texte['service'] ? ' - ' : '') . $erreur['service']['message'];
			}
			$texte['principal'] .= _T("rainette:erreur_{$type_erreur}_{$mode}", ['service' => $nom_service, 'lieu' => $lieu_id]);
			$texte['conseil'] .= _T('rainette:erreur_conseil_service');
			break;
			// Cas d'erreur où le nombre de requêtes maximal a été atteint.
		case 'limite_service':
			$texte['principal'] .= _T("rainette:erreur_{$type_erreur}", ['service' => $nom_service]);
			$texte['conseil'] .= _T('rainette:erreur_conseil_limite');
			break;
			// Cas d'erreur du à une mauvaise utilisation des modèles
		case 'modele_periodicite':
			$texte['principal'] .= _T("rainette:erreur_{$type_erreur}", ['modele' => $modele]);
			$texte['conseil'] .= _T('rainette:erreur_conseil_periodicite');
			break;
		case 'modele_service':
			$texte['principal'] .= _T("rainette:erreur_{$type_erreur}", ['modele' => $modele, 'service' => $nom_service]);
			$texte['conseil'] .= _T('rainette:erreur_conseil_modele_changer');
			break;
		case 'modele_inutilisable':
			$texte['principal'] .= _T("rainette:erreur_{$type_erreur}", ['modele' => $modele]);
			$texte['conseil'] .= _T('rainette:erreur_conseil_modele_expliciter');
			break;
			// Cas d'erreur du à un service inactif ou inconnu
		case 'service_inactif':
			$texte['principal'] .= _T("rainette:erreur_{$type_erreur}", ['service' => $nom_service]);
			$texte['conseil'] .= _T('rainette:erreur_conseil_service_changer');
			break;
		case 'service_inconnu':
			$texte['principal'] .= _T("rainette:erreur_{$type_erreur}", ['service' => $service]);
			$texte['conseil'] .= _T('rainette:erreur_conseil_service_verifier');
			break;
	}

	return $texte;
}

/**
 * Détermine la périodicité par défaut d'un type de modèle pour un service donné.
 *
 * @param string $type_modele Type de modèle.
 * @param string $service     Le nom abrégé du service.
 *
 * @return int Périodicité calculée.
 */
function periodicite_determiner(string $type_modele, string $service) : int {
	// Périodicité initialisée à "non trouvée"
	$periodicite = 0;

	// Lecture de la configuration technique des compatibilités type de modèle / périodicités
	include_spip('inc/config');
	$periodicites_modele = lire_config("rainette_config/modele_periodicites/{$type_modele}", []);

	if ($periodicites_modele) {
		// Acquérir la configuration statique du service pour connaître les périodicités horaires supportées
		// pour le mode prévisions.
		$periodicites_service = lire_config("rainette_{$service}/previsions/periodicites", []);

		// Déterminer la première périodicité compatible
		foreach ($periodicites_modele as $_periodicite_modele) {
			if (array_key_exists($_periodicite_modele, $periodicites_service)) {
				$periodicite = $_periodicite_modele;
				break;
			}
		}
	}

	return $periodicite;
}

/**
 * Vérifie, pour un type de modèle, si la périodicité proposée est compatible.
 *
 * @param string $type_modele Type de modèle.
 * @param int    $periodicite La périodicité horaire des prévisions uniquement (0 si le ode est différent).
 *
 * @return bool `true` si compatible, `false` sinon.
 */
function periodicite_est_compatible(string $type_modele, int $periodicite) : bool {
	// Initialisation de la compatibilité à "non compatible".
	$compatible = false;

	// Lecture de la configuration technique des compatibilités type de modèle / périodicités
	include_spip('inc/config');
	$periodicites_modele = lire_config("rainette_config/modele_periodicites/{$type_modele}", []);

	if (in_array($periodicite, $periodicites_modele)) {
		$compatible = true;
	}

	return $compatible;
}

/**
 * Construit le tableau du cache en fonction du service, du lieu, du type de données, de la langue utilisée par le site
 * et de l'unité des données.
 *
 * @param string $lieu_id               Id normalisé du lieu pour lequel on requiert le nom du cache.
 * @param string $mode                  Type de données météorologiques. Les valeurs possibles sont `infos`, `conditions` ou `previsions`.
 * @param int    $periodicite           La périodicité horaire des prévisions :
 *                                      - `24`, `12`, `6`, `3` ou `1`, pour le mode `previsions`
 *                                      - `0`, pour les modes `conditions` et `infos`
 * @param string $langue                Code de langue spip représentant la langue dans laquelle le résumé calculé sera fournit.
 * @param array  $configuration_service Configuration complète du service, statique et utilisateur.
 *
 * @return array Chemin complet du fichier cache.
 */
function cache_normaliser(string $lieu_id, string $mode, int $periodicite, string $langue, array $configuration_service) : array {
	// Identification de la langue du resume.
	$cache = [];

	// Le cache est stocké dans un sous-dossier au nom du service
	$cache['sous_dossier'] = $configuration_service['alias'];

	// Composants obligatoires
	// -- Le nom du lieu normalisé (sans espace et dont tous les caractères non alphanumériques sont remplacés par un tiret)
	$cache['lieu'] = str_replace([' ', ',', '+', '.', '/'], '-', $lieu_id);

	// -- Le nom du mode (infos, conditions ou previsions) accolé à la périodicité du cache pour les prévisions uniquement
	$cache['donnees'] = $mode . ($periodicite ? (string) $periodicite : '');

	// -- Identification de la langue du resume.
	$cache['langage'] = strtolower($langue);

	// Composants facultatifs
	// -- Unité des données si le mode n'est pas infos.
	if ($mode != 'infos') {
		$cache['unite'] = $configuration_service['unite'];
	}

	// La durée de conservation par défaut est positionné pour le mode infos. Pour les autres modes il faut
	// la positionner spécifiquement car chaque service a sa propre récurrence.
	if ($mode != 'infos') {
		$cache['conservation'] = $configuration_service['periode_maj'];
	}

	return $cache;
}

/**
 * Normalise la chaine représentant le lieu et renvoie son format d'expression (lat/lon, adresse IP, etc.).
 *
 * @param string $lieu Le lieu concerné par la méteo exprimé selon les critères requis par le service.
 *
 * @return array La chaine du lieu prête à être utilisée dans une requête et le format du lieu
 */
function lieu_normaliser(string $lieu) : array {
	$lieu_id_normalise = trim($lieu);

	if (preg_match(_RAINETTE_REGEXP_LIEU_WEATHER_ID, $lieu_id_normalise, $match)) {
		$format_lieu = 'weather_id';
		$lieu_id_normalise = $match[0];
	} elseif (preg_match(_RAINETTE_REGEXP_LIEU_COORDONNEES, $lieu_id_normalise, $match)) {
		$format_lieu = 'latitude_longitude';
		$lieu_id_normalise = "{$match[1]},{$match[2]}";
	} elseif (preg_match(_RAINETTE_REGEXP_LIEU_IP, $lieu_id_normalise, $match)) {
		$format_lieu = 'adresse_ip';
		$lieu_id_normalise = $match[0];
	} elseif (preg_match(_RAINETTE_REGEXP_LIEU_PWSID, $lieu_id_normalise, $match)) {
		$format_lieu = 'pws_id';
		$lieu_id_normalise = $match[0];
	} elseif ((int) $lieu_id_normalise) {
		$format_lieu = 'city_id';
	} elseif (preg_match(_RAINETTE_REGEXP_LIEU_INSEE_ID, $lieu_id_normalise, $match)) {
		$format_lieu = 'insee_id';
		$lieu_id_normalise = $match[0];
	} else {
		$format_lieu = 'ville_pays';
		// On détermine la ville et éventuellement le pays (ville[,pays])
		// et on élimine les espaces par un seul "+".
		$elements = explode(',', $lieu_id_normalise);
		$lieu_id_normalise = trim($elements[0]) . (!empty($elements[1]) ? ',' . trim($elements[1]) : '');
		$lieu_id_normalise = preg_replace('#\s+#', '+', $lieu_id_normalise);
	}

	return [
		'id'     => $lieu_id_normalise,
		'format' => $format_lieu
	];
}

/**
 * Vérifie que le format du lieu est bien supporté par l'API du service.
 *
 * @param string $format  Identifiant du format de lieu.
 * @param string $service Identifiant du service.
 *
 * @return bool `true` si la format est supporté, `false` sinon.
 */
function lieu_format_verifier_compatibilite(string $format, string $service) : bool {
	$est_compatible = false;

	// Acquérir la configuration statique du service et le paramétrage utilisateur.
	$configuration = configuration_service_lire($service, 'service');

	if (in_array($format, $configuration['lieu_formats'])) {
		$est_compatible = true;
	}

	return $est_compatible;
}

/**
 * Détermine la langue d'acquisition du résumé météorologique via l'API du service.
 *
 * Ce n'est pas forcément la langue d'affichage du résumé si le service ne fournit pas la traduction demandée.
 *
 * @param string $langue                Code de langue spip indiquant langue choisie pour afficher les données météo.
 *                                      Suivant le service il est possible que cette langue ne soit pas disponible nativement,
 *                                      la fonction se rabat alors sur un backup.
 * @param array  $configuration_service Configuration complète du service, statique et utilisateur.
 *
 * @return string Code de langue spip
 */
function langue_service_determiner(string $langue, array $configuration_service) : string {
	// On cherche d'abord si le service fournit nativement la langue demandée.
	// -- Pour cela on utilise la configuration du service qui fournit un tableau des langues disponibles
	//    sous le format [code de langue du service] = code de langue spip.
	$langue_service = array_search($langue, $configuration_service['langues']['disponibles']);

	if ($langue_service === false) {
		// Lecture de la configuration technique des langues alternatives.
		include_spip('inc/config');
		$langues_alternatives = lire_config('rainette_config/langues_alternatives', []);

		// La langue utilisée par SPIP n'est pas supportée par le service.
		// -- On cherche si il existe une langue SPIP utilisable meilleure que la langue par défaut du service.
		// -- Pour ce faire on a défini pour chaque code de langue spip, un ou deux codes de langue SPIP à utiliser
		//    en cas d'absence de la langue concernée dans un ordre de priorité (index 0, puis index 1).
		if ($langues_alternatives[$langue]) {
			foreach ($langues_alternatives[$langue] as $_langue_alternative) {
				$langue_service = array_search($_langue_alternative, $configuration_service['langues']['disponibles']);
				if ($langue_service !== false) {
					break;
				}
			}
		}
	}

	// Aucune langue ne correspond véritablement, on choisit donc la langue configurée par défaut.
	if ($langue_service === false) {
		$langue_service = $configuration_service['langues']['defaut'];
	}

	return $langue_service;
}

/**
 * Détermine la langue utilisée par spip, à savoir, soit celle de la page qui affiche la météo, soit celle du site.
 *
 * @return string Code de langue spip
 */
function langue_spip_determiner() : string {
	return !empty($GLOBALS['lang']) ? $GLOBALS['lang'] : $GLOBALS['spip_lang'];
}

/**
 * Collectionne les langues traduites par des modules de langues spip.
 *
 * Pour un service donné, le module de langue coincide avec l'identifiant du service.
 *
 * @param string $service Identifiant du service.
 *
 * @return array Liste des codes de langue spip
 */
function langue_spip_lister_modules($service) : array {
	// Initialiser la liste des codes.
	$langues = [];

	if ($modules = glob(_DIR_PLUGIN_RAINETTE . "lang/{$service}_*.php")) {
		foreach ($modules as $_module) {
			$nom_module = basename($_module, '.php');
			$langues[] = str_replace("{$service}_", '', $nom_module);
		}
	}

	return $langues;
}

/**
 * Renvoie pour un service, la configuration du bloc général du service et d'un éventuel autre bloc correspondant
 * en général à un mode météo ou toute la configuration.
 *
 * @param string      $service Le nom abrégé du service.
 * @param null|string $bloc    Index désignant le bloc de configuration à retourner.
 *                             Si infos, conditions, prévisions ou erreurs, on retourne ce bloc en plus du bloc service.
 *                             Si service, seul le bloc service est retourné.
 *                             Si vide ou incorrect toute la configuration est retournée.
 *
 * @return array Extrait de la configuration demandé.
 */
function configuration_service_lire(string $service, ?string $bloc = '') : array {
	// Lecture de la configuration du service.
	include_spip('inc/config');
	$configuration_service = lire_config("rainette_{$service}", []);

	// Calcul de la configuration à renvoyer en fonction du bloc passé en argument.
	if ($bloc === 'service') {
		$configuration = $configuration_service['service'] ?? [];
	} elseif (in_array($bloc, ['infos', 'conditions', 'previsions', 'erreurs'])) {
		$configuration = array_merge(
			$configuration_service['service'] ?? [],
			$configuration_service[$bloc] ?? []
		);
	} else {
		$configuration = $configuration_service;
	}

	// On renvoie le bloc du mode concerné et le bloc général du service.
	return $configuration;
}

/**
 * Normalise le bloc de configuration des données de service d'un mode donné.
 *
 * @param string $mode                  Le type de données météorologiques demandé.
 * @param array  $configuration_service Configuration de la source des données.
 *
 * @return array Bloc de configuration normalisé.
 */
function configuration_donnees_normaliser($mode, $configuration_service) {
	// On renvoie un tableau vide en cas d'erreur
	$configuration_normalisee = [
		'source_api'    => [],
		'source_calcul' => [],
		'source_none'   => [],
	];

	foreach ($configuration_service as $_donnee => $_configuration) {
		if (!empty($_configuration['cle'])) {
			// Donnée fournie par l'api du service
			$configuration_normalisee['source_api'][] = $_donnee;
		} elseif (!empty($_configuration['calcul'])) {
			// Donnée calculée à partir d'une donnée fournie par l'api du service
			$configuration_normalisee['source_calcul'][] = $_donnee;
		} else {
			// Donnée ni fournie par l'api du service ni calculée
			$configuration_normalisee['source_none'][] = $_donnee;
		}
	}

	return $configuration_normalisee;
}

/**
 * Renvoie pour un service, le paramétrage utilisateur ou vide si le service n'a pas encore été paramétré.
 *
 * @param string $service Le nom abrégé du service.
 *
 * @return array Paramétrage utilisateur d'un service.
 */
function parametrage_service_lire(string $service) : array {
	include_spip('inc/config');

	return lire_config("rainette/{$service}", []);
}

/**
 * Vérifie pour un service, si le paramétrage utilisateur est valide afin d'utiliser le service.
 *
 * @param string $service Le nom abrégé du service.
 *
 * @return bool `true` si valide, `false` sinon.
 */
function parametrage_service_est_valide(string $service) : bool {
	// Par défaut non valide
	$parametrage_est_valide = false;

	// Acquérir la configuration statique et le paramétrage utilisateur
	$configuration = configuration_service_lire($service, 'service');
	$parametrage = parametrage_service_lire($service);
	if (
		(
			($configuration['enregistrement']['taille_cle'] > 0)
			and !empty($parametrage['inscription'])
		)
		or ($configuration['enregistrement']['taille_cle'] === 0)
	) {
		if (
			(
				($configuration['enregistrement']['taille_secret'] > 0)
				and !empty($parametrage['secret'])
			)
			or ($configuration['enregistrement']['taille_secret'] === 0)
		) {
			$parametrage_est_valide = true;
		}
	}

	return $parametrage_est_valide;
}

/**
 * Renvoie la configuration utilisateur d'un service après l'avoir complétée par la configuration par défaut quand
 * cela est nécessaire.
 *
 * @param string $service              Le nom abrégé du service.
 * @param array  $configuration_defaut La configuration par défaut du service.
 *
 * @return array Configuration utilisateur d'un service normalisée.
 */
function parametrage_service_normaliser(string $service, array $configuration_defaut) : array {
	// On récupère le paramétrage utilisateur
	$configuration_utilisateur = parametrage_service_lire($service);

	// On complète la configuration avec des valeurs par défaut si nécessaire.
	foreach ($configuration_defaut as $_cle => $_valeur) {
		if (!isset($configuration_utilisateur[$_cle])) {
			$configuration_utilisateur[$_cle] = $_valeur;
		}
	}

	return $configuration_utilisateur;
}

/**
 * Construit le chemin complet d'un icone disponible localement dans un thème donné.
 *
 * @param string      $icone   nom de l'icone
 * @param null|string $service Le nom abrégé du service.
 * @param null|string $theme   Le nom d'un thème d'icones : sert aussi de sous-dossier
 *
 * @return string
 */
function icone_normaliser_chemin(string $icone, ?string $service = '', ?string $theme = '') {
	// On initialise le dossier de l'icone pour le service demandé.
	$chemin = 'themes';

	// Si on demande un thème il faut créer le sous-dossier.
	if ($service) {
		$chemin .= "/{$service}";
	}

	// Si on demande un thème il faut créer le sous-dossier.
	if ($theme) {
		$chemin .= "/{$theme}";
	}

	// On finalise le chemin complet avec le nom de l'icone sauf si on ne veut que le dossier.
	if ($icone) {
		$chemin .= "/{$icone}";
	}

	return $chemin;
}
