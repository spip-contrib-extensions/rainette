<?php
/**
 * Ce fichier contient les fonctions internes de gestion des requêtes vers les services météorologiques.
 *
 * @package SPIP\RAINETTE\REQUETE
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fait appel au service spécifié en utilisant l'URL fournie et retourne le flux brut JSON ou XML transcodé dans un
 * tableau. Chaque appel est comptabilisé et logé dans une meta.
 *
 * @param string $url           URL complète de la requête formatée en fonction de la demande et du service.
 * @param string $service       Alias du service.
 * @param array  $configuration Configuration statique et utilisateur du service nécessaire pour identifier les seuils
 *                              de requêtes par période propres au service et le format du flux pour le transcodage.
 *
 * @return array Tableau des données météorologiques retournées par le service ou tableau limité à l'index `erreur` en
 *               cas d'erreur de transcodage.
 *
 * @throws Exception
 */
function requeter(string $url, string $service, array $configuration) : array {
	// Acquisition des données spécifiées par l'url
	// - on demande le transcodage de la réponse
	// - on ajoute le format accepté par la réponse car certains services l'utilise à la place d'un paramètre d'URL
	include_spip('inc/distant');
	$flux = recuperer_url(
		$url,
		[
			'headers'    => ['Accept' => 'application/' . $configuration['format_flux']],
			'transcoder' => true
		]
	);

	// On loge la date de l'appel et on incrémente les compteurs de requêtes du service.
	include_spip('inc/config');
	$execution = lire_config('rainette_execution', []);
	$execution[$service]['dernier_appel'] = date('Y-m-d H:i:s');
	if (!isset($execution[$service]['compteurs'])) {
		// On initialise les compteurs de requêtes.
		$execution[$service]['compteurs'] = [];
	}
	// On met à jour tous les compteurs
	if ($configuration['offres']['limites']) {
		foreach ($configuration['offres']['limites'] as $_periode => $_seuil) {
			$execution[$service]['compteurs'][$_periode] = isset($execution[$service]['compteurs'][$_periode])
				? $execution[$service]['compteurs'][$_periode] + 1
				: 1;
		}
	}
	ecrire_config('rainette_execution', $execution);

	// Initialisation de la réponse et du bloc d'erreur normalisé.
	$reponse = [];
	if (
		!$flux
		or ($flux['status'] === 404)
		or empty($flux['page'])
	) {
		spip_log("URL indiponible : {$url}", 'rainette');
		$reponse['erreur'] = 'url_indisponible';
	} elseif ($configuration['format_flux'] === 'xml') {
		// Transformation de la chaîne xml reçue en tableau associatif
		$convertir = charger_fonction('simplexml_to_array', 'inc');

		// Pouvoir attraper les erreurs de simplexml_load_string().
		// http://stackoverflow.com/questions/17009045/how-do-i-handle-warning-simplexmlelement-construct/17012247#17012247
		set_error_handler(
			function ($erreur_id, $erreur_message, $erreur_fichier, $erreur_ligne) {
				throw new Exception($erreur_message, $erreur_id);
			}
		);

		try {
			$reponse = $convertir(simplexml_load_string($flux['page']), false);
			$reponse = $reponse['root'];
		} catch (Exception $erreur) {
			$reponse['erreur'] = 'analyse_xml';
			restore_error_handler();
			spip_log("Erreur d'analyse XML pour l'URL `{$url}` : " . $erreur->getMessage(), 'rainette' . _LOG_ERREUR);
		}
		restore_error_handler();
	} else {
		// Transformation de la chaîne json reçue en tableau associatif
		try {
			$reponse = json_decode($flux['page'], true);
		} catch (Exception $erreur) {
			$reponse['erreur'] = 'analyse_json';
			spip_log("Erreur d'analyse JSON pour l'URL `{$url}` : " . $erreur->getMessage(), 'rainette' . _LOG_ERREUR);
		}
	}

	return $reponse;
}

/**
 * Vérifie si la requête prévue peut être adressée au service sans excéder les limites d'utilisation fixées dans
 * les conditions d'utilisation du service.
 * Si une période est échue, la fonction remet à zéro le compteur associé.
 *
 * @param array  $limites Tableau des seuils de requêtes par période (année, mois,..., minute, seconde).
 * @param string $service Alias du service.
 *
 * @return bool `true` si la requête est autorisée, `false`sinon.
 */
function requete_autorisee(array $limites, string $service) : bool {
	// Initialisations
	// -- formats de date correspondant aux périodes possibles pour les limites d'utilisation
	static $formats = [
		'year'   => 'Y',
		'month'  => 'Y-m',
		'day'    => 'Y-m-d',
		'hour'   => 'Y-m-d H',
		'minute' => 'Y-m-d H:i',
		'second' => 'Y-m-d H:i:s',
	];
	// -- autorisation à vrai par défaut car les cas à faux sont moins nombreux.
	$autorisee = true;

	// Si aucune information d'exécution n'a été logée on considère que c'est la mise en route du service
	// et donc que la requête est forcément autorisée. De même si aucune limite n'est imposée.
	include_spip('inc/config');
	$execution = lire_config('rainette_execution', []);
	if (
		isset($execution[$service])
		and $limites
	) {
		// Timestamps de la dernière requête au service et de la date courante.
		$derniere_execution = strtotime($execution[$service]['dernier_appel']);
		$date_courante = time();
		$nouvelle_periode = false;
		foreach ($limites as $_periode => $_max) {
			if ($nouvelle_periode) {
				// On remet à zéro le compteur d'exécution des périodes inférieures à celle qui a détecté le changement.
				$execution[$service]['compteurs'][$_periode] = 0;
			} else {
				// On détermine la période correspondante pour le dernier appel et pour la date courante de façon
				// absolue afin de pouvoir les comparer avec une simple égalité.
				// Pour ce faire, on calcule une date avec un format extrayant la période sans les sous-périodes.
				$periode_execution = date($formats[$_periode], $derniere_execution);
				$periode_courante = date($formats[$_periode], $date_courante);
				if ($periode_courante === $periode_execution) {
					// La période est la même, il faut donc vérifier si on a atteint ou pas la limite autorisée du nombre
					// de requêtes pour cette période de temps.
					// Si la limite est atteinte alors on sort immédiatement avec un refus d'autorisation. Sinon, on
					// continue à tester les autres périodes.
					if ($_periode === 'second') {
						// Pour la période "même seconde" (second) plutôt que de vérifier si on atteint ou pas la limite, on
						// temporise le calcul 1s afin d'être sur que ce ne sera pas le cas et autorise donc la requête.
						// Il faut donc remettre le compteur à zéro.
						sleep(1);
						$nouvelle_periode = true;
						$execution[$service]['compteurs'][$_periode] = 0;
					} elseif ($execution[$service]['compteurs'][$_periode] === $_max) {
						$autorisee = false;
						break;
					}
				} else {
					// La période est différente (nouvelle période de compatibilisation), la requête est donc autorisée.
					// Le compteur de la période et de toutes les périodes inférieures doivent être remis à zéro.
					$nouvelle_periode = true;
					$execution[$service]['compteurs'][$_periode] = 0;
				}
			}
		}

		if ($nouvelle_periode) {
			ecrire_config('rainette_execution', $execution);
		}
	}

	return $autorisee;
}
