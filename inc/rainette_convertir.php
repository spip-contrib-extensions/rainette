<?php
/**
 * Ce fichier contient les fonctions de conversion entre unités, en particulier, les conversions du système métrique
 * vers le système impérial US.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_RAINETTE_ICONE_NOM_VIDE')) {
	/**
	 * Nom de l'icone vide par défaut.
	 */
	define('_RAINETTE_ICONE_NOM_VIDE', 'icone-vide');
}

if (!defined('_RAINETTE_ICONE_NOM_NA')) {
	/**
	 * Nom de l'icone n/a par défaut si le thème ne le fournit pas.
	 */
	define('_RAINETTE_ICONE_NOM_NA', 'na');
}

if (!defined('_RAINETTE_ICONE_EXTENSIONS')) {
	/**
	 * Liste des extensions possibles pour les icones dans l'ordre de préférence.
	 */
	define('_RAINETTE_ICONE_EXTENSIONS', ['png', 'svg']);
}

/**
 * Lister les 16 directions abrégées en anglais ainsi que la direction variable.
 *
 * @return array Liste des directions abrégées.
 */
function lister_directions() : array {
	static $liste_directions = [
		0  => 'N',
		1  => 'NNE',
		2  => 'NE',
		3  => 'ENE',
		4  => 'E',
		5  => 'ESE',
		6  => 'SE',
		7  => 'SSE',
		8  => 'S',
		9  => 'SSW',
		10 => 'SW',
		11 => 'WSW',
		12 => 'W',
		13 => 'WNW',
		14 => 'NW',
		15 => 'NNW',
		16 => 'N',
		17 => 'V'
	];

	return $liste_directions;
}

/**
 * Convertit un angle en degré en une direction sur 16 valeurs.
 *
 * @param null|int $angle Angle du vent exprimé en degrés.
 *
 * @return string Direction du vent en abrégée anglais standard selon 16 directions.
 */
function angle2direction(?int $angle = null) : string {
	static $liste_directions = [];
	$direction = '';

	if (is_int($angle)) {
		// Acquérir les directions une fois par hit
		if (!$liste_directions) {
			$liste_directions = lister_directions();
		}

		// Normaliser l'angle dans la plage de 0 à 360 degrés
		$angle_normalise = fmod(($angle + 360), 360);

		// Calculer l'index de la direction dans le tableau des directions et renvoyer la direction
		$index = (int) (($angle_normalise + 11.25) / 22.5) % 16;
		$direction = $liste_directions[$index];
	}

	return $direction;
}

/**
 * Convertit un indice UV normalisé en une chaine correspondant à un niveau de risque.
 *
 * @param null|float $indice_uv Entier représentant l'indice UV
 *
 * @return string Chaine représentant le risque lié à l'indice UV. Cette chaine permet de calculer
 *                l'item de langue du risque dans la langue requise.
 */
function indice2risque_uv(?float $indice_uv = null) : string {
	$risque_uv = '';
	if (is_float($indice_uv)) {
		if ($indice_uv >= 11) {
			$risque_uv = 'extreme';
		} elseif ($indice_uv >= 8) {
			$risque_uv = 'tres_eleve';
		} elseif ($indice_uv >= 6) {
			$risque_uv = 'eleve';
		} elseif ($indice_uv >= 3) {
			$risque_uv = 'modere';
		} elseif ($indice_uv >= 0) {
			$risque_uv = 'faible';
		}
	}

	return $risque_uv;
}

/**
 * Convertit des kilomètres en miles.
 *
 * @param null|float $kilometre La valeur réelle en kilomètres.
 *
 * @return null|float La valeur réelle correspondante convertie en miles
 */
function kilometre2mile(?float $kilometre = null) {
	return $kilometre !== null ? 0.621371 * $kilometre : null;
}

/**
 * Convertit des miles en kilomètres.
 *
 * @param null|float $miles La valeur réelle en miles.
 *
 * @return null|float La valeur réelle correspondante convertie en kilomètres
 */
function mile2kilometre(?float $miles = null) {
	return $miles !== null ? $miles / 0.621371 : null;
}

/**
 * Convertit des températures celsius en farenheit.
 *
 * @param null|int $celsius La valeur réelle en degrés celsius.
 *
 * @return null|float La valeur réelle correspondante convertie en farenheit.
 */
function celsius2farenheit(?int $celsius = null) {
	return $celsius !== null ? $celsius * 9 / 5 + 32 : null;
}

/**
 * Convertit des températures celsius en farenheit.
 *
 * @param null|int $farenheit La valeur réelle en degrés celsius.
 *
 * @return null|float La valeur réelle correspondante convertie en farenheit.
 */
function farenheit2celsius(?int $farenheit = null) {
	return $farenheit !== null ? ($farenheit - 32) * 5 / 9 : null;
}

/**
 * Convertit des millimètres en pouces.
 *
 * @param null|float $millimetre La valeur réelle en millimètres
 *
 * @return null|float La valeur réelle correspondante convertie en pouces.
 */
function millimetre2inch(?float $millimetre = null) {
	return $millimetre !== null ? $millimetre / 25.4 : null;
}

/**
 * Convertit des pressions millibar en pouces.
 *
 * @param null|float $millibar La valeur réelle en millibars
 *
 * @return null|float La valeur réelle correspondante convertie en pouces.
 */
function millibar2inch(?float $millibar = null) {
	return $millibar !== null ? $millibar / 33.86 : null;
}

/**
 * Calcule la température de rosée en fonction de la température ambiente et de l'humidité (en pourcentage).
 * On utilise la formule de Heinrich Gustav Magnus-Tetens.
 *
 * @param null|float  $temperature Température réelle mesurée en celsius.
 * @param null|float  $humidite    Pourcentage d'humidité.
 * @param null|string $unite       Système d'unité métrique (`m`, défaut) ou impérial (`s`)
 *
 * @return null|float La température du point de rosée.
 */
function temperature2pointrosee(?float $temperature = null, ?float $humidite = null, ?string $unite = 'm') {
	$point_rosee = null;

	// Vérifier les bornes de validité de la formule.
	if (
		is_float($temperature)
		and ($temperature > 0)
		and ($temperature < 60)
		and is_float($humidite)
		and $humidite > 1
		and ($humidite < 100)
	) {
		// On convertit en système métrique pour effectuer le calcul
		$temperature = ($unite === 'm') ? $temperature : farenheit2celsius($temperature);

		// Formule de calcul Heinrich Gustav Magnus-Tetens
		$alpha = (17.27 * $temperature) / (237.7 + $temperature) + log($humidite / 100);
		$point_rosee = (237.7 * $alpha) / (17.27 - $alpha);
		$point_rosee = round($point_rosee, 1);
	}

	return $point_rosee;
}

/**
 * Calcule la température ressentie par refroidissement éolien ou l'humidex.
 *
 * Le calcul par refroidissement éolien n'a de sens que pour des températures réelles supérieures à -50°C et inférieures
 * à 10°C. Au-delà de ces valeurs, la fonction renvoie l'indice humidex la température réelle fournie en entrée.
 * Les calculs nécessitent des données en système métrique.
 *
 * @param null|float  $temperature  Température réelle.
 * @param null|float  $vitesse_vent Vitesse du vent.
 * @param null|float  $point_rosee  Température de rosée.
 * @param null|string $unite        Système d'unité métrique (`m`, défaut) ou impérial (`s`)
 *
 * @return null|float 'Température ressentie' (indice) arrondie à la première décimale
 */
function temperature2ressenti(?float $temperature = null, ?float $vitesse_vent = null, ?float $point_rosee = null, ?string $unite = 'm') {
	// Initialisation de la température à null dans le cas où l'une des données d'entrée est null ou hors domaine
	$ressenti = null;

	if (is_float($temperature)) {
		// On convertit en système métrique pour effectuer le calcul
		$temperature = ($unite === 'm') ? $temperature : farenheit2celsius($temperature);

		if (
			($temperature >= -50)
			and ($temperature <= 10)
			and is_float($vitesse_vent)
		) {
			// Calcul du refroidissement éollien en système métrique
			$vitesse_vent = ($unite === 'm') ? $vitesse_vent : mile2kilometre($vitesse_vent);
			if ($vitesse_vent > 4.8) {
				$ressenti = 13.12 + 0.6215 * $temperature + (0.3965 * $temperature - 11.37) * pow($vitesse_vent, 0.16);
			} else {
				$ressenti = $temperature + 0.2 * (0.1345 * $temperature - 1.59) * $vitesse_vent;
			}
		} elseif (
			($temperature > 10)
			and is_float($point_rosee)
		) {
			$point_rosee = ($unite === 'm') ? $point_rosee : farenheit2celsius($point_rosee);
			$ressenti = $temperature + 0.5555 * (6.11 * exp(5417.7530 * ((1 / 273.16) - (1 / (273.16 + $point_rosee)))) - 10);
		}

		// Si le système impérial est demandé, on convertit la température
		if (null !== $ressenti) {
			if ($unite !== 'm') {
				$ressenti = celsius2farenheit($ressenti);
			}
			$ressenti = round($ressenti, 1);
		}
	}

	return $ressenti;
}

/**
 * Convertit en kilomètres une valeur en mètres.
 *
 * @param null|int $metre La valeur entière en mètres
 *
 * @return null|float La valeur correspondante convertie en kilomètres.
 */
function metre2kilometre(?int $metre = null) {
	return $metre !== null ? $metre / 1000 : null;
}

/**
 * Convertit en kilomètres par heure une valeur en mètres par seconde.
 *
 * @param null|float $vitesse_vent Vitesse du vent en mètres par seconde.
 *
 * @return null|float La valeur correspondante convertie en kilomètres par heure.
 */
function metre_seconde2kilometre_heure(?float $vitesse_vent = null) {
	return $vitesse_vent !== null ? ($vitesse_vent * 3600) / 1000 : null;
}

/**
 * Calcule le chemin de l'icone local à utiliser.
 * La fonction considère qu'au moment où elle est appelée, les données `code` et `periode` sont correctement remplis
 * par le service (nativement ou par complétion).
 *
 * @param int|string $code             Code méteo retourné par le service et servant d'index pour définir l'icone.
 * @param int        $periode          Indicateur jour (0) ou nuit (1) et utilisé si les icones différent suivant la période.
 * @param array      $parametres_icone Tableau de paramètres identifiant comment choisir l'icone :
 *                                     - service : le nom du service dont est issu le code.
 *                                     - transcodages : les tableaux de transcodages code / icone du service
 *                                     - theme_origine : le type de thème parmi 'local', 'weather', 'wmo' (index de transcodage)
 *                                     - theme_id : l'id du thème
 *                                     - periode_stricte : indique qu'on ne cherche que l'icone de la période (défaut false)
 *
 * @return string Le chemin de l'icone
 */
function code_meteo2icone($code, int $periode, array $parametres_icone) : string {
	// On initialise le nom de l'icone à na pour indiquer que le code météo n'est pas valide
	$nom_icone = _RAINETTE_ICONE_NOM_NA;

	// Extraire le type de thème utilisé par le service (local, weather ou wmo)
	$type_theme = $parametres_icone['theme_origine'];

	// Calcul de l'icone à afficher suivant le thème configuré si le code météo est valide
	$transcodages = $parametres_icone['transcodages'];
	if (in_array($code, array_keys($transcodages))) {
		// -- en déduire le nom de l'icone à afficher en utilisant le transcodage
		$nom_icone = $transcodages[$code][$type_theme][$periode];

		// Si le nom de l'icone est vide, c'est que contrairement à ce qui est prévu, le code météo est utilisé
		// par une période qui n'est pas licite : on se rabat sur l'autre période
		if (
			($nom_icone === '')
			and empty($parametres_icone['periode_stricte'])
		) {
			$periode_inverse = $periode === 1 ? 0 : 1;
			$nom_icone = $transcodages[$code][$type_theme][$periode_inverse];
		}
	}

	// Extensions possible pour les icones du thème
	$extensions = _RAINETTE_ICONE_EXTENSIONS;

	// A partir du nom de l'icone, détermine le fichier d'icone à afficher en traitant les cas d'erreur
	include_spip('inc/rainette_normaliser');
	include_spip('inc/utils');
	$fichier_icone = '';
	if ($nom_icone === '') {
		// il n'y a pas d'icone associé à ce code météo, c'est surement une erreur de configuration mais on affiche
		// un svg vide
		$chemin_icone = icone_normaliser_chemin(_RAINETTE_ICONE_NOM_VIDE . '.svg');
	} else {
		// On teste l'existence pour le thème avec les extensions possibles
		$service_icone = $type_theme === 'local' ? $parametres_icone['service'] : $type_theme;
		foreach ($extensions as $_extension) {
			$chemin_icone = icone_normaliser_chemin(
				"{$nom_icone}.{$_extension}",
				$service_icone,
				$parametres_icone['theme_id']
			);
			if ($fichier_icone = find_in_path($chemin_icone)) {
				break;
			}
		}

		if (!$fichier_icone) {
			// L'icone n'est pas fourni par le thème : on renvoie l'icone na par défaut ou l'icone vide
			$chemin_icone = $nom_icone === _RAINETTE_ICONE_NOM_NA
				? icone_normaliser_chemin("{$nom_icone}.svg")
				: icone_normaliser_chemin(_RAINETTE_ICONE_NOM_VIDE . '.svg');
		}
	}

	// Construction du chemin complet de l'icone et retour.
	return !$fichier_icone
		? (empty($parametres_icone['periode_stricte']) ? find_in_path($chemin_icone) : '')
		: $fichier_icone;
}
