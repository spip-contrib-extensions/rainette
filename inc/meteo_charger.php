<?php
/**
 * Ce fichier contient la fonction standard de chargement et fourniture des données météo.
 * Elle s'applique à tous les services et à tous les types de données.
 *
 * @package SPIP\RAINETTE\CACHE
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoyer le contenu du fichier cache des données météos correspondant au lieu et au type de données choisis
 * après l'avoir éventuellement mis à jour.
 *
 * Si le fichier cache est obsolète ou absent, on le crée après avoir chargé puis phrasé le flux XML ou JSON
 * et stocké les données collectées et transcodées dans un tableau standardisé. L'appelant doit s'assurer que la
 * périodicité est compatible avec le service; cette fonction ne retourne donc que des erreurs de chargement.
 *
 * @uses rainette_service_defaut()
 * @uses rainette_service_est_indisponible()
 * @uses erreur_normaliser_extras()
 * @uses configuration_service_lire()
 * @uses parametrage_service_normaliser()
 * @uses cache_normaliser()
 * @uses cache_est_valide()
 * @uses requete_autorisee()
 * @uses meteo_normaliser()
 * @uses indice2risque_uv()
 * @uses code_meteo2icone()
 * @uses cache_ecrire()
 * @uses cache_lire()
 *
 * @param string      $lieu    Le lieu concerné par la méteo exprimé selon les critères requis par le service mais non normalisé.
 * @param null|string $mode    Le type de données météorologiques demandé :
 *                             - `conditions`, la valeur par défaut
 *                             - `previsions`
 *                             - `infos`
 * @param null|string $service Le nom abrégé du service :
 *                             - `owm` pour OpenWeather, la valeur par défaut
 *                             - `weatherbit` pour Weatherbit.io
 *                             - `weatheapi` pour WeatherAPI
 *                             - `accuweather` pour AccuWeather
 *                             - `aeris` pour AerisWeather
 *                             - `openmeteo` pour OpenMeteo
 *                             - `wwo` pour World Weather Online
 *                             - `meteoconcept` pour Meteo-Concept
 * @param null|array  $options Options pour l'acquisition des données météo :
 *                             La périodicité horaire des prévisions (`periodicite`) :
 *                             - `24`, les prévisions de la journée (défaut pour le mode prévisions)
 *                             - `12`, les prévisions du jour et de la nuit
 *                             - `6`, les prévisions de la journée par période de 6h
 *                             - `3`, les prévisions de la journée par période de 3h
 *                             - `1`, les prévisions de la journée pour chaque heure
 *                             - `0`, pour les modes `conditions` et `infos`
 *                             La langue du résumé méteo (`langue`) : par défaut la langue spip de la page
 *                             L'indicateur du mode de lancement du chargement (`via_job`) :
 *                             Prend les valeurs 'on' pour indiquer que l'appel vient d'un job et '' sinon (défaut).
 *                             Cette information ne sert qu'au pipeline post_chargement_meteo
 *
 * @return array Le contenu du fichier cache avec les données demandées à jour.
 *
 * @throws Exception
 */
function inc_meteo_charger_dist(string $lieu, ?string $mode = 'conditions', ?string $service = '', ?array $options = []) : array {
	// Initialisation du tableau de sortie.
	$tableau = [];

	// Traitement des cas ou les arguments sont vides.
	include_spip('rainette_fonctions');
	include_spip('inc/rainette_normaliser');
	$mode = $mode ?: 'conditions';
	$service = $service ?: rainette_service_defaut();
	$periodicite = $options['periodicite'] ?? 0;
	if ($mode !== 'previsions') {
		$periodicite = 0;
	}
	$langue = !empty($options['langue']) ? $options['langue'] : langue_spip_determiner();
	$via_job = $options['via_job'] ?? '';

	// Normalisation du lieu
	// -> la fonction renvoie un tableau avec l'id normalisé et le format du lieu
	$lieu_normalise = lieu_normaliser($lieu);

	// On vérifie d'abord si le service est connu et actif car sinon il est inutile de continuer.
	// -- on initialise le bloc d'erreur
	$erreur = [
		'type'    => rainette_service_est_indisponible($service),
		'service' => [
			'code'    => '',
			'message' => ''
		]
	];
	// -- on vérifie le service qui renvoie un type d'erreur ou vide sinon.
	if ($erreur['type']) {
		// Service nok :
		// -----------
		// On construit le tableau directement sans passer par un cache.
		$tableau = [
			'donnees' => [],
			'extras'  => erreur_normaliser_extras($erreur, $lieu_normalise['id'], $mode, $service, $periodicite)
		];
	} else {
		// Service ok :
		// ----------
		// Lecture de la configuration technique des données pour le mode concerné.
		include_spip('inc/config');
		$configuration_mode = lire_config("rainette_config/{$mode}/donnees", []);

		// En fonction du service, on inclut le fichier d'API.
		// Le principe est que chaque service propose la même liste de fonctions d'interface dans un fichier unique.
		include_spip("services/{$service}");

		// Acquérir la configuration statique du service (periode, format, données...)
		$configuration_service  = configuration_service_lire($service, $mode);

		// Acquérir la configuration dynamique du service (celle modifiable par l'utilisateur via
		// le formulaire et stockée en BDD dans la table spip_meta) et la merger avec la configuration statique.
		// Cependant, celle-ci pouvant être incomplète on la complète par les valeurs par défaut quand
		// cela est nécessaire.
		$configuration_utilisateur = parametrage_service_normaliser($service, $configuration_service['defauts']);

		// Concaténer l'ensemble des configurations.
		$configuration_service  = array_merge($configuration_service, $configuration_utilisateur);

		// Si on a demandé le mode 'previsions' sans préciser la periodicité horaire des données, il faut prendre l'intervalle
		// par défaut configuré pour le service.
		if (
			($mode === 'previsions')
			and !$periodicite
		) {
			$periodicite = $configuration_service['previsions']['periodicite_defaut'];
		}

		// Construire le tableau identifiant le cache
		$cache = cache_normaliser($lieu_normalise['id'], $mode, $periodicite, $langue, $configuration_service);

		// Mise à jour du cache avec les nouvelles données météo si:
		// - le fichier cache n'existe pas
		// - la période de validité du cache est échue
		include_spip('inc/ezcache_cache');
		if (!$fichier_cache = cache_est_valide('rainette', 'meteo', $cache)) {
			// Construire l'url de la requête
			$urler = "{$service}_service2url";
			$url = $urler($lieu_normalise, $mode, $periodicite, $langue, $configuration_service);

			// Acquérir le flux XML ou JSON dans un tableau si les limites du service ne sont pas atteintes
			// et traiter les cas d'erreurs du plugin ou du service.
			include_spip('inc/rainette_requeter');
			if (!requete_autorisee($configuration_service['offres']['limites'], $service)) {
				// La requête n'est pas autorisée parce qu'elle excède les limitations d'utilisation du service.
				// On renvoie une erreur pour prévenir les utilisateurs.
				$erreur['type'] = 'limite_service';
			} else {
				$flux = requeter($url, $service, $configuration_service);
				if (!empty($flux['erreur'])) {
					// Erreur lors du traitement de la requête due à l'URL ou à la conversion en XML ou JSON.
					// Cette erreur n'est pas retournée par le service.
					$erreur['type'] = $flux['erreur'];
				} else {
					// On teste une erreur d'acquisition renvoyée par le service. Pour tous les services, une cle de base
					// est explicitement utilisée pour distinguer les erreurs; sa présence permet d'identifier un cas d'erreur.
					include_spip('inc/filtres');
					$configuration_service_erreur = configuration_service_lire($service, 'erreurs');
					$flux_erreur = $flux;
					if (!empty($configuration_service_erreur['cle_base'])) {
						$flux_erreur = table_valeur($flux, implode('/', $configuration_service_erreur['cle_base']), []);
					}

					// On normalise le flux en utilisant le mode d'erreur pour vérifier si on obtient bien une erreur.
					$configuration_erreur = lire_config('rainette_config/erreurs/donnees', []);
					$erreur_service = meteo_normaliser($flux_erreur, $langue, $configuration_erreur, $configuration_service_erreur, -1);
					$verifier = "{$service}_erreur_verifier";
					if ($verifier($erreur_service)) {
						// Une erreur est renvoyée par le service, on formate l'erreur correctement.
						$erreur['type'] = 'reponse_service';
						$erreur['service'] = array_merge($erreur['service'], $erreur_service);
					} else {
						// On se positionne sur le niveau de base du flux des données où commence le tableau des données météorologiques.
						if (!empty($configuration_service['cle_base'])) {
							$flux = table_valeur($flux, implode('/', $configuration_service['cle_base']), []);
						}

						if (empty($flux)) {
							// Pas d'erreur retournée par le service mais aucune donnée fournie. Ce cas est peut-être impossible
							// mais on le traite tout de même par sécurité avec un type particulier.
							$erreur['type'] = 'aucune_donnee';
						} else {
							// En mode prévisions, le niveau de base est un tableau de n éléments, chaque élément étant un tableau contenant
							// les données météorologiques d'un jour à venir ([0] => jour0[], [1] => jour1[]...).
							// En mode infos ou conditions, on a directement accès aux données météorologiques (jour[]).
							// Pour réaliser un traitement standard, on transforme donc le jour[] en un tableau d'un seul élément ([0] => jour[])
							// qui pourra être traité comme celui des prévisions.
							if (
								($mode === 'conditions')
								or ($mode === 'infos')
							) {
								$flux = [$flux];
							}

							// Certains flux, en général de prévisions, ne propose pas les données par jour.
							// Il est donc nécessaire de rétablir une logique par jour avant de passer les jours en revue.
							$arranger = "{$service}_flux2flux";
							if (function_exists($arranger)) {
								$flux = $arranger($flux, $mode, $periodicite, $configuration_service);
							}

							// Convertir le flux en tableau standard pour la mise en cache. Ce traitement se déroule en
							// 3 étapes :
							// -1- initialisation du tableau standard à partir uniquement des données reçues du service
							// -2- complément du tableau avec les données propres à chaque service
							// -3- complément du tableau avec les données communes à tous les services
							include_spip('inc/rainette_convertir');
							foreach ($flux as $_index_jour => $_flux_jour) {
								// Certains services proposent ses flux heure dans un format incompatible avec l'attendu du mashup.
								// Pour ces services, il est nécessaire de réorganiser le flux jour pour que les flux heure
								// soient adressables et numériques.
								$arranger = "{$service}_flux2{$mode}";
								if (function_exists($arranger)) {
									$_flux_jour = $arranger($_flux_jour, $periodicite, $configuration_service);
								}

								// Pour les informations et les conditions les données récupérées concernent toute la même "période".
								// Par contre, pour les prévisions on distingue 2 type de données :
								// - celles du jour indépendamment de la période horaire
								// - celles correspondant à une période horaire choisie (24, 12, 6, 3, 1)
								//   Ces données sont stockées à un index horaire de 0 à n qui représente la période horaire.
								// Pour avoir un traitement identique pour les deux types de données on considère que l'index horaire
								// des données jour est égal à -1.
								// On crée donc le tableau des index correspondant au mode choisi et on boucle dessus.
								$periodes_horaires = [-1];
								if ($periodicite) {
									for ($i = 0; $i < (24 / $periodicite); $i++) {
										$periodes_horaires[] = $i;
									}
								}

								// On détermine le flux heure en fonction du service. Ce flux heure coincide avec le flux jour dans
								// la majeure partie des cas
								$flux_heure = $_flux_jour;
								if (
									(count($periodes_horaires) > 1)
									and !empty($configuration_service['cle_heure'])
								) {
									$flux_heure = table_valeur($_flux_jour, implode('/', $configuration_service['cle_heure']), []);
								}

								// On boucle sur chaque période horaire pour remplir le tableau complet.
								foreach ($periodes_horaires as $_periode) {
									// 1- Initialiser le tableau normalisé des informations à partir des données brutes
									//    fournies par le service.
									//    Suivant la période il faut prendre le flux jour ou le flux heure. On calcule donc le flux heure
									//    quand c'est nécessaire.
									$flux_a_normaliser = ($_periode === -1)
										? $_flux_jour
										: ($configuration_service['structure_heure'] ? $flux_heure[$_periode] : $flux_heure);
									$donnees = meteo_normaliser($flux_a_normaliser, $langue, $configuration_mode, $configuration_service, $_periode);

									if ($donnees) {
										// 2- Pré-calculs standard communs à tous les services :
										//    - donnée 'code' : on l'initialise avec la donnée 'code_meteo' native
										//    - données 'icone' : on initialise les index 'code' et 'code_meteo'
										if (
											($mode !== 'infos')
											and isset($donnees['code_meteo'])
										) {
											$donnees['code'] = $donnees['code_meteo'];
											$donnees['icone']['code'] = $donnees['code'];
											$donnees['icone']['code_meteo'] = $donnees['code_meteo'];
										}

										//    -- Calcul du point de rosée pour les conditions si celui-ci n'est pas fourni
										if (
											($mode === 'conditions')
											and !is_float($donnees['point_rosee'])
										) {
											$donnees['point_rosee'] = temperature2pointrosee(
												$donnees['temperature_reelle'],
												$donnees['humidite'],
												$configuration_service['unite']
											);
										}

										// 3- Compléments spécifiques au service et au mode permettant de convertir certaines
										//    et mettre au point des données calculées (résumé, période...).
										//    Si ces compléments sont inutiles, la fonction n'existe pas.
										$completer = "{$service}_complement2{$mode}";
										if (function_exists($completer)) {
											$donnees = $mode === 'previsions'
												? $completer($donnees, $langue, $configuration_service, $_periode)
												: $completer($donnees, $langue, $configuration_service);
										}

										// 4- Post-calculs standard communs à tous les services
										//    -- Calcul de l'icone si celui-ci n'a pas été rempli par la fonction de complément
										//       (si rempli c'est que l'icone est, par configuration, fourni par l'API)
										if (
											($mode !== 'infos')
											and empty($donnees['icone']['source'])
											and ($configuration_service['condition'] !== $configuration_service['alias'])
											and isset($donnees['code'])
										) {
											$theme_origine = str_replace("{$configuration_service['alias']}_", '', $configuration_service['condition']);
											$parametres_icone = [
												'service'       => $service,
												'transcodages'  => $configuration_service['icones']['transcodages'],
												'theme_origine' => $theme_origine,
												'theme_id'      => $configuration_service["theme_{$theme_origine}"]
											];
											$donnees['icone']['source'] = code_meteo2icone($donnees['code'], $donnees['periode'], $parametres_icone);
										}

										//    -- Calcul du risque uv à partir de l'indice uv si celui-ci est fourni
										if (
											!empty($configuration_service['donnees']['indice_uv']['cle'])
											and isset($donnees['indice_uv'])
											and is_float($donnees['indice_uv'])
										) {
											$donnees['risque_uv'] = indice2risque_uv($donnees['indice_uv']);
										}

										// 5- Ajout du bloc à l'index en cours
										if ($_periode === -1) {
											$tableau[$_index_jour] = $donnees;
										} else {
											$tableau[$_index_jour]['heure'][$_periode] = $donnees;
										}
									}
								}
							}
						}
					}
				}
			}

			// 4- Compléments standard à tous les services et tous les modes
			$extras = erreur_normaliser_extras($erreur, $lieu_normalise['id'], $mode, $service, $periodicite, $configuration_service);

			// On range les données et les extras dans un tableau associatif à deux entrées ('donnees', 'extras')
			if ($tableau) {
				// Pour les modes "conditions" et "infos" l'ensemble des données météo est accessible sous
				// l'index 'donnees'. Il faut donc supprimer l'index 0 provenant du traitement commun avec
				// les prévisions.
				// Pour les prévisions l'index 0 à n désigne le jour, il faut donc le conserver
				$tableau = [
					'donnees' => ($mode != 'previsions' ? array_shift($tableau) : $tableau),
					'extras'  => $extras
				];
			} else {
				// Traitement des erreurs de flux. On positionne toujours les bloc extra contenant l'erreur,
				// le bloc des données qui est mis à tableau vide dans ce cas à l'index 1.
				$tableau = [
					'donnees' => [],
					'extras'  => $extras
				];
			}

			// Pipeline de fin de chargement des données météo. Peut-être utilisé :
			// -- pour effectuer des traitements annexes à partir des données météo (archivage, par exemple)
			// -- pour ajouter ou modifier des données au tableau (la modification n'est pas conseillée cependant)
			$tableau = pipeline(
				'post_chargement_meteo',
				[
					'args' => [
						'lieu'          => $lieu_normalise,
						'mode'          => $mode,
						'service'       => $service,
						'periodicite'   => $periodicite,
						'langue'        => $langue,
						'via_job'       => $via_job,
						'configuration' => $configuration_service
					],
					'data' => $tableau
				]
			);

			// Mise à jour du cache
			cache_ecrire('rainette', 'meteo', $cache, json_encode($tableau));
		} else {
			// Lecture des données du fichier cache valide
			$tableau = cache_lire('rainette', 'meteo', $fichier_cache);
		}
	}

	return $tableau;
}
