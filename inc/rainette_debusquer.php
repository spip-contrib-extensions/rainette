<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_RAINETTE_DEBUG')) {
	/**
	 * Activation ou désactivation des traces de debug.
	 */
	define('_RAINETTE_DEBUG', false);
}
if (!defined('_RAINETTE_DEBUG_CLES_PREVISIONS')) {
	/**
	 * Clé jour par défaut utilisée pour afficher les prévisions : jour suivant.
	 */
	define('_RAINETTE_DEBUG_CLES_PREVISIONS', '1');
}

/**
 * Affiche de façon lisible le cache d'une requête d'un service donné en utilisant la fonction `bel_env()`.
 *
 * @param string      $lieu    Le lieu concerné par la méteo exprimé selon les critères requis par le service.
 * @param null|string $mode    Le type de données météorologiques demandé.
 * @param null|string $service Le nom abrégé du service.
 * @param null|string $langue Code de langue spip indiquant langue choisie pour afficher les données météo.
 *
 * @return string
 */
function rainette_debug_afficher_cache(string $lieu, ?string $mode = 'previsions', ?string $service = '', ?string $langue = '') : string {
	// Initialisations : en particulier on récupère le service par défaut si besoin
	static $cles_previsions = [];
	$debug = '';

	// Service par défaut si non fourni
	include_spip('rainette_fonctions');
	if (!$service) {
		$service = rainette_service_defaut();
	}

	// Langue par défaut si non fournie
	include_spip('inc/rainette_normaliser');
	if (!$langue) {
		$langue = langue_spip_determiner();
	}

	// Recuperation du tableau des conditions courantes
	if (
		!rainette_service_est_indisponible($service)
		and _RAINETTE_DEBUG
		and function_exists('bel_env')
	) {
		// Si on est en mode prévisions, on impose la périodicité à la valeur par défaut pour le service
		$periodicite = 0;
		if ($mode === 'previsions') {
			$configuration = configuration_service_lire($service, $mode);
			$periodicite = $configuration['periodicite_defaut'];
		}

		// Chargement du cache
		$charger = charger_fonction('meteo_charger', 'inc');
		$options = [
			'periodicite' => $periodicite,
			'langue' => $langue
		];
		$tableau = $charger($lieu, $mode, $service, $options);

		// Pour le mode prévisions, on supprime tous les jours postérieur au lendemain pour éviter d'avoir un
		// affichage trop conséquent.
		if ($mode === 'previsions') {
			// Récupérer les index de prévisions à afficher
			if (!$cles_previsions) {
				$cles_previsions = explode(',', _RAINETTE_DEBUG_CLES_PREVISIONS);
			}
			foreach ($tableau['donnees'] as $_jour => $_valeurs) {
				if (!in_array($_jour, $cles_previsions)) {
					unset($tableau['donnees'][$_jour]);
				}
			}
		}
		$debug = bel_env(serialize($tableau), true);
	}

	return $debug;
}

/**
 * Construit un tableau comparatif de la météo d'un lieu pour les différents services demandés.
 *
 * @param null|string $mode Le type de données météorologiques demandé.
 * @param null|string $langue Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param null|array  $jeu  Jeu de données sous la forme [service] = lieu. Le lieu doit être le même exprimé sous la forme
 *                          recommandée par le service.
 *
 * @return array Le tableau de comparaison indexé par chaque service
 */
function rainette_debug_comparer_services(?string $mode = 'conditions', ?string $langue = '', ?array $jeu = []) : array {
	$debug = [];

	$mode = $mode ?: 'conditions';

	// On acquiert la structure exact des tableaux de données standard
	include_spip('inc/config');
	$config_donnees = lire_config("rainette_config/{$mode}/donnees", []);

	if ($config_donnees) {
		// Paramètres par défaut si besoin
		include_spip('inc/rainette_normaliser');
		$langue = $langue ?: langue_spip_determiner();
		$jeu = $jeu ?: rainette_debug_jeu_defaut();

		// On boucle sur chaque jeu de demo
		include_spip('inc/utils');
		$periodicite = 0;
		foreach ($jeu as $_service => $_lieu) {
			// Vérifier que le service est bien configuré avant de requérir sa météo
			$configuration = configuration_service_lire($_service, $mode);
			// Si on est en mode prévisions, on impose la périodicité à la valeur par défaut pour le service
			if ($mode === 'previsions') {
				$periodicite = $configuration['periodicite_defaut'];
			}

			// Chargement des données
			$charger = charger_fonction('meteo_charger', 'inc');
			$options = [
				'periodicite' => $periodicite,
				'langue' => $langue
			];
			$tableau = $charger($_lieu, $mode, $_service, $options);

			// Si pas d'erreur on affiche les données
			if (!$tableau['extras']['erreur']['type']) {
				// Suivant le mode on extrait les données à afficher. Pour le mode prévisions, on choisit le
				// jour suivant le jour courant et pour les données heures l'index 0 qui existe toujours.
				// En outre, il faut tenir compte pour les prévisions qu'il existe des données "jour" et
				// des données "heure" alors que pour les autres modes ll n'existe que des données "jour".
				$tableau_jour = ($mode === 'previsions') ? $tableau['donnees'][1] : $tableau['donnees'];
				foreach ($config_donnees as $_donnee => $_config) {
					// On détermine la valeur et le type PHP de la donnée dans le tableau standard et
					// on stocke les informations de configuration rangement pour les prévisions
					if (
						isset($_config['rangement'])
						and ($_config['rangement'] === 'heure')
					) {
						$valeur = $tableau_jour['heure'][0][$_donnee];
						$type_php = strtolower(gettype($tableau_jour['heure'][0][$_donnee]));
						$rangement = $_config['rangement'];
					} else {
						$valeur = $tableau_jour[$_donnee];
						$type_php = strtolower(gettype($tableau_jour[$_donnee]));
						$rangement = $_config['rangement'] ?? '';
					}

					// On construit le tableau de debug
					$debug[$_donnee][$_service]['valeur'] = $valeur;
					if (
						($_donnee === 'icon_meteo')
						and tester_url_absolue($valeur)
					) {
						$debug[$_donnee][$_service]['valeur'] = dirname($valeur) . '/<br />' . basename($valeur);
					}
					$debug[$_donnee][$_service]['type_php'] = $_config['type_php'];
					$debug[$_donnee][$_service]['type_data'] = $type_php;
					$debug[$_donnee][$_service]['rangement'] = $rangement;
					$debug[$_donnee][$_service]['groupe'] = $_config['groupe'];
					$debug[$_donnee][$_service]['type_unite'] = $_config['type_unite'];
					$debug[$_donnee][$_service]['origine'] = in_array($_donnee, $tableau['extras']['config']['source_api'])
						? 'service'
						: (in_array($_donnee, $tableau['extras']['config']['source_calcul']) ? 'calcul' : 'none');
					$debug[$_donnee][$_service]['disponible'] = !in_array($_donnee, $tableau['extras']['config']['source_none']);
					if (!$debug[$_donnee][$_service]['disponible']) {
						// La donnée n'est pas mise à disposition par le service : on informe par erreur nondispo
						$debug[$_donnee][$_service]['erreur'] = 'non_service';
					} elseif ($type_php === 'null') {
						// La donnée est normalement mise à disposition par le service ou par calcul mais
						// n'a pas été mise à jour depuis son initialisation : il y a donc une indispo temporaire
						$debug[$_donnee][$_service]['erreur'] = 'erreur';
					} else {
						// La donnée est normalement mise à disposition par le service ou par calcul et
						// mise à jour correctement : pas d'erreur
						$debug[$_donnee][$_service]['erreur'] = '';
					}
				}
			}
		}
	}

	return $debug;
}

/**
 * Renvoie la chaine d'une valeur donnée de la façon la plus lisible possible.
 *
 * @param string      $donnee  Type de la donnée
 * @param array       $valeur  Valeur de la donnée
 * @param null|string $service Le nom abrégé du service.
 *
 * @return string Chaine à afficher pour fournir l'information la plus précise possible sur la donnée.
 */
function rainette_debug_afficher_donnee(string $donnee, array $valeur, ?string $service = '') : string {
	// Initialisations : en particulier on récupère le service par défaut si besoin
	$texte = '';
	include_spip('rainette_fonctions');
	if (!$service) {
		$service = rainette_service_defaut();
	}

	// Détermination de l'origine de la donnée (service ou calcul), du type de la valeur obtenue
	// et l'unité
	$type_data = $valeur['type_data'];
	$type_unite = $valeur['type_unite'];

	if (!$valeur['disponible']) {
		// La donnée n'est pas mise à disposition par le service, ni par api ni par calcul :
		// -> on informe par un texte adapté
		$texte = 'Non Fourni';
	} elseif ($type_data === 'null') {
		// La donnée est normalement mise à disposition par le service ou par calcul mais
		// n'a pas été mise à jour depuis son initialisation : il y a donc une indispo temporaire
		$texte = 'Indispo Temp';
	} elseif ($type_data === 'array') {
		// La donnée est normalement mise à disposition par le service ou par calcul et est un tableau
		foreach ($valeur['valeur'] as $_cle => $_valeur) {
			$texte .= ($texte ? '<br />' : '') . "<strong>{$_cle}</strong> : " . $_valeur;
		}
	} else {
		// La donnée est normalement mise à disposition par le service ou par calcul et
		// mise à jour correctement : on l'affiche avec son unité éventuelle
		$texte = $type_unite ? rainette_afficher_unite($valeur['valeur'], $type_unite, -1, $service) : $valeur['valeur'];
	}

	// On termine en ajoutant le type obtenu de la donnée
	$texte .= "<br /><em>{$type_data}</em>";

	return $texte;
}

/**
 * Fournit le jeu de données par défaut pour l'ensemble des services.
 * Paris est utilisé comme le lieu par défaut.
 *
 * @return array
 */
function rainette_debug_jeu_defaut() : array {
	$jeu = [];

	include_spip('rainette_fonctions');
	$services = rainette_lister_services('tableau', true, true);
	if ($services) {
		foreach ($services as $_service => $_nom) {
			if ($_service === 'accuweather') {
				$jeu[$_service] = '623';
			} elseif ($_service === 'meteoconcept') {
				$jeu[$_service] = '75101';
			} elseif ($_service === 'openmeteo') {
				$jeu[$_service] = '48.85,2.35';
			} else {
				$jeu[$_service] = 'Paris,France';
			}
		}
	}

	return $jeu;
}

/**
 * Affiche l'état d'exécution de l'ensemble des services.
 *
 * @return array
 */
function rainette_debug_afficher_execution() : array {
	$debug = [];

	include_spip('rainette_fonctions');
	$services = rainette_lister_services();
	if ($services) {
		include_spip('inc/config');
		$execution = lire_config('rainette_execution', []);

		include_spip('inc/rainette_normaliser');
		foreach ($services as $_service => $_nom) {
			$configuration = configuration_service_lire($_service, 'service');

			$debug[$_service]['dernier_appel'] = $execution[$_service]['dernier_appel'] ?? '--';
			foreach (['year', 'month', 'day', 'hour', 'minute', 'second'] as $_periode) {
				if (isset($configuration['offres']['limites'][$_periode])) {
					$compteur = $execution[$_service]['compteurs'][$_periode] ?? '--';
					$debug[$_service][$_periode] = "{$compteur} / {$configuration['offres']['limites'][$_periode]}";
				} else {
					$debug[$_service][$_periode] = '';
				}
			}
		}
	}

	return $debug;
}
/**
 * Vérifie pour un service, si le paramétrage utilisateur est valide afin d'utiliser le service.
 * Encapsule la fonction parametrage_service_est_valide().
 *
 * @param string $service Le nom abrégé du service.
 *
 * @return bool `true` si valide, `false` sinon.
 */
function rainette_debug_service_est_parametre(string $service) : bool {
	include_spip('inc/rainette_normaliser');

	return parametrage_service_est_valide($service);
}

/**
 * Vérifie si la liste des langues de SPIP a changé et qu'il faut modifier la config rainette.
 *
 * @return string Texte fournissant la liste des langues manquantes.
 */
function rainette_debug_verifier_langue_manquante() : string {
	// Initialisation de l'affichage à aucune erreur.
	$texte = 'Aucun code de langue SPIP manquant';

	// Lecture de la configuration technique des langues alternatives.
	include_spip('inc/config');
	$langues_alternatives = lire_config('rainette_config/langues_alternatives', []);

	include_spip('inc/lang_liste');
	foreach ($GLOBALS['codes_langues'] as $code => $langue) {
		if (!array_key_exists($code, $langues_alternatives)) {
			$texte .= "Code manquant {$code}<br />";
		}
	}

	return $texte;
}
