<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Rainette.
 *
 * @package    SPIP\RAINETTE\INSTALLATION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'installation de Rainette limitée à la création de la configuration en meta.
 *
 * @param string $nom_meta_base_version Nom de la meta informant de la version du schéma de données du plugin
 *                                      installé dans SPIP
 * @param string $version_cible         Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 *
 * @return void
**/
function rainette_upgrade(string $nom_meta_base_version, string $version_cible) : void {
	// A la création on initialise la configuration complète (technique + services)
	$maj['create'] = [
		['rainette_configurer'],
	];

	// Ajout de la configuration du nouveau service Meteo Concept
	$maj['3'] = [
		['rainette_configurer'],
	];

	// Recharger la configuration du plugin et de tous les services
	// - Amélioration de la configuration des données calculées
	$maj['4'] = [
		['rainette_configurer'],
	];

	// Recharger la configuration du plugin et de tous les services
	$maj['5'] = [
		['rainette_configurer'],
	];

	// Recharger la configuration du plugin et de tous les services
	// - ajout version pour les YAML
	// - ajout service Open-Meteo
	$maj['6'] = [
		['rainette_configurer'],
	];

	// Recharger la configuration du plugin et de tous les services
	// - Mise à jour de la gestion des icones et en particulier du transcodage du code météo vers les icones (configuration)
	$maj['7'] = [
		['rainette_configurer'],
	];

	// Recharger la configuration du plugin et de tous les services
	// - Ajout d'un indicateur 'historisable' pour les données de conditions temps réel
	$maj['8'] = [
		['rainette_configurer'],
	];

	// Ajout de la configuration du service weather mais qui restera inactif
	$maj['9'] = [
		['rainette_configurer'],
	];

	// Ajout des formats de lieu autorisés par chaque service
	$maj['10'] = [
		['rainette_configurer'],
	];

	// Ajout des formats de lieu autorisés par chaque service
	$maj['11'] = [
		['rainette_configurer'],
	];

	// La température ressentie est un indice pas une vraie température
	$maj['12'] = [
		['rainette_configurer'],
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Fonction de désinstallation du plugin Rainette (suppression des metas).
 *
 * @param string $nom_meta_base_version Nom de la meta informant de la version du schéma de données du plugin
 *                                      installé dans SPIP
 *
 * @return void
**/
function rainette_vider_tables(string $nom_meta_base_version) : void {
	// Effacer les meta du plugin
	// - la meta de configuration technique de Rainette
	effacer_meta('rainette_config');
	// - la meta de configuration de chaque service
	include_spip('rainette_fonctions');
	$services = rainette_lister_services('tableau', false);
	foreach (array_keys($services) as $_id_service) {
		effacer_meta("rainette_{$_id_service}");
	}
	// - la meta de stockage des informations de dernière exécution de chaque service
	effacer_meta('rainette_execution');
	// - la configuration utilisateur de chaque service
	effacer_meta('rainette');
	// - la meta de schéma
	effacer_meta($nom_meta_base_version);
}

/**
 * Enregistre en meta la configuration générale de Rainette ainsi que celle de tous les services disponibles.
 *
 * @return void
 */
function rainette_configurer() {
	// Compilation de la configuration générale de Rainette incluse dans le fichier services/config/rainette.yaml
	if ($fichier_config = find_in_path('services/config/rainette.yaml')) {
		// Extraire et normaliser la configuration du plugin incluse dans le fichier.
		$configuration = configuration_technique_charger($fichier_config);

		// Ecriture de la meta associée
		ecrire_config('rainette_config', $configuration);

		// La configuration de chaque service est stockée dans un fichier YAML dont le nom équivaut à l'id du service.
		if ($fichiers_yaml = glob(_DIR_PLUGIN_RAINETTE . 'services/*.yaml')) {
			foreach ($fichiers_yaml as $_fichier) {
				// On détermine l'id du service
				$service = strtolower(basename($_fichier, '.yaml'));

				// Extraire et normaliser la configuration du service incluse dans le fichier.
				$configuration = configuration_service_charger($service, $_fichier);

				// Ecriture de la meta associée au service
				ecrire_config("rainette_{$service}", $configuration);
			}
		}
	}
}

/**
 * Renvoie la configuration générale de Rainette extraite de son fichier YAML.
 *
 * @param null|string $fichier_config Fichier de configuration YAML de Rainette pour optimiser le traitement.
 *                                    Si vide on le cherche dans le path.
 *
 * @return array Tableau de configuration de Rainette.
 */
function configuration_technique_charger(?string $fichier_config = '') : array {
	// Si le fichier n'a pas été fourni, on le recherche.
	if (!$fichier_config) {
		$fichier_config = find_in_path('services/config/rainette.yaml');
	}

	// Extraire la configuration incluse dans le fichier.
	include_spip('inc/yaml');

	return yaml_decode_file($fichier_config);
}

/**
 * Normalise et renvoie la configuration d'un service donné extraite de son fichier YAML.
 *
 * @param string      $service        Le nom abrégé du service
 * @param null|string $fichier_config Fichier de configuration YAML de Rainette pour optimiser le traitement.
 *                                    Si vide on le cherche dans le path.
 *
 * @return array
 */
function configuration_service_charger(string $service, ?string $fichier_config = '') : array {
	$configuration = [];

	// Si le fichier n'a pas été fourni, on le recherche.
	if (!$fichier_config) {
		$fichier_config = find_in_path("services/{$service}.yaml");
	}

	// Lecture de la configuration technique de Rainette.
	// Cette configuration doit toujours exister avant de charger celle d'un service.
	include_spip('inc/config');
	$configuration_technique = lire_config('rainette_config', []);

	// Compilation de la configuration du service:
	// - si le fichier YAML existe
	// - et si la configuration technique a bien été déjà stockée en meta.
	if (
		$configuration_technique
		and $fichier_config
	) {
		// Initialisation standard du bloc (permet d'avoir toujours la même structure et de simplifier le YAML)
		$configuration_defaut = configuration_service_initialiser($service);

		// Extraire la configuration incluse dans le fichier du service.
		include_spip('inc/yaml');
		$configuration_yaml = yaml_decode_file($fichier_config);

		// Traitement de chaque bloc si le YAML du fichier est conforme
		if ($configuration_yaml) {
			$blocs = $configuration_technique['blocs'];
			foreach ($blocs as $_bloc => $_infos_bloc) {
				// Ne retenir que les blocs utiles à l'initialisation de la configuration d'un service
				if ($_infos_bloc['init']) {
					// -- Normalisation des sous-blocs
					foreach ($_infos_bloc['sousblocs'] as $_sous_bloc) {
						$configuration[$_bloc][$_sous_bloc] = array_merge(
							$configuration_defaut[$_bloc][$_sous_bloc],
							$configuration_yaml[$_bloc][$_sous_bloc]
						);
						// -- -- Suppression du sous-bloc
						unset($configuration_yaml[$_bloc][$_sous_bloc], $configuration_defaut[$_bloc][$_sous_bloc]);
					}

					if ($_bloc === 'service') {
						// -- Traitement du sous-bloc des langues du bloc service
						// -- -- Rangement des langues dont le code est identique à ceux de SPIP
						foreach ($configuration[$_bloc]['langues']['codes_spip'] as $_code) {
							$configuration[$_bloc]['langues']['disponibles'][$_code] = $_code;
						}
						unset($configuration[$_bloc]['langues']['codes_spip']);
						// -- -- Rangement des langues dont le code est différent à ceux de SPIP
						$configuration[$_bloc]['langues']['disponibles'] = array_merge(
							$configuration[$_bloc]['langues']['disponibles'],
							$configuration[$_bloc]['langues']['codes_non_spip']
						);
						unset($configuration[$_bloc]['langues']['codes_non_spip']);
					}

					// -- Normalisation des champs hors sous-blocs
					$configuration[$_bloc] = array_merge(
						$configuration_defaut[$_bloc],
						$configuration_yaml[$_bloc],
						$configuration[$_bloc]
					);
				}
			}
		}
	}

	return $configuration;
}

/**
 * Initialise la configuration d'un service donné.
 *
 * @param string $service Le nom abrégé du service
 *
 * @return array
 */
function configuration_service_initialiser(string $service) : array {
	// Configuration par défaut anonymisée (sans la mise à jour de l'id du service).
	// - permet d'éviter des boucles inutiles quand on configure tous les services.
	static $configuration_anonyme = [];

	// Initialiser la configuration à vide dans le cas improbable d'une erreur
	$configuration = [];

	// Lecture de la configuration technique de Rainette.
	include_spip('inc/config');
	$configuration_technique = lire_config('rainette_config', []);

	if ($configuration_technique) {
		if (!$configuration_anonyme) {
			foreach ($configuration_technique['blocs'] as $_bloc => $_infos_bloc) {
				// Ne retenir que les blocs utiles à l'initialisation de la configuration d'un service
				if ($_infos_bloc['init']) {
					$configuration_anonyme[$_bloc] = $configuration_technique[$_bloc];
				}
			}
		}

		// On intègre la configuration par défaut anonyme
		$configuration = $configuration_anonyme;

		// On complète la configuration avec l'id du service qui sert au champ alias, nom et condition.
		$configuration['service']['alias'] = $service;
		$configuration['service']['nom'] = $service;
		$configuration['service']['defauts']['condition'] = $service;

		// On complète la configuration avec le logo pour les crédits
		$chemin_logo = "services/images/{$service}.png";
		$configuration['service']['credits']['logo'] = find_in_path($chemin_logo) ? $chemin_logo : '';
	}

	return $configuration;
}
