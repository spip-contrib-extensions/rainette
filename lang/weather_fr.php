<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/weather-rainette?lang_cible=fr
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'meteo_0' => 'tornade',
	'meteo_1' => 'tempête tropicale',
	'meteo_10' => 'pluie verglaçante',
	'meteo_11' => 'averses',
	'meteo_12' => 'pluie',
	'meteo_13' => 'quelques flocons',
	'meteo_14' => 'faibles averses de neige',
	'meteo_15' => 'blizzard',
	'meteo_16' => 'neige',
	'meteo_17' => 'grêle',
	'meteo_18' => 'neige fondue',
	'meteo_19' => 'poussière',
	'meteo_2' => 'ouragan',
	'meteo_20' => 'brumeux',
	'meteo_21' => 'brume',
	'meteo_22' => 'brouillard',
	'meteo_23' => 'bourasques',
	'meteo_24' => 'vent',
	'meteo_25' => 'froid',
	'meteo_26' => 'nuageux',
	'meteo_27' => 'clair de lune très nuageux',
	'meteo_28' => 'très nuageux',
	'meteo_29' => 'clair de lune et nuages épars',
	'meteo_3' => 'orage violent',
	'meteo_30' => 'soleil et nuages épars',
	'meteo_31' => 'clair de lune',
	'meteo_32' => 'soleil',
	'meteo_33' => 'clair de lune voilé',
	'meteo_34' => 'soleil voilé',
	'meteo_35' => 'pluie et grêle mélée',
	'meteo_36' => 'chaleur',
	'meteo_37' => 'orages isolés',
	'meteo_38' => 'orage épars',
	'meteo_39' => 'averses éparses (jour)',
	'meteo_4' => 'orage',
	'meteo_40' => 'forte pluie',
	'meteo_41' => 'averses de neige éparses (jour)',
	'meteo_42' => 'forte chute de neige',
	'meteo_43' => 'tempête de neige',
	'meteo_44' => 'ensoleillé',
	'meteo_45' => 'averses éparses (nuit)',
	'meteo_46' => 'averses de neige éparses (nuit)',
	'meteo_47' => 'orages isolés',
	'meteo_5' => 'pluie et neige mélée',
	'meteo_6' => 'pluie et verglas',
	'meteo_7' => 'neige et verglas',
	'meteo_8' => 'bruine verglaçante',
	'meteo_9' => 'bruine'
);
