<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/weather-rainette?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'meteo_1' => 'tropischer sturm',
	'meteo_10' => 'überfrierende nässe',
	'meteo_11' => 'regenschauer',
	'meteo_13' => 'einige schneeflocken',
	'meteo_14' => 'schwache schneeschauer',
	'meteo_15' => 'blizzard',
	'meteo_16' => 'schnee',
	'meteo_17' => 'hagel',
	'meteo_18' => 'schneeregen',
	'meteo_19' => 'staub',
	'meteo_2' => 'orkan',
	'meteo_20' => 'neblig',
	'meteo_21' => 'dunst',
	'meteo_22' => 'nebel',
	'meteo_23' => 'windstöße',
	'meteo_24' => 'wind',
	'meteo_25' => 'kalt',
	'meteo_26' => 'wolkig',
	'meteo_27' => 'vollmond und wolken',
	'meteo_28' => 'sehr wolkig',
	'meteo_29' => 'vollmond und einzelne wolken',
	'meteo_3' => 'hetiges gewitter',
	'meteo_30' => 'sonne und einzelne wolken',
	'meteo_31' => 'vollmond',
	'meteo_32' => 'sonne',
	'meteo_33' => 'vollmond und einzelne wolken',
	'meteo_34' => 'sonne und einzelne wolken',
	'meteo_35' => 'regen und hagel gemischt',
	'meteo_36' => 'hitze',
	'meteo_37' => 'vereinzelte gewitter',
	'meteo_38' => 'vereinzelte gewitter',
	'meteo_4' => 'gewitter',
	'meteo_44' => 'sonnig',
	'meteo_5' => 'regen und schnee gemischt',
	'meteo_6' => 'regen und glatteis',
	'meteo_7' => 'schnee und glatteis',
	'meteo_8' => 'überfrierender nieselregen',
	'meteo_9' => 'sprühregen'
);
