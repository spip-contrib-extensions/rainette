<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/weather-rainette?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'meteo_1' => 'tempestad tropical',
	'meteo_10' => 'lluvia heladiza',
	'meteo_11' => 'chubascos',
	'meteo_13' => 'unos copos de nieve',
	'meteo_14' => 'escasas nevadas',
	'meteo_15' => 'ventisca',
	'meteo_16' => 'nieve',
	'meteo_17' => 'granizo',
	'meteo_18' => 'aguanieve',
	'meteo_19' => 'polva',
	'meteo_2' => 'huracán',
	'meteo_20' => 'brumoso',
	'meteo_21' => 'bruma',
	'meteo_22' => 'niebla',
	'meteo_23' => 'borrasca',
	'meteo_24' => 'viento',
	'meteo_25' => 'frío',
	'meteo_26' => 'nubes',
	'meteo_27' => 'claro de luna y nubes',
	'meteo_28' => 'muy nuboso',
	'meteo_29' => 'claro de luna y nubosidad variable',
	'meteo_3' => 'tormenta violenta',
	'meteo_30' => 'sol y nubosidad variable',
	'meteo_31' => 'claro de luna',
	'meteo_32' => 'sol',
	'meteo_33' => 'claro de luna y nubosidad variable',
	'meteo_34' => 'sol y nubosidad variable',
	'meteo_35' => 'lluvia y granizo mezclados',
	'meteo_36' => 'calor',
	'meteo_37' => 'tormentas aisladas',
	'meteo_38' => 'tormentas dispersas',
	'meteo_4' => 'tormentas',
	'meteo_44' => 'soleado',
	'meteo_5' => 'lluvia y nieve mezcladas',
	'meteo_6' => 'lluvia y hielo',
	'meteo_7' => 'nieve y hielo',
	'meteo_8' => 'llovizna heladiza',
	'meteo_9' => 'llovizna'
);
