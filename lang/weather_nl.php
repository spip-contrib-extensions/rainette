<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/weather-rainette?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'meteo_1' => 'tropische storm',
	'meteo_10' => 'aanvriezende regen',
	'meteo_11' => 'regenbuien',
	'meteo_13' => 'enkele sneeuwvlokken',
	'meteo_14' => 'lichte sneeuwbuien',
	'meteo_15' => 'stuifsneeuw',
	'meteo_16' => 'sneeuw',
	'meteo_17' => 'hagel',
	'meteo_18' => 'natte sneeuw',
	'meteo_19' => 'stof',
	'meteo_2' => 'orkaan',
	'meteo_20' => 'nevelig',
	'meteo_21' => 'mist',
	'meteo_22' => 'nevel',
	'meteo_23' => 'windstoten',
	'meteo_24' => 'wind',
	'meteo_25' => 'koud',
	'meteo_26' => 'bewolkt',
	'meteo_27' => 'overwegend bewolkt',
	'meteo_28' => 'zeer bewolkt',
	'meteo_29' => 'lichtbewolkt',
	'meteo_3' => 'hevige onweer',
	'meteo_30' => 'overwegend zonnig',
	'meteo_31' => 'heldere nacht',
	'meteo_32' => 'zon',
	'meteo_33' => 'heldere maan',
	'meteo_34' => 'zon met enkele wolken',
	'meteo_35' => 'regen- en hagelbuien',
	'meteo_36' => 'hitte',
	'meteo_37' => 'kans op lokaal onweer',
	'meteo_38' => 'kans op onweer',
	'meteo_4' => 'onweer',
	'meteo_44' => 'zonnig',
	'meteo_5' => 'regen en sneeuw',
	'meteo_6' => 'aanvriezende regen',
	'meteo_7' => 'aanvriezende sneeuw',
	'meteo_8' => 'aanvriezende motregen',
	'meteo_9' => 'motregen'
);
