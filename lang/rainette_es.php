<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/rainette-rainette?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// C
	'coucher_soleil' => 'puesta del sol',

	// D
	'demain' => 'mañana',
	'derniere_maj' => 'actualizado',
	'direction_E' => 'este',
	'direction_ENE' => 'este noreste',
	'direction_ESE' => 'este sureste',
	'direction_N' => 'norte',
	'direction_NE' => 'noreste',
	'direction_NNE' => 'norte noreste',
	'direction_NNW' => 'norte noroeste',
	'direction_NW' => 'noroeste',
	'direction_S' => 'sur',
	'direction_SE' => 'sureste',
	'direction_SSE' => 'sur sureste',
	'direction_SSW' => 'sur suroeste',
	'direction_SW' => 'suroeste',
	'direction_W' => 'oeste',
	'direction_WNW' => 'oeste noroeste',
	'direction_WSW' => 'oeste suroeste',

	// H
	'humidite' => 'humedad',

	// J
	'jour' => 'día',

	// L
	'latitude' => 'latitud',
	'lever_soleil' => 'salida del sol',
	'longitude' => 'longitud',

	// M
	'meteo' => 'tiempo',
	'meteo_conditions' => 'condiciones actuales',
	'meteo_consultation' => 'Consulte el tiempo para @ville@',
	'meteo_de' => 'Tiempo para @ville@',
	'meteo_na' => 'desconocido',
	'meteo_previsions' => 'pronóstico',
	'meteo_previsions_aujourdhui' => 'pronóstico para hoy',
	'meteo_previsions_n_jours' => 'pronósticos a @nbj@ días',

	// N
	'nuit' => 'noche',

	// P
	'point_rosee' => 'punto de rocío',
	'pression' => 'presión',

	// R
	'risque_precipitation' => 'riesgo de precip.',

	// S
	'station_observation' => 'observatorio',

	// T
	'temperature_max' => 'max.',
	'temperature_min' => 'min.',
	'temperature_ressentie' => 'percibida',
	'tendance_symbole_falling' => '↓',
	'tendance_symbole_rising' => '↑',
	'tendance_symbole_steady' => '→',
	'tendance_texte_falling' => 'bajando',
	'tendance_texte_rising' => 'subiendo',
	'tendance_texte_steady' => 'estable',

	// U
	'unite_angle_metrique' => '°',
	'unite_angle_standard' => '°',
	'unite_distance_metrique' => 'km',
	'unite_distance_standard' => 'milas',
	'unite_pourcentage_metrique' => '%',
	'unite_pourcentage_standard' => '%',
	'unite_precipitation_metrique' => 'mm',
	'unite_precipitation_standard' => 'pulgadas',
	'unite_pression_metrique' => 'mbar',
	'unite_pression_standard' => 'pulgadas',
	'unite_temperature_metrique' => '°C',
	'unite_temperature_standard' => '°F',
	'unite_vitesse_metrique' => 'km/h',
	'unite_vitesse_standard' => 'mph',

	// V
	'valeur_indeterminee' => 'N/D',
	'vent' => 'viento',
	'visibilite' => 'visibilidad',
];
