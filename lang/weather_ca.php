<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/weather-rainette?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'meteo_1' => 'tempesta tropical',
	'meteo_10' => 'pluja gelada',
	'meteo_11' => 'xàfecs',
	'meteo_13' => 'alguns flocs',
	'meteo_14' => 'xàfecs de neu febles',
	'meteo_15' => 'borrufada',
	'meteo_16' => 'neu',
	'meteo_17' => 'calamarça',
	'meteo_18' => 'aiguaneu',
	'meteo_19' => 'pols',
	'meteo_2' => 'huracà',
	'meteo_20' => 'ennuvolat ',
	'meteo_21' => 'boirina',
	'meteo_22' => 'boira',
	'meteo_23' => 'borrasques',
	'meteo_24' => 'vent',
	'meteo_25' => 'fred',
	'meteo_26' => 'ennuvolat',
	'meteo_27' => 'clar de lluna molt ennuvolat',
	'meteo_28' => 'molt ennuvolat',
	'meteo_29' => 'clar de lluna i núvols dispersos ',
	'meteo_3' => 'tempesta violent',
	'meteo_30' => 'sol i núvols dispersos',
	'meteo_31' => 'Clar de lluna ',
	'meteo_32' => 'sol',
	'meteo_33' => 'clar de lluna boirós',
	'meteo_34' => 'sol emboirat',
	'meteo_35' => 'pluja i calamarsa barrejats',
	'meteo_36' => 'calor',
	'meteo_37' => 'tempestes aïllades',
	'meteo_38' => 'tempestes disperses',
	'meteo_4' => 'tempesta',
	'meteo_44' => 'assolellat',
	'meteo_5' => 'pluja i neu barrejades',
	'meteo_6' => 'pluja i gel',
	'meteo_7' => 'neu i gel',
	'meteo_8' => 'plugims gelats',
	'meteo_9' => 'plugim'
);
