<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/rainette-rainette?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// C
	'coucher_soleil' => 'sonnenuntergang',

	// D
	'demain' => 'morgen',
	'derniere_maj' => 'aktualisierung',
	'description' => 'Beschreibung', # MODIF
	'direction_E' => 'ost',
	'direction_ENE' => 'ost nord-ost',
	'direction_ESE' => 'ost sud-ost',
	'direction_N' => 'norden',
	'direction_NE' => 'nord-osten',
	'direction_NNE' => 'nord nord-osten',
	'direction_NNW' => 'nord nord-westen',
	'direction_NW' => 'nord-westen',
	'direction_S' => 'süden',
	'direction_SE' => 'süd-osten',
	'direction_SSE' => 'süd süd-osten',
	'direction_SSW' => 'süd süd-westen',
	'direction_SW' => 'süd-westen',
	'direction_W' => 'westen',
	'direction_WNW' => 'west nord-westen',
	'direction_WSW' => 'west süd-westen',

	// H
	'humidite' => 'feuchtigkeit',

	// J
	'jour' => 'tag',

	// L
	'latitude' => 'Breite',
	'lever_soleil' => 'sonnenaufgang',
	'longitude' => 'Länge',

	// M
	'meteo' => 'wetter',
	'meteo_conditions' => 'aktuelles wetter', # MODIF
	'meteo_consultation' => 'Wetterbericht für @ville@ ansehen',
	'meteo_de' => 'Wetterbericht von  @ville@',
	'meteo_na' => 'unbekannt',
	'meteo_previsions' => 'wettervorhersage',
	'meteo_previsions_aujourdhui' => 'Vorhersagen für heute', # MODIF
	'meteo_previsions_n_jours' => 'Vorhersagen für @nbj@ Tage', # MODIF

	// N
	'nuit' => 'nacht',

	// P
	'point_rosee' => 'taupunkt',
	'pression' => 'druck',

	// R
	'risque_precipitation' => 'niederschlagrisiko',

	// S
	'station_observation' => 'wetterstation',

	// T
	'temperature_max' => 'max.',
	'temperature_min' => 'min.',
	'temperature_ressentie' => 'gefühlt wie',
	'tendance_symbole_falling' => '↓',
	'tendance_symbole_rising' => '↑',
	'tendance_symbole_steady' => '→',
	'tendance_texte_falling' => 'fallend',
	'tendance_texte_rising' => 'steigend',
	'tendance_texte_steady' => 'stabil',

	// U
	'unite_angle_metrique' => '°',
	'unite_angle_standard' => '°',
	'unite_distance_metrique' => 'km',
	'unite_distance_standard' => 'meilen',
	'unite_pourcentage_metrique' => '%',
	'unite_pourcentage_standard' => '%',
	'unite_precipitation_metrique' => 'mm',
	'unite_precipitation_standard' => 'inches',
	'unite_pression_metrique' => 'mbar',
	'unite_pression_standard' => 'inches',
	'unite_temperature_metrique' => '°C',
	'unite_temperature_standard' => '°F',
	'unite_vitesse_metrique' => 'km/h',
	'unite_vitesse_standard' => 'mph',

	// V
	'valeur_indeterminee' => 'N/A',
	'vent' => 'wind',
	'visibilite' => 'sichtweite',
];
