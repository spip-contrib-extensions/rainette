<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/rainette-rainette?lang_cible=ca
// ** ne pas modifier le fichier **

return [

	// C
	'coucher_soleil' => 'posta de sol',

	// D
	'demain' => 'demà',
	'derniere_maj' => 'Actualització',
	'description' => 'description', # MODIF
	'direction_E' => 'est',
	'direction_ENE' => 'est nord-est',
	'direction_ESE' => 'est sud-est',
	'direction_N' => 'nord',
	'direction_NE' => 'nord-est',
	'direction_NNE' => 'nord nord-est',
	'direction_NNW' => 'nord nord-oest',
	'direction_NW' => 'nord-oest',
	'direction_S' => 'sud',
	'direction_SE' => 'sud-est',
	'direction_SSE' => 'sud sud-est',
	'direction_SSW' => 'sud sud-oest',
	'direction_SW' => 'sud-oest',
	'direction_W' => 'oest',
	'direction_WNW' => 'oest nord-oest',
	'direction_WSW' => 'oest sud-oest',

	// H
	'humidite' => 'humitat',

	// J
	'jour' => 'dia',

	// L
	'latitude' => 'latitud',
	'lever_soleil' => 'alba',
	'longitude' => 'longitud',

	// M
	'meteo' => 'temps',
	'meteo_conditions' => 'condicions actuals', # MODIF
	'meteo_consultation' => 'Consulteu el Temps de @ville@',
	'meteo_de' => 'El Temps dee @ville@',
	'meteo_na' => 'desconegut',
	'meteo_previsions' => 'previsions del ',
	'meteo_previsions_aujourdhui' => 'previsions per avui', # MODIF
	'meteo_previsions_n_jours' => 'previsions a @nbj@ dies', # MODIF

	// N
	'nuit' => 'nit',

	// P
	'point_rosee' => 'punt de rosada',
	'pression' => 'pressió ',

	// R
	'region' => 'Regió', # MODIF
	'risque_precipitation' => 'risc de precip.',

	// S
	'station_observation' => 'estació',

	// T
	'temperature_max' => 'màx.',
	'temperature_min' => 'mín.',
	'temperature_ressentie' => 'sensació',
	'tendance_symbole_falling' => '↓',
	'tendance_symbole_rising' => '↑',
	'tendance_symbole_steady' => '→',
	'tendance_texte_falling' => 'en descens',
	'tendance_texte_rising' => 'cap amunt',
	'tendance_texte_steady' => 'estable',

	// U
	'unite_angle_metrique' => '°',
	'unite_angle_standard' => '°',
	'unite_distance_metrique' => 'km',
	'unite_distance_standard' => 'milles',
	'unite_pourcentage_metrique' => '%',
	'unite_pourcentage_standard' => '%',
	'unite_precipitation_metrique' => 'mm',
	'unite_precipitation_standard' => 'polzades',
	'unite_pression_metrique' => 'mbar',
	'unite_pression_standard' => 'polzades',
	'unite_temperature_metrique' => '°C',
	'unite_temperature_standard' => '°F',
	'unite_vitesse_metrique' => 'km/h',
	'unite_vitesse_standard' => 'mph',

	// V
	'valeur_indeterminee' => 'N/D',
	'vent' => 'vent',
	'visibilite' => 'visibilitat',
];
