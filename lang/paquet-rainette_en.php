<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/rainette-paquet-xml-rainette?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// R
	'rainette_description' => 'This new version of Rainette lets you select your weather service from 8 different ones.  A configuration is available for each service, in particular for setting a registration key. The displays offered by this version are incompatible with those of the v1 and v2 branches.

This plugin allows you to display weather conditions and forecasts for a given location from the feed provided by one of the supported weather services.
It does not store any information in a database, nor does it manage the selection of locations.

Weather data is displayed mainly through the use of models in templates. The plugin offers 
default templates such as {{rainette_previsions}} and {{rainette_conditions}}. It is also possible to display information on the selected location either via the {{rainette_infos}} template,
or via the <code>#RAINETTE_INFOS</code> tag. All Rainette displays can be customized (icons, labels, units, presentation, etc.).',
	'rainette_slogan' => 'Everyday weather',
];
