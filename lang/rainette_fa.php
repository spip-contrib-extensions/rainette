<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/rainette-rainette?lang_cible=fa
// ** ne pas modifier le fichier **

return [

	// C
	'coucher_soleil' => 'غروب ',

	// D
	'demain' => 'فردا',
	'derniere_maj' => 'روزآمدسازي',
	'direction_E' => 'غرب',
	'direction_ENE' => 'شرق شمال-شرقي',
	'direction_ESE' => 'شرق جنوب-شرقي',
	'direction_N' => 'جنوب ',
	'direction_NE' => 'جنوب-شرق',
	'direction_NNE' => 'جنوب جنوب-شرقي',
	'direction_NNW' => 'شمال شمال-غربي',
	'direction_NW' => 'شمال-غرب',
	'direction_S' => 'جنوب',
	'direction_SE' => 'جنوب - شرق',
	'direction_SSE' => 'جنوب جنوب-شرق ',
	'direction_SSW' => 'جنوب جنوب-غرب',
	'direction_SW' => 'جنوب-غرب',
	'direction_W' => 'غرب',
	'direction_WNW' => 'غرب شمال-غرب',
	'direction_WSW' => '<غرب جنوب-غرب',

	// H
	'humidite' => 'رطوبت',

	// J
	'jour' => 'روز',

	// L
	'latitude' => 'عرض جغرافيايي',
	'lever_soleil' => 'طلوع آفتاب',
	'longitude' => 'طول جغرافيايي',

	// M
	'meteo' => 'آب و هوا',
	'meteo_conditions' => 'وضعيت فعلي ', # MODIF
	'meteo_consultation' => 'هوايِ @ville@',
	'meteo_de' => 'هواي @ville@',
	'meteo_na' => 'ناشناخته',
	'meteo_previsions' => 'پيش‌ بيني وضعيت ',
	'meteo_previsions_aujourdhui' => 'پيش‌ بيني امروز', # MODIF
	'meteo_previsions_n_jours' => 'پيش بيني @nbj@ روز ', # MODIF

	// N
	'nuit' => 'شب ',

	// P
	'point_rosee' => 'شبنم',
	'pression' => 'فشار',

	// R
	'region' => 'منطقه', # MODIF
	'risque_precipitation' => 'ميزان بارندگي',

	// S
	'station_observation' => 'ايستگاه ',

	// T
	'temperature_max' => 'بالا',
	'temperature_min' => 'پائيين',
	'temperature_ressentie' => 'احساس ',
	'tendance_symbole_falling' => '↓',
	'tendance_symbole_rising' => '↑',
	'tendance_symbole_steady' => '→',
	'tendance_texte_falling' => 'سقوط',
	'tendance_texte_rising' => 'صعود',
	'tendance_texte_steady' => 'ثابت',

	// U
	'unite_angle_metrique' => '°',
	'unite_angle_standard' => '°',
	'unite_distance_metrique' => 'ك.م(كيلومتر)',
	'unite_distance_standard' => 'ميل ',
	'unite_pourcentage_metrique' => '%',
	'unite_pourcentage_standard' => '%',
	'unite_precipitation_metrique' => 'م‌م',
	'unite_precipitation_standard' => 'اينچ',
	'unite_pression_metrique' => 'م‌بار',
	'unite_pression_standard' => 'اينچ',
	'unite_temperature_metrique' => '°C',
	'unite_temperature_standard' => '°F',
	'unite_vitesse_metrique' => 'ك‌م/س',
	'unite_vitesse_standard' => 'م‌دس(مايل در ساعت)',

	// V
	'valeur_indeterminee' => 'ان/دي',
	'vent' => 'باد',
	'visibilite' => 'ديد',
];
