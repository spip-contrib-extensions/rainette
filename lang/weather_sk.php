<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/weather-rainette?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'meteo_1' => 'tropická búrka',
	'meteo_10' => 'mrznúci dážď',
	'meteo_11' => 'prehánky',
	'meteo_13' => 'prívaly snehu',
	'meteo_14' => 'jemné snehové prehánky',
	'meteo_15' => 'zvírený sneh',
	'meteo_16' => 'sneh',
	'meteo_17' => 'krupobitie',
	'meteo_18' => 'dážď so snehom',
	'meteo_19' => 'prach',
	'meteo_2' => 'hurikán',
	'meteo_20' => 'hmlisto',
	'meteo_21' => 'opar',
	'meteo_22' => 'hmla',
	'meteo_23' => 'nárazový vietor',
	'meteo_24' => 'veterno',
	'meteo_25' => 'chladno',
	'meteo_26' => 'zamračené',
	'meteo_27' => 'zväčša zamračené (noc)',
	'meteo_28' => 'zväčša zamračené (deň)',
	'meteo_29' => 'miestami zamračené (noc)',
	'meteo_3' => 'silné búrky',
	'meteo_30' => 'miestami zamračené (deň)',
	'meteo_31' => 'jasno (noc)',
	'meteo_32' => 'slnečno',
	'meteo_35' => 'dážď s krúpami',
	'meteo_36' => 'horúco',
	'meteo_37' => 'lokálne búrky',
	'meteo_38' => 'rozptýlené búrky',
	'meteo_4' => 'búrky',
	'meteo_44' => 'miestami zamračené',
	'meteo_5' => 'dážď so snehom',
	'meteo_6' => 'dážď a poľadovica',
	'meteo_7' => 'sneh a poľadovica',
	'meteo_8' => 'mrznúce mrholenie',
	'meteo_9' => 'mrholenie'
);
