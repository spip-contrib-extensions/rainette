<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/weather-rainette?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'meteo_1' => 'tempesta tropicale',
	'meteo_10' => 'pioggia gelata',
	'meteo_11' => 'rovesci',
	'meteo_13' => 'qualche fiocco',
	'meteo_14' => 'deboli rovesci di neve',
	'meteo_15' => 'bufera di neve',
	'meteo_16' => 'neve',
	'meteo_17' => 'grandine',
	'meteo_18' => 'nevischio',
	'meteo_19' => 'polvere',
	'meteo_2' => 'uragano',
	'meteo_20' => 'nebbioso',
	'meteo_21' => 'nebbia',
	'meteo_22' => 'nebbia',
	'meteo_23' => 'burrasca',
	'meteo_24' => 'vento',
	'meteo_25' => 'freddo',
	'meteo_26' => 'nuvoloso',
	'meteo_27' => 'chiaro di luna nuvoloso',
	'meteo_28' => 'nuvoloso',
	'meteo_29' => 'chiaro di luna e nuvole sparse',
	'meteo_3' => 'tempesta violenta',
	'meteo_30' => 'sole e nubi sparse',
	'meteo_31' => 'chiaro di luna',
	'meteo_32' => 'sole',
	'meteo_33' => 'chiaro di luna velato',
	'meteo_34' => 'sole velato',
	'meteo_35' => 'pioggia mista a grandine',
	'meteo_36' => 'calore',
	'meteo_37' => 'temporali isolati',
	'meteo_38' => 'temporali sparsi',
	'meteo_4' => 'tempesta',
	'meteo_44' => 'soleggiato',
	'meteo_5' => 'pioggia mista a neve',
	'meteo_6' => 'pioggia e ghiaccio',
	'meteo_7' => 'neve e ghiaccio',
	'meteo_8' => 'pioggerella gelata',
	'meteo_9' => 'pioggerella'
);
