<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/openmeteo-rainette?lang_cible=fr
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'meteo_0' => 'Ciel dégagé',
	'meteo_1' => 'Plutôt dégagé',
	'meteo_2' => 'Partiellement nuageux',
	'meteo_3' => 'Couvert',
	'meteo_45' => 'Brouillard',
	'meteo_48' => 'Brouillard déposant du givre',
	'meteo_51' => 'Légère Bruine',
	'meteo_53' => 'Bruine modérée',
	'meteo_55' => 'Forte bruine',
	'meteo_56' => 'Légère bruine verglaçante',
	'meteo_57' => 'Forte bruine verglaçante',
	'meteo_61' => 'Légère pluie',
	'meteo_63' => 'Pluie modérée',
	'meteo_65' => 'Forte pluie',
	'meteo_66' => 'Faible pluie verglaçante',
	'meteo_67' => 'Forte pluie verglaçante',
	'meteo_71' => 'Légère chute de neige',
	'meteo_73' => 'Chute de neige modérée',
	'meteo_75' => 'Forte chute de neige',
	'meteo_77' => 'Neige en grains',
	'meteo_80' => 'Légère averse de pluie',
	'meteo_81' => 'Averse de pluie modérée',
	'meteo_82' => 'Violente averse de pluie',
	'meteo_85' => 'Légère averse de neige',
	'meteo_86' => 'Forte averse de neige',
	'meteo_95' => 'Orage faible ou modéré',
	'meteo_96' => 'Orage faible ou modéré avec grêle',
	'meteo_99' => 'Orage fort avec grêle',
	'meteo_na' => 'n/a'
);
