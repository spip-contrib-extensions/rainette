<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/rainette.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'meteo_0' => 'tornado',
	'meteo_1' => 'tropical storm',
	'meteo_10' => 'freezing rain',
	'meteo_11' => 'showers',
	'meteo_12' => 'rain',
	'meteo_13' => 'snow flurries',
	'meteo_14' => 'light snow showers',
	'meteo_15' => 'blowing snow',
	'meteo_16' => 'snow',
	'meteo_17' => 'hail',
	'meteo_18' => 'sleet',
	'meteo_19' => 'dust',
	'meteo_2' => 'hurricane',
	'meteo_20' => 'foggy',
	'meteo_21' => 'haze',
	'meteo_22' => 'smoky',
	'meteo_23' => 'blustery',
	'meteo_24' => 'windy',
	'meteo_25' => 'cold',
	'meteo_26' => 'cloudy',
	'meteo_27' => 'mostly cloudy (night)',
	'meteo_28' => 'mostly cloudy (day)',
	'meteo_29' => 'partly cloudy (night)',
	'meteo_3' => 'severe thunderstorms',
	'meteo_30' => 'partly cloudy (day)',
	'meteo_31' => 'clear (night)',
	'meteo_32' => 'sunny',
	'meteo_33' => 'fair (night)',
	'meteo_34' => 'fair (day)',
	'meteo_35' => 'mixed rain and hail',
	'meteo_36' => 'hot',
	'meteo_37' => 'isolated thunderstorms',
	'meteo_38' => 'scattered thunderstorms',
	'meteo_39' => 'scattered showers (day)',
	'meteo_4' => 'thunderstorms',
	'meteo_40' => 'heavy rain',
	'meteo_41' => 'scattered snow showers (day)',
	'meteo_42' => 'heavy snow',
	'meteo_43' => 'blizzard',
	'meteo_44' => 'partly cloudy',
	'meteo_45' => 'scattered showers (night)',
	'meteo_46' => 'scattered snow showers (night)',
	'meteo_47' => 'scattered thundershowers',
	'meteo_5' => 'mixed rain and snow',
	'meteo_6' => 'mixed rain and sleet',
	'meteo_7' => 'mixed snow and sleet',
	'meteo_8' => 'freezing drizzle',
	'meteo_9' => 'drizzle'
);
