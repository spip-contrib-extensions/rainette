<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/rainette-rainette?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// C
	'coucher_soleil' => 'západ slnka',

	// D
	'demain' => 'zajtra',
	'derniere_maj' => 'aktualizované',
	'description' => 'Opis', # MODIF
	'direction_E' => 'východ',
	'direction_ENE' => 'východ severovýchod',
	'direction_ESE' => 'východ juhovýchod',
	'direction_N' => 'sever',
	'direction_NE' => 'severovýchod',
	'direction_NNE' => 'sever severovýchod',
	'direction_NNW' => 'sever severozápad',
	'direction_NW' => 'severozápad',
	'direction_S' => 'juh',
	'direction_SE' => 'juhovýchod',
	'direction_SSE' => 'juh juhovýchod',
	'direction_SSW' => 'juh juhozápad',
	'direction_SW' => 'juhozápad',
	'direction_W' => 'západ',
	'direction_WNW' => 'západ severozápad',
	'direction_WSW' => 'západ juhozápad',

	// H
	'humidite' => 'vlhkosť vzduchu',

	// J
	'jour' => 'deň',

	// L
	'latitude' => 'zemepisná šírka',
	'lever_soleil' => 'východ slnka',
	'longitude' => 'zemepisná dĺžka',

	// M
	'meteo' => 'počasie',
	'meteo_conditions' => 'súčasné poveternostné podmienky',
	'meteo_consultation' => 'Počasie pre @ville@ ',
	'meteo_de' => 'Počasie – @ville@ ',
	'meteo_na' => 'neznáme',
	'meteo_previsions' => 'predpovedané poveternostné podmienky',
	'meteo_previsions_aujourdhui' => 'predpoveď na dnes',
	'meteo_previsions_n_jours' => '@nbj@-dňová predpoveď',

	// N
	'nuit' => 'noc',

	// P
	'point_rosee' => 'dew point',
	'pression' => 'tlak',

	// R
	'risque_precipitation' => 'zrážky',

	// S
	'station_observation' => 'stanica',

	// T
	'temperature_max' => 'maximum',
	'temperature_min' => 'minimum',
	'temperature_ressentie' => 'na pocit',
	'tendance_symbole_falling' => '↓',
	'tendance_symbole_rising' => '↑',
	'tendance_symbole_steady' => '→',
	'tendance_texte_falling' => 'klesá',
	'tendance_texte_rising' => 'stúpa',
	'tendance_texte_steady' => 'stagnuje',

	// U
	'unite_angle_metrique' => '°',
	'unite_angle_standard' => '°',
	'unite_distance_metrique' => 'km',
	'unite_distance_standard' => 'míľ',
	'unite_pourcentage_metrique' => '%',
	'unite_pourcentage_standard' => '%',
	'unite_precipitation_metrique' => 'mm',
	'unite_precipitation_standard' => 'palcov',
	'unite_pression_metrique' => 'mbar',
	'unite_pression_standard' => 'palcov',
	'unite_temperature_metrique' => '°C',
	'unite_temperature_standard' => '°F',
	'unite_vitesse_metrique' => 'km/h',
	'unite_vitesse_standard' => 'm/h',

	// V
	'valeur_indeterminee' => 'N/A',
	'vent' => 'vietor',
	'visibilite' => 'viditeľnosť',
];
