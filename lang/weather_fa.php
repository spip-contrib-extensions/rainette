<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/weather-rainette?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'meteo_0' => 'توفان',
	'meteo_1' => 'توفان استوايي',
	'meteo_10' => 'باران سرد',
	'meteo_11' => 'رگبارها',
	'meteo_13' => 'رگبار برف',
	'meteo_14' => 'بارش سبك برف ',
	'meteo_15' => 'كولاك',
	'meteo_16' => 'برف',
	'meteo_17' => 'تگرگ',
	'meteo_18' => 'برف باران',
	'meteo_19' => 'گرد و خاك',
	'meteo_2' => 'تندباد',
	'meteo_20' => 'مه‌آلود',
	'meteo_21' => 'مه',
	'meteo_22' => 'دودآلود',
	'meteo_23' => 'توفاني',
	'meteo_24' => 'باد',
	'meteo_25' => 'سرد',
	'meteo_26' => 'ابري',
	'meteo_27' => 'شب بسيار ابري',
	'meteo_28' => 'روز بيشتر ابري',
	'meteo_29' => 'شب تا حدي ابري',
	'meteo_3' => 'توفان شديد',
	'meteo_30' => 'تا حدي ابري ',
	'meteo_31' => 'شب صاف',
	'meteo_32' => 'آفتابي',
	'meteo_33' => 'مهتابي ',
	'meteo_34' => 'روز خوب',
	'meteo_35' => 'باران و تگرگ',
	'meteo_36' => 'گرم',
	'meteo_37' => 'توفان پراكنده',
	'meteo_38' => 'توفان پراكنده',
	'meteo_4' => 'توفان',
	'meteo_44' => 'كمي تا قسمتي ابري',
	'meteo_5' => 'مخلوط باران و برف',
	'meteo_6' => 'مخلوط باران و برفباران',
	'meteo_7' => 'مخلوط برف و برفباران',
	'meteo_8' => 'بارن يخي ',
	'meteo_9' => 'بارش نم‌نم'
);
