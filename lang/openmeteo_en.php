<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/rainette.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'meteo_0' => 'Clear sky',
	'meteo_1' => 'Mainly clear',
	'meteo_2' => 'Partly cloudy',
	'meteo_3' => 'Overcast',
	'meteo_45' => 'Fog',
	'meteo_48' => 'Depositing rime fog',
	'meteo_51' => 'Light drizzle',
	'meteo_53' => 'Moderate drizzle',
	'meteo_55' => 'Dense intensity drizzle',
	'meteo_56' => 'Light freezing drizzle',
	'meteo_57' => 'Dense intensity freezing drizzle',
	'meteo_61' => 'Slight rain',
	'meteo_63' => 'Moderate rain',
	'meteo_65' => 'Heavy intensity rain',
	'meteo_66' => 'Light freezing rain',
	'meteo_67' => 'Heavy intensity freezing rain',
	'meteo_71' => 'Light snow fall',
	'meteo_73' => 'Moderate snow fall',
	'meteo_75' => 'Heavy intensity snow fall',
	'meteo_77' => 'Snow grains',
	'meteo_80' => 'Slight rain showers',
	'meteo_81' => 'Moderate rain showers',
	'meteo_82' => 'Violent rain showers',
	'meteo_85' => 'Slight snow showers',
	'meteo_86' => 'Heavy snow showers',
	'meteo_95' => 'Slight or moderate thunderstorm',
	'meteo_96' => 'Slight or moderate thunderstorm with hail',
	'meteo_99' => 'Heavy thunderstorm with hail',
	'meteo_na' => 'n/a'
);
