<?php
/**
 * Ce fichier contient les fonctions implémentant le service AerisWeather (aeris).
 * Ce service est capable de fournir des données au format JSON.
 *
 * Les fonctions qui suivent définissent l'API standard du service et sont appelées par la fonction
 * unique de chargement des données météorologiques `meteo_charger()`.
 *
 * @package SPIP\RAINETTE\SERVICES\AERIS
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Construit l'url de la requête correspondant au lieu, au type de données et à la configuration utilisateur
 * du service (par exemple, le code d'inscription, le format des résultats...).
 *
 * @uses langue_service_determiner()
 *
 * @param array  $lieu_normalise Lieu normalisé avec son format pour lequel on acquiert les données météorologiques.
 * @param string $mode           Type de données météorologiques. Les valeurs possibles sont `infos`, `conditions` ou `previsions`.
 * @param int    $periodicite    La périodicité horaire des prévisions :
 *                               - `24`, ... pour le mode `previsions` (mettre la bonne liste des périodicités acceptées)
 *                               - `0`, pour les modes `conditions` et `infos`
 * @param string $langue         Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration  Configuration complète du service, statique et utilisateur.
 *
 * @return string URL complète de la requête.
 */
function aeris_service2url(array $lieu_normalise, string $mode, int $periodicite, string $langue, array $configuration) : string {
	// On construit le mode et la période
	if ($mode === 'infos') {
		$demande = 'places/closest';
		$query = 'p=' . $lieu_normalise['id'] . '&limit=1';
	} elseif ($mode === 'conditions') {
		$demande = $mode . '/' . $lieu_normalise['id'];
		$query = 'for=now';
	} else {
		$demande = 'forecasts/' . $lieu_normalise['id'];
		$query = 'limit=' . $configuration['periodicites'][$periodicite]['max_jours'] * 24 / $periodicite;
		if ($periodicite === 12) {
			$query .= '&filter=daynight';
		}
	}

	return
		$configuration['endpoint_requete']
		. $demande
		. '?' . $query
		. '&client_id=' . $configuration['inscription']
		. '&client_secret=' . $configuration['secret'];
}

/**
 * Lit le bloc de réponse potentiellement constitutif d'une erreur pour déterminer si la requête est réellement en échec.
 *
 * @param array $erreur Sous-tableau de la réponse dans lequel vérifier une erreur de flux.
 *
 * @return bool `true` si une erreur est détectée, `false` sinon.
 */
function aeris_erreur_verifier(array $erreur) : bool {
	// Initialisation
	$est_erreur = false;

	// Une erreur possède toujours un code pour ce service
	if (!empty($erreur['code'])) {
		$est_erreur = true;
	}

	return $est_erreur;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu uniquement de la lecture du flux.
 *
 * @uses etat2resume_aeris()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               au service.
 */
function aeris_complement2conditions(array $tableau, string $langue, array $configuration) : array {
	if ($tableau) {
		// Compléter le tableau standard avec les états météorologiques calculés (résumé, icones locaux ou weather).
		$tableau = etat2resume_aeris($tableau, $langue, $configuration);
	}

	return $tableau;
}

/**
 * Modifie le flux des prévisions issu directement du service pour qu'il puisse être conforme
 * à la logique de normalisation qui s'en suivra.
 *
 * @param array  $flux          Flux non normalisé provenant du service
 * @param string $mode          Type de données météorologiques. Les valeurs possibles sont `infos`, `conditions` ou `previsions`.
 * @param int    $periodicite   Périodicité horaire des prévisions (24, 12, 6, 3 ou 1)
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Flux jour modifié
 */
function aeris_flux2flux(array $flux, string $mode, int $periodicite, array $configuration) : array {
	if (
		($mode === 'previsions')
		and $flux
	) {
		// Liste des données jour du flux : la liste est tenue à jour manuellement car il n'y a pas d'action
		// déterministe pour l'extraire de la configuration.
		$donnees_jour = [
			'dateTimeISO',
			'sunrise',
			'sunset',
			'maxTempC',
			'maxTempF',
			'minTempC',
			'minTempF',
		];

		// Les prévisions sont fournies en liste pour chaque période quelque soit le jour.
		// Il faut donc rétablir un tableau par jour et une structure heure pour chaque sous-période, ici 12h.
		$flux_modifie = [];
		$nb_periodes_jour = 24 / $periodicite;
		foreach ($flux as $_cle => $_valeurs) {
			// Détermination des index jour et heure
			$index_jour = intdiv($_cle, $nb_periodes_jour);
			$index_heure = $_cle % $nb_periodes_jour;

			// -- extraction des données jour : on ne peut pas utiliser array_intersect_key() car certaines
			//    données min et max ne sont pas présentes dans chaque flux jour, il faut les compléter petit
			//    à petit
			foreach ($donnees_jour as $_donnee) {
				if (isset($_valeurs[$_donnee])) {
					if (
						!isset($flux_modifie[$index_jour][$_donnee])
						or (null === $flux_modifie[$index_jour][$_donnee])
					) {
						$flux_modifie[$index_jour][$_donnee] = $_valeurs[$_donnee] ?? null;
					}
				} elseif (!isset($flux_modifie[$index_jour][$_donnee])) {
					$flux_modifie[$index_jour][$_donnee] = null;
				}
			}

			// -- extraction des données heure
			$flux_heure = array_diff_key($_valeurs, array_flip($donnees_jour));
			$flux_modifie[$index_jour]['hourly'][$index_heure] = $flux_heure;
		}

		// On renvoie le flux modifié
		$flux = $flux_modifie;
	}

	return $flux;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu
 * uniquement de la lecture du flux.
 *
 * @uses etat2resume_aeris()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 * @param int    $index_periode Index où trouver et ranger les données. Cet index n'est pas utilisé pour les conditions
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               du service.
 */
function aeris_complement2previsions(array $tableau, string $langue, array $configuration, int $index_periode) : array {
	if ($tableau) {
		// Compléter le tableau standard avec les états météorologiques calculés (résumé, icones locaux ou weather).
		$tableau = etat2resume_aeris($tableau, $langue, $configuration);
	}

	return $tableau;
}

// ---------------------------------------------------------------------------------------------
// Les fonctions qui suivent sont des utilitaires uniquement appelées par les fonctions de l'API
// ---------------------------------------------------------------------------------------------.

/**
 * Calcule les états en fonction des états météorologiques natifs fournis par le service.
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service. Le tableau est mis à jour et renvoyé à l'appelant.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standard mis à jour.
 */
function etat2resume_aeris(array $tableau, string $langue, array $configuration) : array {
	if (
		isset($tableau['code_meteo'])
		and (null !== $tableau['code_meteo'])
	) {
		// Determination de l'indicateur jour/nuit qui permet de choisir le bon icone
		if ($tableau['jour_meteo'] === false) {
			// C'est la nuit
			$tableau['periode'] = 1;
		} else {
			// C'est le jour
			$tableau['periode'] = 0;
		}

		// Mise à jour de la donnée 'code' initialisée avec le code météo natif mais que l'on va écraser
		// -> on utilise le nom de l'icone pour calculer la donnée 'code' en distinguant le cas où il indique la nuit (n final)
		// -> ce code va servir au transcodage de l'icone uniquement
		$codes = array_keys($configuration['icones']['transcodages']);
		// -- on extrait le nom de l'icone et on initialise le code méteo avec
		$icone_meteo = strtolower(basename($tableau['icon_meteo'], '.png'));
		$code = $icone_meteo;
		// -- on teste si il existe un 'n' en fin de nom sinon on est sur que c'est le code qu'on veut récupérer
		if (substr($icone_meteo, -1, 1) === 'n') {
			$code = substr($icone_meteo, 0, strlen($icone_meteo) - 1);
			if (!in_array($code, $codes)) {
				$code = $icone_meteo;
			}
		}
		// -- on stocke ce résultat comme le code méteo calculé qui permettra de trouver l'icone associé, uniquement
		$tableau['code'] = $code;
		// -- on réplique ce code dans l'icone
		$tableau['icone']['code'] = $code;

		// Détermination du résumé à afficher :
		// - si la langue est traduite par un module spip (donc non fournie nativement, on utilise le code météo natif
		//   pour s'indexer dans un fichier de langue
		// - sinon, on utilise la donnée native 'desc_meteo' qui est toujours en anglais
		if (in_array($langue, langue_spip_lister_modules($configuration['alias']))) {
			// On construit le code météo servant à définir l'item de langue à partir du code natif
			// -- le code météo natif est composé de 3 sous-codes séparés par ':'
			[$couverture, $intensite, $meteo] = explode(':', $tableau['code_meteo']);
			// -- on réduit l'intensité à 3 valeurs au lieu de 5 (VL et L deviennent L, VH et H deviennent H, vide devient
			//    M pour moderate si la météo n'est pas un état des nuages.
			if ($intensite) {
				$intensite = str_replace('V', '', $intensite);
			} elseif (!in_array($meteo, ['CL', 'FW', 'SC', 'BK', 'OV'])) {
				$intensite = 'M';
			}
			// -- on ne considère que l'intensité et la météo pour limiter la combinatoire de traductions
			$code_meteo = $intensite . ($intensite ? '_' : '') . $meteo;
			$code_meteo = strtolower($code_meteo);

			// Extraire la traduction
			$tableau['resume'] = _T("aeris:meteo_{$code_meteo}", ['spip_lang' => $langue]);
		} else {
			// On renvoie le résumé natif
			$tableau['resume'] = spip_ucfirst($tableau['desc_meteo']);
		}

		// Determination de l'icone qui sera affiché.
		// -- Etant donné que le service ne fournit pas d'icone via l'API l'icone est calculé par la fonction commune
	}

	return $tableau;
}
