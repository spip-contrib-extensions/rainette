<?php
/**
 * Ce fichier contient la configuration et l'ensemble des fonctions implémentant le service World Weather Online (wwo).
 * Ce service est capable de fournir des données au format XML ou JSON. Néanmoins, l'API actuelle du plugin utilise
 * uniquement le format JSON.
 *
 * Les fonctions qui suivent définissent l'API standard du service et sont appelées par la fonction
 * unique de chargement des données météorologiques `meteo_charger()`.
 *
 * @package SPIP\RAINETTE\SERVICES\WWO
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Construit l'url de la requête correspondant au lieu, au type de données et à la configuration utilisateur
 * du service (par exemple, le code d'inscription, le format des résultats...).
 *
 * @uses langue_service_determiner()
 *
 * @param array  $lieu_normalise Lieu normalisé avec son format pour lequel on acquiert les données météorologiques.
 * @param string $mode           Type de données météorologiques. Les valeurs possibles sont `infos`, `conditions` ou `previsions`.
 * @param int    $periodicite    La périodicité horaire des prévisions :
 *                               - `24`, `12`, `3`, `1` pour le mode `previsions`
 *                               - `0`, pour les modes `conditions` et `infos`
 * @param string $langue         Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration  Configuration complète du service, statique et utilisateur.
 *
 * @return string URL complète de la requête.
 */
function wwo_service2url(array $lieu_normalise, string $mode, int $periodicite, string $langue, array $configuration) : string {
	// Identification de la langue du resume.
	include_spip('inc/rainette_normaliser');
	$code_langue = langue_service_determiner($langue, $configuration);

	$url = $configuration['endpoint_requete']
		   . '?key=' . $configuration['inscription']
		   . '&format=' . $configuration['format_flux']
		   . '&extra=localObsTime'
		   . '&lang=' . $code_langue
		   . '&q=' . $lieu_normalise['id'];

	if ($mode === 'infos') {
		$url .= '&includeLocation=yes&cc=no&fx=no';
	} elseif ($mode === 'conditions') {
		$url .= '&cc=yes&fx=no';
	} else {
		$url .= '&cc=no&fx=yes'
				. '&num_of_days=' . $configuration['periodicites'][$periodicite]['max_jours']
				. '&tp=' . (string) $periodicite;
	}

	return $url;
}

/**
 * Lit le bloc de réponse potentiellement constitutif d'une erreur pour déterminer si la requête est réellement en échec.
 *
 * @param array $erreur Sous-tableau de la réponse dans lequel vérifier une erreur de flux.
 *
 * @return bool `true` si une erreur est détectée, `false` sinon.
 */
function wwo_erreur_verifier(array $erreur) : bool {
	// Initialisation
	$est_erreur = false;

	// Une erreur est uniquement décrite par un message.
	if (!empty($erreur['message'])) {
		$est_erreur = true;
	}

	return $est_erreur;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu
 * uniquement de la lecture du flux.
 *
 * @uses etat2resume_wwo()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               au service.
 */
function wwo_complement2conditions(array $tableau, string $langue, array $configuration) : array {
	if ($tableau) {
		// Compléter le tableau standard avec les états météorologiques calculés
		$tableau = etat2resume_wwo($tableau, $langue, $configuration);
	}

	return $tableau;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu
 * uniquement de la lecture du flux.
 *
 * @uses etat2resume_wwo()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 * @param int    $index_periode Index où trouver et ranger les données. Cet index n'est pas utilisé pour les conditions
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               du service.
 */
function wwo_complement2previsions(array $tableau, string $langue, array $configuration, int $index_periode) : array {
	if (($tableau) and ($index_periode > -1)) {
		// Compléter le tableau standard avec les états météorologiques calculés
		$tableau = etat2resume_wwo($tableau, $langue, $configuration);
	}

	return $tableau;
}

// ---------------------------------------------------------------------------------------------
// Les fonctions qui suivent sont des utilitaires uniquement appelées par les fonctions de l'API
// ---------------------------------------------------------------------------------------------.

/**
 * Calcule les états en fonction des états météorologiques natifs fournis par le service.
 *
 * @uses icone_normaliser_chemin()
 * @uses icone_weather_normaliser_chemin()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service. Le tableau est mis à jour et renvoyé à l'appelant.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standard mis à jour.
 */
function etat2resume_wwo(array $tableau, string $langue, array $configuration) : array {
	if (
		$tableau['code_meteo']
		and $tableau['icon_meteo']
	) {
		// Determination de l'indicateur jour/nuit qui permet de choisir le bon icone
		// Pour ce service aucun indicateur n'est disponible
		// -> on utilise le nom de l'icone qui contient toujours l'indication "night" pour la nuit
		$icone = basename($tableau['icon_meteo']);
		if (strpos($icone, '_night') === false) {
			// C'est le jour
			$tableau['periode'] = 0;
		} else {
			// C'est la nuit
			$tableau['periode'] = 1;
		}

		// Détermination du résumé à afficher.
		// -> Pour wwo, la description est dans trad_meteo et non dans desc_meteo sauf pour l'anglais qui est nativement
		//    dans desc_meteo.
		$tableau['resume'] = $tableau['trad_meteo']
			? spip_ucfirst($tableau['trad_meteo'])
			: spip_ucfirst($tableau['desc_meteo']);

		// Determination de l'icone qui sera affiché dans le cas où c'est l'API qui le fournit
		// -- on calcule le chemin complet de l'icone.
		if ($configuration['condition'] === $configuration['alias']) {
			// On affiche l'icône natif fourni par le service et désigné par son url
			// en faisant une copie locale dans IMG/.
			include_spip('inc/distant');
			$tableau['icone']['source'] = copie_locale($tableau['icon_meteo']);
		}
	}

	return $tableau;
}
