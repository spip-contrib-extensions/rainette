<?php
/**
 * Ce fichier contient les fonctions implémentant le service Méteo-Concept (meteoconcept).
 * Ce service est capable de fournir des données au format JSON ou XML mais seul le JSON est exploité par Rainette.
 *
 * Les fonctions qui suivent définissent l'API standard du service et sont appelées par la fonction
 * unique de chargement des données météorologiques `meteo_charger()`.
 *
 * @package SPIP\RAINETTE\SERVICES\METEOCONCEPT
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Construit l'url de la requête correspondant au lieu, au type de données et à la configuration utilisateur
 * du service (par exemple, le code d'inscription, le format des résultats...).
 *
 * @uses langue_service_determiner()
 *
 * @param array  $lieu_normalise Lieu normalisé avec son format pour lequel on acquiert les données météorologiques.
 * @param string $mode           Type de données météorologiques. Les valeurs possibles sont `infos`, `conditions` ou `previsions`.
 * @param int    $periodicite    La périodicité horaire des prévisions :
 *                               - `24`, ... pour le mode `previsions` (mettre la bonne liste des périodicités acceptées)
 *                               - `0`, pour les modes `conditions` et `infos`
 * @param string $langue         Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration  Configuration complète du service, statique et utilisateur. *
 *
 * @return string URL complète de la requête.
 */
function meteoconcept_service2url(array $lieu_normalise, string $mode, int $periodicite, string $langue, array $configuration) : string {
	// Le service accepte les coordonnées et la code INSEE
	if ($lieu_normalise['format'] === 'latitude_longitude') {
		$query = "latlng={$lieu_normalise['id']}";
	} else {
		// Format INSEE
		$query = "insee={$lieu_normalise['id']}";
	}

	// On construit le mode
	if ($mode === 'infos') {
		$demande = 'location/city';
		// On demande de considérer les pays étrangers si on identifie la ville par ses coordonnées
		if ($lieu_normalise['format'] === 'latitude_longitude') {
			$query .= '&world=true';
		}
	} elseif ($mode === 'conditions') {
		$demande = 'forecast/nextHours';
		$query .= '&hourly=true';
	} else {
		$demande = 'forecast/daily';
	}

	return
		$configuration['endpoint_requete']
		. $demande
		. '?token=' . $configuration['inscription']
		. '&' . $query;
}

/**
 * Lit le bloc de réponse potentiellement constitutif d'une erreur pour déterminer si la requête est réellement en échec.
 *
 * @param array $erreur Sous-tableau de la réponse dans lequel vérifier une erreur de flux.
 *
 * @return bool `true` si une erreur est détectée, `false` sinon.
 */
function meteoconcept_erreur_verifier(array $erreur) : bool {
	// Initialisation
	$est_erreur = false;

	// Une erreur est uniquement décrite par un message.
	if (!empty($erreur['message'])) {
		$est_erreur = true;
	}

	return $est_erreur;
}

/**
 * Complète par des données spécifiques au service le tableau des informations issu
 * uniquement de la lecture du flux.
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standardisé des informations complété par les données spécifiques
 *               du service.
 */
function meteoconcept_complement2infos(array $tableau, string $langue, array $configuration) : array {
	if ($tableau) {
		if (empty($tableau['pays'])) {
			// France : le pays n'est pas précisé, on le rajoute
			$tableau['pays'] = 'France';
			$tableau['pays_iso2'] = 'FR';
		} else {
			// Pays limitrophes : Belgique, Luxembourg et Andorre. On rajoute l'ISO alpha-2
			$pays = strtolower($tableau['pays']);
			switch($pays) {
				case 'belgique':
					$tableau['pays_iso2'] = 'BE';
					break;
				case 'luxembourg':
					$tableau['pays_iso2'] = 'LU';
					break;
				case 'andorre':
					$tableau['pays_iso2'] = 'AD';
					break;
			}
		}
	}

	return $tableau;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu
 * uniquement de la lecture du flux.
 *
 * @uses angle2direction()
 * @uses temperature2ressenti()
 * @uses etat2resume_meteoconcept()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               au service.
 */
function meteoconcept_complement2conditions(array $tableau, string $langue, array $configuration) : array {
	if ($tableau) {
		include_spip('inc/rainette_convertir');
		// Calcul de la direction du vent (16 points), celle-ci n'étant pas fournie nativement par owm
		$tableau['direction_vent'] = angle2direction($tableau['angle_vent']);

		// Calcul de la température ressentie forcément fournie en °C
		$tableau['temperature_ressentie'] = temperature2ressenti(
			$tableau['temperature_reelle'],
			$tableau['vitesse_vent'],
			$tableau['point_rosee']
		);

		// Conversion en système impérial : le service ne fournit que des données en système métrique
		if ($configuration['unite'] !== 'm') {
			$tableau['temperature_reelle'] = celsius2farenheit($tableau['temperature_reelle']);
			$tableau['temperature_ressentie'] = celsius2farenheit($tableau['temperature_ressentie']);
			$tableau['vitesse_vent'] = kilometre2mile($tableau['vitesse_vent']);
			$tableau['rafale_vent'] = kilometre2mile($tableau['rafale_vent']);
			$tableau['precipitation'] = millimetre2inch($tableau['precipitation']);
			$tableau['point_rosee'] = celsius2farenheit($tableau['point_rosee']);
			$tableau['pression'] = millibar2inch($tableau['pression']);
		}

		// Compléter le tableau standard avec les états météorologiques calculés
		$tableau = etat2resume_meteoconcept($tableau, $langue, $configuration);
	}

	return $tableau;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu
 * uniquement de la lecture du flux.
 *
 * @uses etat2resume_meteoconcept()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 * @param int    $index_periode Index où trouver et ranger les données. Cet index n'est pas utilisé pour les conditions
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               du service.
 */
function meteoconcept_complement2previsions(array $tableau, string $langue, array $configuration, int $index_periode) : array {
	if (($tableau) and ($index_periode > -1)) {
		include_spip('inc/rainette_convertir');
		// Calcul de la direction du vent (16 points), celle-ci n'étant pas fournie nativement par owm
		$tableau['direction_vent'] = angle2direction($tableau['angle_vent']);

		// Conversion en système impérial : le service ne fournit que des données en système métrique
		if ($configuration['unite'] !== 'm') {
			$tableau['temperature_max'] = celsius2farenheit($tableau['temperature_max']);
			$tableau['temperature_min'] = celsius2farenheit($tableau['temperature_min']);
			$tableau['vitesse_vent'] = kilometre2mile($tableau['vitesse_vent']);
			$tableau['rafale_vent'] = kilometre2mile($tableau['rafale_vent']);
			$tableau['precipitation'] = millimetre2inch($tableau['precipitation']);
		}

		// Compléter le tableau standard avec les états météorologiques calculés
		$tableau = etat2resume_meteoconcept($tableau, $langue, $configuration);
	}

	return $tableau;
}

// ---------------------------------------------------------------------------------------------
// Les fonctions qui suivent sont des utilitaires uniquement appelées par les fonctions de l'API
// ---------------------------------------------------------------------------------------------.

/**
 * Calcule les états en fonction des états météorologiques natifs fournis par le service.
 *
 * @uses icone_weather_normaliser_chemin()
 *
 * @param array  &$tableau      Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service. Le tableau est mis à jour et renvoyé à l'appelant.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standard mis à jour.
 */
function etat2resume_meteoconcept(array $tableau, string $langue, array $configuration) : array {
	if ($tableau) {
		// Determination de l'indicateur jour/nuit qui permet de choisir le bon icone
		// Pour ce service aucun indicateur n'est disponible, on positionne la période à 0 tout le temps
		$tableau['periode'] = 0;

		// Prévisions uniquement : détermination du résumé à afficher:
		// - Meteo-Concept ne fourni pas de résumé dans l'API, seul le code est fourni. Rainette construit donc
		//   un fichier de langue spécifique initialisé en français par le tableau fourni par le service.
		$tableau['resume'] = _T('meteoconcept:meteo_' . $tableau['code_meteo'], ['spip_lang' => $langue]);

		// Determination de l'icone qui sera affiché.
		// -- Etant donné que le service ne fournit pas d'icone via l'API l'icone est calculé par la fonction
		//    commune
	}

	return $tableau;
}
