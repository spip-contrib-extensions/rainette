<?php
/**
 * Ce fichier contient l'ensemble des constantes et fonctions implémentant le service Open Weather Map (owm).
 * Ce service fournit des données au format XML ou JSON mais Rainette utilise uniquement le JSON.
 *
 * Les fonctions qui suivent définissent l'API standard du service et sont appelées par la fonction
 * unique de chargement des données météorologiques `meteo_charger()`.
 *
 * @package SPIP\RAINETTE\SERVICES\OWM
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Construit l'url de la requête correspondant au lieu, au type de données et à la configuration utilisateur
 * du service (par exemple, le code d'inscription, le format des résultats...).
 *
 * @uses langue_service_determiner()
 *
 * @param array  $lieu_normalise Lieu normalisé avec son format pour lequel on acquiert les données météorologiques.
 * @param string $mode           Type de données météorologiques. Les valeurs possibles sont `infos`, `conditions` ou `previsions`.
 * @param int    $periodicite    La périodicité horaire des prévisions :
 *                               - `24` pour le mode `previsions`
 *                               - `0`, pour les modes `conditions` et `infos`
 * @param string $langue         Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration  Configuration complète du service, statique et utilisateur.
 *
 * @return string URL complète de la requête.
 */
function owm_service2url(array $lieu_normalise, string $mode, int $periodicite, string $langue, array $configuration) : string {
	// Determination de la demande
	$demande = ($mode === 'previsions') ? 'forecast' : 'weather';
	if ($periodicite === 24) {
		$demande .= '/daily';
	}

	// Identification de la langue du resume.
	include_spip('inc/rainette_normaliser');
	$code_langue = langue_service_determiner($langue, $configuration);

	// On normalise le lieu et on récupère son format.
	// Le service accepte la format ville,pays et le format latitude,longitude
	if ($lieu_normalise['format'] === 'latitude_longitude') {
		[$latitude, $longitude] = explode(',', $lieu_normalise['id']);
		$query = "lat={$latitude}&lon={$longitude}";
	} elseif ($lieu_normalise['format'] === 'city_id') {
		// City ID
		$query = "id={$lieu_normalise['id']}";
	} else {
		// Format ville,pays
		$query = "q={$lieu_normalise['id']}";
	}

	return
		$configuration['endpoint_requete']
		. $demande . '?'
		. $query
		. '&mode=' . $configuration['format_flux']
		. '&units=' . ($configuration['unite'] === 'm' ? 'metric' : 'imperial')
		. ((($mode === 'previsions') and ($periodicite === 24))
			? '&cnt=' . $configuration['periodicites'][$periodicite]['max_jours']
			: '')
		. '&lang=' . $code_langue
		. ($configuration['inscription'] ? '&appid=' . $configuration['inscription'] : '');
}

/**
 * Lit le bloc de réponse potentiellement constitutif d'une erreur pour déterminer si la requête est réellement en échec.
 *
 * @param array $erreur Sous-tableau de la réponse dans lequel vérifier une erreur de flux.
 *
 * @return bool `true` si une erreur est détectée, `false` sinon.
 */
function owm_erreur_verifier(array $erreur) : bool {
	// Initialisation
	$est_erreur = false;

	// Pour OWM une erreur possède deux attributs, le code et le message.
	// Néanmoins, le code 200 est aussi renvoyé pour dire ok sans message.
	// => il faut donc écarter ce cas d'une erreur.
	if (!empty($erreur['code']) and !empty($erreur['message']) and ($erreur['code'] != '200')) {
		$est_erreur = true;
	}

	return $est_erreur;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu
 * uniquement de la lecture du flux.
 *
 * @uses angle2direction()
 * @uses metre2kilometre()
 * @uses metre_seconde2kilometre_heure()
 * @uses etat2resume_owm()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               au service.
 */
function owm_complement2conditions(array $tableau, string $langue, array $configuration) : array {
	if ($tableau) {
		include_spip('inc/rainette_convertir');
		// Calcul de la direction du vent (16 points), celle-ci n'étant pas fournie nativement par owm
		$tableau['direction_vent'] = angle2direction($tableau['angle_vent']);
		// On convertit aussi la visibilité en km car elle est fournie en mètres quelque soit le système d'unité choisi.
		$tableau['visibilite'] = metre2kilometre($tableau['visibilite']);

		if ($configuration['unite'] === 'm') {
			// Vitesse du vent en km/h plutôt qu'en m/s si on est en système métrique.
			$tableau['vitesse_vent'] = metre_seconde2kilometre_heure($tableau['vitesse_vent']);
			$tableau['rafale_vent'] = metre_seconde2kilometre_heure($tableau['rafale_vent']);
		} else {
			// La pression et la visibilité sont toujours renvoyée en système métrique
			$tableau['visibilite'] = kilometre2mile($tableau['visibilite']);
			$tableau['pression'] = millibar2inch($tableau['pression']);
		}

		// Compléter le tableau standard avec les états météorologiques calculés
		$tableau = etat2resume_owm($tableau, $langue, $configuration);
	}

	return $tableau;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu
 * uniquement de la lecture du flux.
 *
 * @uses angle2direction()
 * @uses metre_seconde2kilometre_heure()
 * @uses etat2resume_owm()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 * @param int    $index_periode Index où trouver et ranger les données. Cet index n'est pas utilisé pour les conditions
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               du service.
 */
function owm_complement2previsions(array $tableau, string $langue, array $configuration, int $index_periode) : array {
	if (($tableau) and ($index_periode > -1)) {
		include_spip('inc/rainette_convertir');
		// Calcul de la direction du vent (16 points), celle-ci n'étant pas fournie nativement par owm
		$tableau['direction_vent'] = angle2direction($tableau['angle_vent']);

		// Vérifier les précipitations. Pour les prévisions, OWM renvoie le champ rain uniquement si il est
		// différent de zéro. Il faut donc rétablir la valeur zéro dans ce cas pour éviter d'avoir N/D lors de
		// l'affichage.
		if ($tableau['precipitation'] === '') {
			$tableau['precipitation'] = (int) 0;
		}

		if ($configuration['unite'] === 'm') {
			// Vitesse du vent en km/h plutôt qu'en m/s si on est en système métrique.
			$tableau['vitesse_vent'] = metre_seconde2kilometre_heure($tableau['vitesse_vent']);
			$tableau['rafale_vent'] = metre_seconde2kilometre_heure($tableau['rafale_vent']);
		} else {
			// Précipitation et pression sont toujours renvoyées en système métrique
			$tableau['precipitation'] = millimetre2inch($tableau['precipitation']);
			$tableau['pression'] = millibar2inch($tableau['pression']);
		}

		// Compléter le tableau standard avec les états météorologiques calculés
		$tableau = etat2resume_owm($tableau, $langue, $configuration);
	}

	return $tableau;
}

// ---------------------------------------------------------------------------------------------
// Les fonctions qui suivent sont des utilitaires uniquement appelées par les fonctions de l'API
// ---------------------------------------------------------------------------------------------.

/**
 * Calcule les états en fonction des états météorologiques natifs fournis par le service.
 *
 * @uses icone_normaliser_chemin()
 * @uses icone_weather_normaliser_chemin()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service. Le tableau est mis à jour et renvoyé à l'appelant.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standard mis à jour.
 */
function etat2resume_owm(array $tableau, string $langue, array $configuration) : array {
	if (
		$tableau['code']
		and $tableau['icon_meteo']
	) {
		// Determination de l'indicateur jour/nuit qui permet de choisir le bon icone
		// Pour ce service le nom du fichier icone finit toujours par "d" pour le jour et
		// par "n" pour la nuit.
		if (strpos($tableau['icon_meteo'], 'n') === false) {
			// C'est le jour
			$tableau['periode'] = 0;
		} else {
			// C'est la nuit
			$tableau['periode'] = 1;
		}

		// Détermination du résumé à afficher.
		$tableau['resume'] = spip_ucfirst($tableau['desc_meteo']);

		// Determination de l'icone qui sera affiché dans le cas où c'est l'API qui le fournit
		// -- on calcule le chemin complet de l'icone.
		if ($configuration['condition'] === $configuration['alias']) {
			// On affiche l'icône natif fourni par le service et désigné par son url
			// en faisant une copie locale dans IMG/.
			include_spip('inc/distant');
			$url = $configuration['endpoint_icone']
				. (substr($configuration['theme'], 0, 1) === 'n' ? 'n' : '')
				. '/' . $tableau['icon_meteo']
				. (substr($configuration['theme'], 1, 2) === '2x' ? '@2x' : '')
				. '.png';
			$tableau['icone']['source'] = copie_locale($url);
		}
	}

	return $tableau;
}
