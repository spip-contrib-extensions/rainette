<?php
/**
 * Ce fichier contient la configuration et l'ensemble des fonctions implémentant le service Weatherbit.io (weatherbit).
 * Ce service est capable de fournir des données au format JSON.
 *
 * Les fonctions qui suivent définissent l'API standard du service et sont appelées par la fonction
 * unique de chargement des données météorologiques `meteo_charger()`.
 *
 * @package SPIP\RAINETTE\SERVICES\WEATHERBIT
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Construit l'url de la requête correspondant au lieu, au type de données et à la configuration utilisateur
 * du service (par exemple, le code d'inscription, le format des résultats...).
 *
 * @uses langue_service_determiner()
 *
 * @param array  $lieu_normalise Lieu normalisé avec son format pour lequel on acquiert les données météorologiques.
 * @param string $mode           Type de données météorologiques. Les valeurs possibles sont `infos`, `conditions` ou `previsions`.
 * @param int    $periodicite    La périodicité horaire des prévisions :
 *                               - `24`, `3` ou `1` pour le mode `previsions`
 *                               - `0`, pour les modes `conditions` et `infos`
 * @param string $langue         Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration  Configuration complète du service, statique et utilisateur.
 *
 * @return string URL complète de la requête.
 */
function weatherbit_service2url(array $lieu_normalise, string $mode, int $periodicite, string $langue, array $configuration) : string {
	// Identification de la langue du resume.
	include_spip('inc/rainette_normaliser');
	$code_langue = langue_service_determiner($langue, $configuration);

	// Le service accepte la format ville,pays, le format latitude,longitude et le format adresse IP.
	if ($lieu_normalise['format'] === 'latitude_longitude') {
		[$latitude, $longitude] = explode(',', $lieu_normalise['id']);
		$localisation = "lat={$latitude}&lon={$longitude}";
	} elseif ($lieu_normalise['format'] === 'city_id') {
		// City ID
		$localisation = "city_id={$lieu_normalise['id']}";
	} else {
		// Format ville,pays
		$elements = explode(',', $lieu_normalise['id']);
		$localisation = "city={$elements[0]}";
		if (count($elements) === 2) {
			// Le pays est précisé, il faut l'inclure dans un attribut paramètre spécifique 'country'.
			$localisation .= "&country={$elements[1]}";
		}
	}

	// Détermination du paramètre constitutif de la demande
	if ($mode === 'previsions') {
		$demande = 'forecast/';
		if ($periodicite === 24) {
			$demande .= 'daily';
		} elseif ($periodicite === 1) {
			$demande .= 'hourly';
		} else {
			// Forcément 3 heures
			$demande .= '3hourly';
		}
	} else {
		$demande = 'current';
	}

	return
		$configuration['endpoint_requete']
		. $demande . '?'
		. $localisation
		. '&lang=' . $code_langue
		. '&units=' . ($configuration['unite'] === 'm' ? 'M' : 'I')
		. '&key=' . $configuration['inscription'];
}

/**
 * Lit le bloc de réponse potentiellement constitutif d'une erreur pour déterminer si la requête est réellement en échec.
 *
 * @param array $erreur Sous-tableau de la réponse dans lequel vérifier une erreur de flux.
 *
 * @return bool `true` si une erreur est détectée, `false` sinon.
 */
function weatherbit_erreur_verifier(array $erreur) : bool {
	// Initialisation
	$est_erreur = false;

	// Une erreur est toujours décrite par un unique message.
	if (!empty($erreur['message'])) {
		$est_erreur = true;
	}

	return $est_erreur;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu
 * uniquement de la lecture du flux.
 *
 * @uses angle2direction()
 * @uses metre_seconde2kilometre_heure()
 * @uses etat2resume_weatherbit()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               au service.
 */
function weatherbit_complement2conditions(array $tableau, string $langue, array $configuration) : array {
	if ($tableau) {
		include_spip('inc/rainette_convertir');
		// Calcul de la direction du vent (16 points) car l'indicateur fourni change en fonction de la langue.
		$tableau['direction_vent'] = angle2direction($tableau['angle_vent']);

		if ($configuration['unite'] === 'm') {
			// Vitesse du vent en km/h plutôt qu'en m/s si on est en système métrique.
			$tableau['vitesse_vent'] = metre_seconde2kilometre_heure($tableau['vitesse_vent']);
			$tableau['rafale_vent'] = metre_seconde2kilometre_heure($tableau['rafale_vent']);
		} else {
			// La pression et la visibilité sont toujours renvoyée en système métrique
			$tableau['visibilite'] = kilometre2mile($tableau['visibilite']);
			$tableau['pression'] = millibar2inch($tableau['pression']);
		}

		// Compléter le tableau standard avec les états météorologiques calculés
		$tableau = etat2resume_weatherbit($tableau, $langue, $configuration);
	}

	return $tableau;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu
 * uniquement de la lecture du flux.
 *
 * @uses angle2direction()
 * @uses metre_seconde2kilometre_heure()
 * @uses etat2resume_weatherbit()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 * @param int    $index_periode Index où trouver et ranger les données. Cet index n'est pas utilisé pour les conditions
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               du service.
 */
function weatherbit_complement2previsions(array $tableau, string $langue, array $configuration, int $index_periode) : array {
	if (($tableau) and ($index_periode > -1)) {
		include_spip('inc/rainette_convertir');
		// Calcul de la direction du vent (16 points) car l'indicateur fourni change en fonction de la langue.
		$tableau['direction_vent'] = angle2direction($tableau['angle_vent']);

		if ($configuration['unite'] === 'm') {
			// Vitesse du vent en km/h plutôt qu'en m/s si on est en système métrique.
			$tableau['vitesse_vent'] = metre_seconde2kilometre_heure($tableau['vitesse_vent']);
			$tableau['rafale_vent'] = metre_seconde2kilometre_heure($tableau['rafale_vent']);
		} else {
			// La pression et la visibilité sont toujours renvoyée en système métrique
			$tableau['visibilite'] = kilometre2mile($tableau['visibilite']);
			$tableau['pression'] = millibar2inch($tableau['pression']);
		}

		// Compléter le tableau standard avec les états météorologiques calculés
		$tableau = etat2resume_weatherbit($tableau, $langue, $configuration);
	}

	return $tableau;
}

// ---------------------------------------------------------------------------------------------
// Les fonctions qui suivent sont des utilitaires uniquement appelées par les fonctions de l'API
// ---------------------------------------------------------------------------------------------.

/**
 * Calcule les états en fonction des états météorologiques natifs fournis par le service.
 *
 * @uses icone_normaliser_chemin()
 * @uses icone_weather_normaliser_chemin()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service. Le tableau est mis à jour et renvoyé à l'appelant.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standard mis à jour.
 */
function etat2resume_weatherbit(array $tableau, string $langue, array $configuration) : array {
	if (
		$tableau['code_meteo']
		and $tableau['icon_meteo']
	) {
		// Determination de l'indicateur jour/nuit qui permet de choisir le bon icône.
		// - on utilise l'indicateur fourni par le service si il existe sinon l'icone
		if ($tableau['jour_meteo']) {
			$tableau['periode'] = $tableau['jour_meteo'] === 'd' ? 0 : 1;
		} else {
			$tableau['periode'] = strpos($tableau['icon_meteo'], 'n') === false ? 0 : 1;
		}

		// Détermination du résumé à afficher.
		$tableau['resume'] = spip_ucfirst($tableau['desc_meteo']);

		// Determination de l'icone qui sera affiché dans le cas où c'est l'API qui le fournit
		// -- on calcule le chemin complet de l'icone.
		if ($configuration['condition'] === $configuration['alias']) {
			// On affiche l'icône natif fourni par le service et désigné par son url
			// en faisant une copie locale dans IMG/.
			include_spip('inc/distant');
			$url = $configuration['endpoint_icone'] . '/' . $tableau['icon_meteo'] . '.png';
			$tableau['icone']['source'] = copie_locale($url);
		}
	}

	return $tableau;
}
