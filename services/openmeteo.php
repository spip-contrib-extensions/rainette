<?php
/**
 * Ce fichier contient les fonctions implémentant le service Open-Meteo (openmeteo).
 * Ce service est capable de fournir des données au format JSON.
 *
 * Les fonctions qui suivent définissent l'API standard du service et sont appelées par la fonction
 * unique de chargement des données météorologiques `meteo_charger()`.
 *
 * @package SPIP\RAINETTE\SERVICES\OPENMETEO
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Construit l'url de la requête correspondant au lieu, au type de données et à la configuration utilisateur
 * du service (par exemple, le code d'inscription, le format des résultats...).
 *
 * @uses langue_service_determiner()
 *
 * @param array  $lieu_normalise Lieu normalisé avec son format pour lequel on acquiert les données météorologiques.
 * @param string $mode           Type de données météorologiques. Les valeurs possibles sont `infos`, `conditions` ou `previsions`.
 * @param int    $periodicite    La périodicité horaire des prévisions :
 *                               - `24`, ... pour le mode `previsions` (mettre la bonne liste des périodicités acceptées)
 *                               - `0`, pour les modes `conditions` et `infos`
 * @param string $langue         Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration  Configuration complète du service, statique et utilisateur. *
 *
 * @return string URL complète de la requête.
 */
function openmeteo_service2url(array $lieu_normalise, string $mode, int $periodicite, string $langue, array $configuration) : string {
	// Le service accepte les formats suivants :
	// - latitude, longitude
	[$latitude, $longitude] = explode(',', $lieu_normalise['id']);

	// On continue à construire l'URL
	// -- le lieu
	$query = "?latitude={$latitude}&longitude={$longitude}&timezone=auto";
	// -- les données à récupérer suivant le mode
	if ($mode === 'previsions') {
		$query .= '&daily=weathercode,temperature_2m_max,temperature_2m_min,sunrise,sunset,uv_index_max,precipitation_sum,precipitation_probability_mean,uv_index_max,windspeed_10m_max,winddirection_10m_dominant,windgusts_10m_max';
	} else {
		$query .= '&current=temperature_2m,relativehumidity_2m,apparent_temperature,is_day,precipitation,weathercode,cloudcover,surface_pressure,windspeed_10m,winddirection_10m,windgusts_10m';
	}
	// -- le système d'unité : par défaut métrique, pour impérial préciser toutes les unites
	$query .= $configuration['unite'] === 'm' ? '' : '&temperature_unit=fahrenheit&windspeed_unit=mph&precipitation_unit=inch';

	return $configuration['endpoint_requete'] . $query;
}

/**
 * Lit le bloc de réponse potentiellement constitutif d'une erreur pour déterminer si la requête est réellement en échec.
 *
 * @param array $erreur Sous-tableau de la réponse dans lequel vérifier une erreur de flux.
 *
 * @return bool `true` si une erreur est détectée, `false` sinon.
 */
function openmeteo_erreur_verifier(array $erreur) : bool {
	// Initialisation
	$est_erreur = false;

	// Une erreur est décrite par l'existence d'un message expliquant la raison
	if (!empty($erreur['message'])) {
		$est_erreur = true;
	}

	return $est_erreur;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu uniquement de la lecture du flux.
 *
 * @uses etat2resume_openmeteo()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               au service.
 */
function openmeteo_complement2conditions(array $tableau, string $langue, array $configuration) : array {
	if ($tableau) {
		// Calcul de la direction du vent (16 points), celle-ci n'étant pas fournie nativement
		include_spip('inc/rainette_convertir');
		$tableau['direction_vent'] = angle2direction($tableau['angle_vent']);

		// Compléter le tableau standard avec les états météorologiques calculés (résumé, icones locaux ou weather).
		$tableau = etat2resume_openmeteo($tableau, $langue, $configuration);
	}

	return $tableau;
}

/**
 * Modifie le flux des prévisions issu directement du service pour qu'il puisse être conforme
 * à la logique de normalisation qui s'en suivra.
 *
 * @param array  $flux          Flux non normalisé provenant du service
 * @param string $mode          Type de données météorologiques. Les valeurs possibles sont `infos`, `conditions` ou `previsions`.
 * @param int    $periodicite   Périodicité horaire des prévisions (24, 12, 6, 3 ou 1)
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Flux jour modifié
 */
function openmeteo_flux2flux(array $flux, string $mode, int $periodicite, array $configuration) : array {
	if (
		($mode === 'previsions')
		and $flux
	) {
		// Les valeurs journalières des données sont incluses type de donnée par type de donnée.
		// Il faut donc rétablir un tableau par jour.
		$flux_jour = [];
		foreach ($flux as $_donnee => $_valeurs_jour) {
			foreach ($_valeurs_jour as $_index_jour => $_valeur_jour) {
				$flux_jour[$_index_jour][$_donnee] = $_valeur_jour;
			}
		}
		$flux = $flux_jour;
	}

	return $flux;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu
 * uniquement de la lecture du flux.
 *
 * @uses etat2resume_openmeteo()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 * @param int    $index_periode Index où trouver et ranger les données. Cet index n'est pas utilisé pour les conditions
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               du service.
 */
function openmeteo_complement2previsions(array $tableau, string $langue, array $configuration, int $index_periode) : array {
	if (($tableau) and ($index_periode > -1)) {
		// Calcul de la direction du vent (16 points), celle-ci n'étant pas fournie nativement
		include_spip('inc/rainette_convertir');
		$tableau['direction_vent'] = angle2direction($tableau['angle_vent']);

		// Compléter le tableau standard avec les états météorologiques calculés (résumé, icones locaux ou weather).
		$tableau = etat2resume_openmeteo($tableau, $langue, $configuration);
	}

	return $tableau;
}

// ---------------------------------------------------------------------------------------------
// Les fonctions qui suivent sont des utilitaires uniquement appelées par les fonctions de l'API
// ---------------------------------------------------------------------------------------------.

/**
 * Calcule les états en fonction des états météorologiques natifs fournis par le service.
 *
 * @uses icone_normaliser_chemin()
 * @uses icone_weather_normaliser_chemin()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service. Le tableau est mis à jour et renvoyé à l'appelant.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standard mis à jour.
 */
function etat2resume_openmeteo(array $tableau, string $langue, array $configuration) : array {
	if (
		isset($tableau['code_meteo'])
		and (null !== $tableau['code_meteo'])
	) {
		// Determination de l'indicateur jour/nuit qui permet de choisir le bon icone
		// -> le service renvoie un indicateur pour les conditions.
		if ($tableau['jour_meteo'] === 1) {
			// C'est le jour
			$tableau['periode'] = 0;
		} elseif ($tableau['jour_meteo'] === 0) {
			// C'est la nuit
			$tableau['periode'] = 1;
		} else {
			// L'indicateur n'est pas fourni (prévisions), on met à 0 par défaut
			$tableau['periode'] = 0;
		}

		// Détermination du résumé à afficher : aucun fourni par le service, il faut le calculer à partir du code
		// qui représente l'état météo au sens de WMO.
		$tableau['resume'] = _T('openmeteo:meteo_' . $tableau['code_meteo'], ['spip_lang' => $langue]);

		// Determination de l'icone qui sera affiché.
		// -- Etant donné que le service ne fournit pas d'icone via l'API l'icone est calculé par la fonction
		//    commune
	}

	return $tableau;
}
