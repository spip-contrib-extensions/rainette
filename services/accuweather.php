<?php
/**
 * Ce fichier contient la configuration et l'ensemble des fonctions implémentant le service AccuWeather (accuweather).
 * Ce service est capable de fournir des données au format JSON.
 *
 * Les fonctions qui suivent définissent l'API standard du service et sont appelées par la fonction
 * unique de chargement des données météorologiques `meteo_charger()`.
 *
 * @package SPIP\RAINETTE\SERVICES\ACCUWEATHER
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Construit l'url de la requête correspondant au lieu, au type de données et à la configuration utilisateur
 * du service (par exemple, le code d'inscription, le format des résultats...).
 *
 * @uses langue_service_determiner()
 *
 * @param array  $lieu_normalise Lieu normalisé avec son format pour lequel on acquiert les données météorologiques.
 * @param string $mode           Type de données météorologiques. Les valeurs possibles sont `infos`, `conditions` ou `previsions`.
 * @param int    $periodicite    La périodicité horaire des prévisions :
 *                               - `12` pour le mode `previsions`
 *                               - `0`, pour les modes `conditions` et `infos`
 * @param string $langue         Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration  Configuration complète du service, statique et utilisateur.
 *
 * @return string URL complète de la requête.
 */
function accuweather_service2url(array $lieu_normalise, string $mode, int $periodicite, string $langue, array $configuration) : string {
	// Identification de la langue demandé au service en fonction de celle voulue pour l'affichage.
	// -- il est possible que le service ne fournisse pas cette langue, le résumé natif sera donc dans une autre langue.
	include_spip('inc/rainette_normaliser');
	$code_langue = langue_service_determiner($langue, $configuration);

	// On construit le mode
	if ($mode === 'infos') {
		$demande = 'locations';
	} elseif ($mode === 'conditions') {
		$demande = 'currentconditions';
	} else {
		$demande = 'forecasts';
	}

	return
		$configuration['endpoint_requete']
		. $demande
		. '/v1/'
		. ($mode === 'previsions' ? 'daily/5day/' : '')
		. $lieu_normalise['id']
		. ".{$configuration['format_flux']}"
		. '?apikey=' . $configuration['inscription']
		. '&language=' . $code_langue
		. ($configuration['unite'] === 'm' ? '&metric=true' : '')
		. '&details=true';
}

/**
 * Lit le bloc de réponse potentiellement constitutif d'une erreur pour déterminer si la requête est réellement en échec.
 *
 * @param array $erreur Sous-tableau de la réponse dans lequel vérifier une erreur de flux.
 *
 * @return bool `true` si une erreur est détectée, `false` sinon.
 */
function accuweather_erreur_verifier(array $erreur) : bool {
	// Initialisation
	$est_erreur = false;

	// Une erreur est uniquement décrite par un message.
	if (!empty($erreur['code']) or !empty($erreur['message'])) {
		$est_erreur = true;
	}

	return $est_erreur;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu
 * uniquement de la lecture du flux.
 *
 * @uses etat2resume_accuweather()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               au service.
 */
function accuweather_complement2conditions(array $tableau, string $langue, array $configuration) : array {
	static $tendances = ['S' => 'steady', 'R' => 'rising', 'F' => 'falling', '' => ''];

	if ($tableau) {
		// Correspondance des tendances de pression dans le système standard
		$tableau['tendance_pression'] = $tendances[$tableau['tendance_pression']];

		// Compléter le tableau standard avec les états météorologiques calculés
		$tableau = etat2resume_accuweather($tableau, $langue, $configuration);
	}

	return $tableau;
}

/**
 * Modifie le flux jour des prévisions issu directement du service pour qu'il puisse être conforme
 * à la logique de normalisation qui s'en suivra.
 *
 * Pour AccuWeather, pour la périodicité 12h on modifie les index 'Day' et 'Night' qui contiennent les flux
 * horaires par un tableau de base 'hourly' dont les index sont numériques : 0 jour et 1 nuit.
 *
 * @param array $flux          Flux jour non normalisé provenant du service
 * @param int   $periodicite   Périodicité horaire des prévisions (24, 12, 6, 3 ou 1)
 * @param array $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Flux jour modifié
 */
function accuweather_flux2previsions(array $flux, int $periodicite, array $configuration) : array {
	if (
		$flux
		and $periodicite === 12
	) {
		// On extrait les index 'Day' et 'Night' que l'on affecte à un index 'hourly' et on les supprime.
		$flux['Hourly'] = [
			$flux['Day'],
			$flux['Night']
		];
		unset($flux['Day'], $flux['Night']);
	}

	return $flux;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu
 * uniquement de la lecture du flux.
 *
 * @uses etat2resume_accuweather()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 * @param int    $index_periode Index où trouver et ranger les données. Cet index n'est pas utilisé pour les conditions
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               du service.
 */
function accuweather_complement2previsions(array $tableau, string $langue, array $configuration, int $index_periode) : array {
	if (
		$tableau
		and ($index_periode > -1)
	) {
		// Compléter le tableau standard avec les états météorologiques calculés
		$tableau = etat2resume_accuweather($tableau, $langue, $configuration);
	}

	return $tableau;
}

// ---------------------------------------------------------------------------------------------
// Les fonctions qui suivent sont des utilitaires uniquement appelées par les fonctions de l'API
// ---------------------------------------------------------------------------------------------.

/**
 * Calcule les états en fonction des états météorologiques natifs fournis par le service.
 *
 * @uses icone_normaliser_chemin()
 * @uses icone_weather_normaliser_chemin()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service. Le tableau est mis à jour et renvoyé à l'appelant.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standard mis à jour.
 */
function etat2resume_accuweather(array $tableau, string $langue, array $configuration) : array {
	if ($tableau['code_meteo']) {
		// Determination de l'indicateur jour/nuit qui peut permettre de choisir le bon icone
		// -> on utilise l'identifiant jour:nuit fourni par le service, pour les conditions
		if ($tableau['jour_meteo'] === false) {
			// C'est la nuit
			$tableau['periode'] = 1;
		} else {
			// C'est le jour pour les conditions ou ce sont des prévisions pour lesquelles on affiche toujours l'icone jour
			$tableau['periode'] = 0;
		}

		// Détermination du résumé à afficher.
		$tableau['resume'] = spip_ucfirst($tableau['desc_meteo']);

		// Determination de l'icone qui sera affiché.
		// -- Etant donné que le service ne fournit pas d'icone via l'API l'icone est calculé par la fonction
		//    commune
	}

	return $tableau;
}
