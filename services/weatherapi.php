<?php
/**
 * Ce fichier contient la configuration et l'ensemble des fonctions implémentant le service WeatherAPI.com (weatherapi).
 * Ce service est capable de fournir des données au format JSON. Ce service ressemble à WWO à ceci près qu'il propose
 * un plan gratuit.
 *
 * Les fonctions qui suivent définissent l'API standard du service et sont appelées par la fonction
 * unique de chargement des données météorologiques `meteo_charger()`.
 *
 * @package SPIP\RAINETTE\SERVICES\WEATHERAPI
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Construit l'url de la requête correspondant au lieu, au type de données et à la configuration utilisateur
 * du service (par exemple, le code d'inscription, le format des résultats...).
 *
 * @uses langue_service_determiner()
 *
 * @param array  $lieu_normalise Lieu normalisé avec son format pour lequel on acquiert les données météorologiques.
 * @param string $mode           Type de données météorologiques. Les valeurs possibles sont `infos`, `conditions` ou `previsions`.
 * @param int    $periodicite    La périodicité horaire des prévisions :
 *                               - `24` pour le mode `previsions`
 *                               - `0`, pour les modes `conditions` et `infos`
 * @param string $langue         Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration  Configuration complète du service, statique et utilisateur.
 *
 * @return string URL complète de la requête.
 */
function weatherapi_service2url(array $lieu_normalise, string $mode, int $periodicite, string $langue, array $configuration) : string {
	// Identification de la langue du resume.
	include_spip('inc/rainette_normaliser');
	$code_langue = langue_service_determiner($langue, $configuration);

	// On détermine la méthode de l'API en fonction du mode
	if ($mode === 'previsions') {
		$methode = 'forecast';
	} else {
		$methode = 'current';
	}

	$url = $configuration['endpoint_requete']
		. '/' . $methode . '.' . $configuration['format_flux']
		. '?key=' . $configuration['inscription']
		. '&lang=' . $code_langue
		. '&q=' . $lieu_normalise['id'];

	// Si on est en mode prévision il faut rajouter le nombre de jours possibles
	if ($mode === 'previsions') {
		$url .= '&days=' . $configuration['periodicites'][$periodicite]['max_jours'];
	}

	return $url;
}

/**
 * Lit le bloc de réponse potentiellement constitutif d'une erreur pour déterminer si la requête est réellement en échec.
 *
 * @param array $erreur Sous-tableau de la réponse dans lequel vérifier une erreur de flux.
 *
 * @return bool `true` si une erreur est détectée, `false` sinon.
 */
function weatherapi_erreur_verifier(array $erreur) : bool {
	// Initialisation
	$est_erreur = false;

	// Une erreur est uniquement décrite par un message et un code.
	if (!empty($erreur['message'])) {
		$est_erreur = true;
	}

	return $est_erreur;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu
 * uniquement de la lecture du flux.
 *
 * @uses etat2resume_weatherapi()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               au service.
 */
function weatherapi_complement2conditions(array $tableau, string $langue, array $configuration) : array {
	if ($tableau) {
		// Compléter le tableau standard avec les états météorologiques calculés
		$tableau = etat2resume_weatherapi($tableau, $langue, $configuration);
	}

	return $tableau;
}

/**
 * Complète par des données spécifiques au service le tableau des conditions issu
 * uniquement de la lecture du flux.
 *
 * @uses etat2resume_weatherapi()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 * @param int    $index_periode Index où trouver et ranger les données. Cet index n'est pas utilisé pour les conditions
 *
 * @return array Tableau standardisé des conditions météorologiques complété par les données spécifiques
 *               du service.
 */
function weatherapi_complement2previsions(array $tableau, string $langue, array $configuration, int $index_periode) : array {
	if (
		$tableau
		and ($index_periode > -1)
	) {
		// Compléter le tableau standard avec les états météorologiques calculés
		$tableau = etat2resume_weatherapi($tableau, $langue, $configuration);
	}

	return $tableau;
}

// ---------------------------------------------------------------------------------------------
// Les fonctions qui suivent sont des utilitaires uniquement appelées par les fonctions de l'API
// ---------------------------------------------------------------------------------------------.

/**
 * Calcule les états en fonction des états météorologiques natifs fournis par le service.
 *
 * @uses icone_normaliser_chemin()
 * @uses icone_weather_normaliser_chemin()
 *
 * @param array  $tableau       Tableau standardisé des conditions contenant uniquement les données fournies sans traitement
 *                              par le service. Le tableau est mis à jour et renvoyé à l'appelant.
 * @param string $langue        Code de langue spip indiquant langue choisie pour afficher les données météo.
 * @param array  $configuration Configuration complète du service, statique et utilisateur.
 *
 * @return array Tableau standard mis à jour.
 */
function etat2resume_weatherapi(array $tableau, string $langue, array $configuration) : array {
	if ($tableau['icon_meteo']) {
		// Determination de l'indicateur jour/nuit qui permet de choisir le bon icône.
		// - on utilise l'indicateur fourni par le service si il existe sinon l'icone
		if ($tableau['jour_meteo']) {
			$tableau['periode'] = $tableau['jour_meteo'] ? 0 : 1;
		} else {
			$tableau['periode'] = strpos($tableau['icon_meteo'], '/night/') === false ? 0 : 1;
		}

		// Détermination du résumé à afficher.
		$tableau['resume'] = spip_ucfirst($tableau['desc_meteo']);

		// Determination de l'icone qui sera affiché dans le cas où c'est l'API qui le fournit
		// -- on calcule le chemin complet de l'icone.
		if ($configuration['condition'] === $configuration['alias']) {
			// On affiche l'icône natif fourni par le service et désigné par son url
			// en faisant une copie locale dans IMG/.
			include_spip('inc/distant');
			$tableau['icone']['source'] = copie_locale($tableau['icon_meteo']);
		}
	}

	return $tableau;
}
