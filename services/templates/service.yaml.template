# Template du YAML de configuration du service <nom> (<id>)
# -----------------------------------------------------------------------------------------------
#
# Il est composé de 5 blocs, à savoir :
# - service    : la configuration du service lui-même, son titre, son id, ses caractéristiques...
# - infos      : la configuration du mash-up pour les informations sur le lieu de la météo
# - conditions : la configuration du mash-up pour les conditions temps réel
# - previsions : la configuration du mash-up pour les prévisions
# - erreurs    : la configuration du mash-up pour les erreurs fournies par le service
#
# Si un champ vaut la valeur par défaut il est possible de l'omettre.
# -----------------------------------------------------------------------------------------------

# Bloc service : caractéristiques du service
# ------------------------------------------
service:
  # -- Identification: saisir le nom complet du service
  nom: 'monservice'
  # -- Version de la configuration du service
  version:
    numero: '4'
    date: '2023-11-03'
  # -- URL de base des requêtes et des icones (éventuellement)
  endpoint_requete: ''
  endpoint_icone: ''
  lieu_formats: [ 'latitude_longitude', 'adresse_ip' ]
  # -- Activité: par défaut à true, donc à laisser en commentaire. A mettre à false si on veut déprécier le service
  actif: true
  # -- Liens vers les crédits et titre de la page (pour les affichages / modèles)
  #    Le logo, si il existe, est chargé automatiquement à partir du chemin /services/images/<id>.png
  credits:
    titre: 'Credits'
    lien: 'http://xxxx'
  # -- Liens vers les termes d'utilisation (pour la page de configuration)
  termes:
    titre: 'Licences'
    lien: 'http://xxxx'
  # -- Liens la page d'enregistrement (pour la page de configuration)
  enregistrement:
    titre: 'Sign up'
    lien: 'http://xxxx'
    # -- Taille de la clé d'enregistrement en général de 32 caractères (valeur par défaut)
    #    Pour indiquer qu'aucune clé n'est utile attribuer la valeur 0 explicitement à la taille
    #taille_cle: 32
    # -- Taille du secret en général de 0 caractères car peu utilisé (valeur par défaut)
    #    Pour indiquer qu'aucun secret n'est utile attribuer la valeur 0 explicitement à la taille (la plupart des cas)
    #taille_secret: 0
  # -- Liens vers les offres de service (pour la page de configuration et les rafraichissements)
  offres:
    titre: 'Pricing'
    lien: 'http://xxxx'
    # -- Indique si il existe une offre gratuite (défaut true).
    #gratuite: true
    # -- Les limites sont exprimées via les périodes year, month, day, hour, minute et second dans cet ordre.
    #    Seules les périodes utilisées sont à fournir.
    limites: { month: 5000, day: 200 }
  # -- Liste des langues fournies par le service
  langues:
    # -- langues dont le code est identique à celui de SPIP: par défaut initialisé au singleton 'en' qui est aussi la
    #    valeur par défaut
    codes_spip: [ 'fr', 'en', 'es' ]
    # -- langues dont le code est différent de celui de SPIP.
    #    Le champ est rempli par la liste des couples (code service, code spip)
    codes_non_spip: { cz: 'cs' }
    # -- langage par défaut est 'en'.
    #defaut: 'en'
  # Configuration des jeux d'icones utilisables
  icones:
    origines: [ 'api', 'local', 'weather' ]
    themes_api: [ ]
    transcodages: { }
  # -- Valeurs par défauts des paramètres modifiables du service (page de configuration)
  #    En général, seuls les champs theme et theme_local sont à remplir, les autres sont initialisés avec le défaut
  defauts:
    # -- Clé d'enregistrement fournie par le service. A remplir avec la valeur dans le formulaire.
    #inscription: ''
    # -- Système métrique par défaut
    #unite: 'm'
    # -- Mode d'affichage des icones méteo. On privilégie par défaut les icones du service si il existent
    #condition: '<id>'
    theme: ''
    theme_local: ''
    # -- Thème Sticker de weather qui est toujours disponible dans Rainette
    #theme_weather: 'sticker'

# Bloc infos : mash-up des informations géographiques
# ---------------------------------------------------
infos:
  # -- Période de rafraichissement: par défaut à un mois, inutile de le modifier pour chaque service
  #periode_maj: 2592000
  # -- Format des réponse: par défaut le JSON, inutile de le modifier pour chaque service sauf si pas supporté
  #format_flux: 'json'
  # -- Index de base dans le tableau de réponse: peut-être une liste d'index éventuellement vide
  cle_base: [ 'index1', 'index2' ]
  donnees:
    ville:
      cle: [ 'name' ]
    pays:
      cle: [ ]
    pays_iso2:
      cle: [ 'sys', 'country' ]
    region:
      cle: [ ]
    longitude:
      cle: [ 'coord', 'lon' ]
    latitude:
      cle: [ 'coord', 'lat' ]

# Bloc conditions : mash-up des conditions temps réel
# ---------------------------------------------------
conditions:
  # -- Période de rafraichissement: par défaut à 2 heures, inutile de le modifier pour chaque service
  #periode_maj: 7200
  # -- Format des réponse: par défaut le JSON, inutile de le modifier pour chaque service sauf si pas supporté
  #format_flux: 'json'
  # -- Index de base dans le tableau de réponse: peut-être une liste d'index éventuellement vide
  cle_base: [ 'index1', 0 ]
  donnees:
    derniere_maj:
      cle: [ 'dt' ]
    station:
      cle: [ ]
    temperature_reelle:
      cle: [ 'main', 'temp' ]
    temperature_ressentie:
      cle: [ 'main', 'feels_like' ]
    vitesse_vent:
      cle: [ 'wind', 'speed' ]
    rafale_vent:
      cle: []
    angle_vent:
      cle: [ 'wind', 'deg' ]
    direction_vent:
      cle: [ ]
    precipitation:
      cle: [ ]
    humidite:
      cle: [ 'main', 'humidity' ]
    point_rosee:
      cle: [ ]
    pression:
      cle: [ 'main', 'pressure' ]
    tendance_pression:
      cle: [ ]
    visibilite:
      cle: [ 'visibility' ]
    nebulosite:
      cle: [ 'clouds', 'all' ]
    indice_uv:
      cle: [ ]
    risque_uv:
      cle: [ ]
      calcul: false
    code_meteo:
      cle: [ 'weather', 0, 'id' ]
    icon_meteo:
      cle: [ 'weather', 0, 'icon' ]
    desc_meteo:
      cle: [ 'weather', 0, 'description' ]
    trad_meteo:
      cle: [ ]
    jour_meteo:
      cle: [ ]
    icone:
      cle: [ ]
      calcul: true
    resume:
      cle: [ ]
      calcul: true
    periode:
      cle: [ ]
      calcul: true

# Bloc previsions : mash-up des prévisions
# ----------------------------------------
previsions:
  # -- Période de rafraichissement: par défaut à 4 heures, inutile de le modifier pour chaque service
  #periode_maj: 14400
  # -- Format des réponse: par défaut le JSON, inutile de le modifier pour chaque service sauf si pas supporté
  #format_flux: 'json'
  # -- Périodicités disponibles pour les prévisions: peut prendre les valeurs 1, 3, 6, 12, 24. Pour chacune
  #    il faut préciser le nombre de jours de prévisions miximum disponibles
  periodicites:
    24: { max_jours: 16 }
  # -- Périodicité par défaut à 24h. A conserver ainsi sauf si le service ne propose par cette périodicité
  #periodicite_defaut: 24
  # -- Index de base dans le tableau de réponse: peut-être une liste d'index éventuellement vide
  cle_base: [ 'list' ]
  # -- Index des données heure si le service possède une structure par heure
  cle_heure: [ ]
  structure_heure: false
  donnees:
    date:
      cle: [ 'dt' ]
    heure:
      cle: [ ]
    lever_soleil:
      cle: [ ]
    coucher_soleil:
      cle: [ ]
    temperature:
      cle: [ 'temp', 'day' ]
    temperature_max:
      cle: [ 'temp', 'max' ]
    temperature_min:
      cle: [ 'temp', 'min' ]
    vitesse_vent:
      cle: [ 'speed' ]
    rafale_vent:
      cle: []
    angle_vent:
      cle: [ 'deg' ]
    direction_vent:
      cle: [ ]
    risque_precipitation:
      cle: [ ]
    precipitation:
      cle: [ 'rain' ]
    humidite:
      cle: [ 'humidity' ]
    point_rosee:
      cle: [ ]
    pression:
      cle: [ 'pressure' ]
    visibilite:
      cle: [ ]
    nebulosite:
      cle: [ 'clouds' ]
    indice_uv:
      cle: [ ]
    risque_uv:
      cle: [ ]
      calcul: false
    code_meteo:
      cle: [ 'weather', 0, 'id' ]
    icon_meteo:
      cle: [ 'weather', 0, 'icon' ]
    desc_meteo:
      cle: [ 'weather', 0, 'description' ]
    trad_meteo:
      cle: [ ]
    jour_meteo:
      cle: [ ]
    icone:
      cle: [ ]
      calcul: true
    resume:
      cle: [ ]
      calcul: true
    periode:
      cle: [ ]
      calcul: true

# Bloc erreurs : mash-up des erreurs remontées par le service
# -----------------------------------------------------------
erreurs:
  cle_base: [ ]
  donnees:
    code:
      cle: [ 'cod' ]
    message:
      cle: [ 'message' ]
