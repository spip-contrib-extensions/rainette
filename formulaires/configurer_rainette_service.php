<?php
/**
 * Gestion du formulaire de configuration du plugin Rainette.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * On surcharge la fonction de chargement par défaut afin de fournir le service, sa configuration
 * et son état d'exécution au formulaire.
 *
 * @param string $service Alias du service.
 *
 * @return array Tableau des données à charger par le formulaire.
 */
function formulaires_configurer_rainette_service_charger(string $service) : array {
	// Initialiser l'indicateur d'édition
	$valeurs = [
		'editable' => false,
	];

	if (autoriser('configurer')) {

		// On récupère le service par défaut si besoin
		include_spip('rainette_fonctions');
		if (!$service) {
			$service = rainette_service_defaut();
		}

		// Récupération de la configuration générale du service.
		include_spip('inc/rainette_normaliser');
		$configuration = configuration_service_lire($service, 'service');

		// Normalisation de la configuration utilisateur du service afin d'éviter de gérer
		// au sein du formulaire des valeurs par défaut.
		$valeurs = array_merge($valeurs, parametrage_service_normaliser($service, $configuration['defauts']));

		// Ajout du service et des éléments utiles de la configuration statique
		$valeurs['service'] = $service;
		$valeurs['nom'] = $configuration['nom'];
		$valeurs['_configuration']['termes'] = $configuration['termes'];
		$valeurs['_configuration']['enregistrement'] = $configuration['enregistrement'];
		$valeurs['_configuration']['offres'] = $configuration['offres'];

		// Ajout des informations d'utilisation du service
		include_spip('inc/config');
		$execution = lire_config("rainette_execution/{$service}", []);
		$valeurs['_utilisation']['dernier_appel'] = $execution['dernier_appel'] ?? '';
		if (isset($execution['compteurs'])) {
			$valeurs['_utilisation']['compteurs'] = $execution['compteurs'];
		}
		else {
			// On initialise les limites à zéro
			$valeurs['_utilisation']['compteurs'] = [];
			foreach ($configuration['offres']['limites'] as $_periode => $_limite) {
				$valeurs['_utilisation']['compteurs'][$_periode] = 0;
			}
		}

		// Gestion des thèmes.
		// -- liste des types de thèmes
		$valeurs['_themes']['types'] = [
			$service => _T('rainette:label_icone_natif'),
			"{$service}_local" => _T('rainette:label_icone_local'),
			'weather' => _T('rainette:label_icone_weather')
		];
		// -- Liste des thèmes de chaque type pour le service concerné
		$valeurs['_themes']['distants'] = rainette_lister_themes($service, 'api');
		$valeurs['_themes']['locaux'] = rainette_lister_themes($service, 'local');
		$valeurs['_themes']['weather'] = rainette_lister_themes('weather', 'local');
		// -- Construction du disable pour chaque choix de type de thème
		$valeurs['_themes']['disabled'] = [];
		if (!in_array('api', $configuration['icones']['origines'])) {
			$valeurs['_themes']['disabled'][] = $service;
		}
		if (!$valeurs['_themes']['locaux']) {
			$valeurs['_themes']['disabled'][] = "{$service}_local";
		}
		if (
			!$valeurs['_themes']['weather']
			or !in_array('weather', $configuration['icones']['origines'])
		) {
			$valeurs['_themes']['disabled'][] = 'weather';
		}

		// On positionne le meta casier car la fonction de recensement automatique n'est plus appelée ni ne pourrait
		// fonctionner avec un hidden dont la valeur est dynamique (utilisation d'une variable d'environnement #ENV).
		$valeurs['_meta_casier'] = "rainette/{$service}";

		// Edition possible
		$valeurs['editable'] = true;
	}

	return $valeurs;
}
