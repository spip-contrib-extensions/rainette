<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajoute les metas sauvegardables de Rainette pour utiliser le plugin IEConfig.
 *
 * @pipeline ieconfig_metas
 *
 * @param array $table Table des déclarations des sauvegardes
 *
 * @return array Table des déclarations complétée
 **/
function rainette_ieconfig_metas(array $table) : array {
	$prefixe = 'rainette';

	include_spip('inc/filtres');
	$informer = charger_filtre('info_plugin');
	$nom = $informer($prefixe, 'nom', true);

	$table['rainette']['titre'] = $nom;
	$table['rainette']['icone'] = "{$prefixe}-16.png";
	$table['rainette']['metas_serialize'] = $prefixe;

	return $table;
}
