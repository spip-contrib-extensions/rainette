<?php
/**
 * Ce fichier contient les fonctions de service nécessitées par le plugin Cache Factory.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie la configuration spécifique des caches de Rainette.
 *
 * @param string $plugin Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier ou
 *                       un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 *
 * @return array Tableau de la configuration des types de cache du plugin.
 */
function rainette_cache_configurer(string $plugin) : array {
	// Initialisation du tableau de configuration avec les valeurs par défaut du plugin Cache.
	$configuration = [
		'meteo' => [
			'racine'          => '_DIR_VAR',
			'sous_dossier'    => true,
			'nom_obligatoire' => ['lieu', 'donnees', 'langage'],
			'nom_facultatif'  => ['unite'],
			'extension'       => '.json',
			'securisation'    => false,
			'serialisation'   => false,
			'decodage'        => true,
			'separateur'      => '_',
			'conservation'    => 3600 * 24
		],
	];

	return $configuration;
}

/**
 * Effectue le chargement du formulaire de vidage des caches de type `meteo` du plugin.
 * L'intérêt est de permette le rangement des caches par service.
 *
 * @uses rainette_lister_services()
 * @uses cache_repertorier()
 *
 * @param string $plugin        Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                              ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param array  $valeurs       Tableau des valeurs du formulaire à compléter
 * @param array  $options       Tableau d'options qui peut être fourni par un plugin utilisateur uniquement si celui-ci fait appel
 *                              au formulaire. La page cache_vider de Cache Factory n'utilise pas ce paramètre.
 *                              Le tableau est passé à la fonction de service de chargement du formulaire uniquement.
 * @param array  $configuration Configuration complète des caches du type meteo du plugin utilisateur lue à partir de la meta de stockage.
 *
 * @return array Tableau des caches de type meteo.
 */
function rainette_meteo_cache_formulaire_charger(string $plugin, array $valeurs, array $options, array $configuration) : array {
	// On constitue la liste des services requis par l'appel
	include_spip('rainette_fonctions');
	$services = rainette_lister_services('tableau', false);

	// On récupère les caches et leur description pour donner un maximum d'explication sur le contenu.
	include_spip('inc/ezcache_cache');
	foreach ($services as $_service => $_infos) {
		// On récupère les caches du service
		$filtres = ['sous_dossier' => $_service];
		$caches = cache_repertorier('rainette', 'meteo', $filtres);

		// Si il existe des caches pour le service on stocke les informations recueillies
		if ($caches) {
			$complement_titre = $_infos['actif']
				? _T('rainette:service_actif')
				: _T('rainette:service_inactif');
			$valeurs['_caches']['meteo'][$_service]['titre'] = "{$_infos['nom']} ({$complement_titre})";
			$valeurs['_caches']['meteo'][$_service]['liste'] = $caches;
		}
	}

	return $valeurs;
}
