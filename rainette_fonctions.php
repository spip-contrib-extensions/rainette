<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_RAINETTE_SERVICE_DEFAUT')) {
	define('_RAINETTE_SERVICE_DEFAUT', 'owm');
}
if (!defined('_RAINETTE_ICONE_GRANDE_TAILLE')) {
	define('_RAINETTE_ICONE_GRANDE_TAILLE', 110);
}
if (!defined('_RAINETTE_ICONE_PETITE_TAILLE')) {
	define('_RAINETTE_ICONE_PETITE_TAILLE', 28);
}

/**
 * Compile la balise `#RAINETTE_INFOS` qui renvoie une information ou toutes les informations liées à un lieu
 * pour un service donné.
 * La signature de la balise est : `#RAINETTE_INFOS{lieu[, info, service, langue]}`.
 *
 * @balise
 *
 * @uses calculer_infos()
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 */
function balise_RAINETTE_INFOS($p) {
	// Extraction et normalisation des arguments
	$lieu = interprete_argument_balise(1, $p);
	$lieu = isset($lieu) ? str_replace('\'', '"', $lieu) : '""';
	// Donnée informative sur le lieu (vide = toutes)
	$type_info = interprete_argument_balise(2, $p);
	$type_info = isset($type_info) ? str_replace('\'', '"', $type_info) : '""';
	// Le service à utiliser (service par défaut si vide)
	$service = interprete_argument_balise(3, $p);
	$service = isset($service) ? str_replace('\'', '"', $service) : '""';
	// Le service à utiliser (service par défaut si vide)
	$langue = interprete_argument_balise(4, $p);
	$langue = isset($langue) ? str_replace('\'', '"', $langue) : '""';

	$p->code = 'calculer_infos(' . $lieu . ', ' . $type_info . ', ' . $service . ', ' . $langue . ')';
	$p->interdire_scripts = false;

	return $p;
}

/**
 * Renvoie une information sur un lieu pour un service donné ou toutes les informations de ce lieu.
 *
 * @param string $lieu    Le lieu concerné par la méteo exprimé selon les critères requis par le service.
 * @param string $type    Le type d'information à retourner ou vide si on veut toute les informations
 * @param string $service Le nom abrégé du service.
 * @param string $langue  Code de langue spip indiquant langue choisie pour afficher les données météo.
 *                        Pour l'instant non utilisé car aucune donnée d'infos n'est traduite.
 *
 * @return mixed L'information ou le tableau de toutes les informations d'un service
 */
function calculer_infos(string $lieu, string $type, string $service, string $langue) {
	// Initialisation du retour
	$info = '';

	// Traitement des cas ou les arguments sont vides
	// - on ne gère pas la langue vide car inutile actuellement
	if ($lieu) {
		if (!$service) {
			$service = rainette_service_defaut();
		}

		// Récupération des informations sur le lieu
		$charger = charger_fonction('meteo_charger', 'inc');
		$tableau = $charger($lieu, 'infos', $service, ['langue' => $langue]);
		if (!isset($type) or !$type) {
			$info = serialize($tableau);
		} elseif (isset($tableau['donnees'][strtolower($type)])) {
			$info = $tableau['donnees'][strtolower($type)];
		}
	}

	return $info;
}

/**
 * Affiche l'icône correspondant au code météo fourni.
 *
 * @package RAINETTE/AFFICHAGE
 *
 * @api
 *
 * @filtre
 *
 * @param array           $icone   Tableau informatif sur l'icone
 * @param null|int|string $taille  Taille de l'icone : prend les valeurs `petit` (défaut) ou `grand` ou une valeur entière.
 * @param null|array      $options Liste d'options comme la classe CSS à rajouter à la balise img
 *
 * @return string Balise img correspondant à l'affichage de l'icone
 */
function rainette_afficher_icone(array $icone, $taille = 'petit', ?array $options = []) {
	// -- calcul de la taille maximale de l'icone
	if ($taille === 'petit') {
		$taille_max = _RAINETTE_ICONE_PETITE_TAILLE;
	} elseif ($taille === 'grand') {
		$taille_max = _RAINETTE_ICONE_GRANDE_TAILLE;
	} else {
		$taille_max = (int) $taille;
	}

	// Extraction des codes météo et constitution du texte.
	$texte = (string) $icone['code']
		. ($icone['code'] === $icone['code_meteo'] ? '' : ' / ' . (string) $icone['code_meteo']);

	if (
		$icone
		and isset($icone['source'], $icone['code'])
		and ($source = $icone['source'])
	) {
		// On détermine l'extension du fichier source
		$extension = pathinfo($source, PATHINFO_EXTENSION);

		// On retaille si nécessaire l'image pour qu'elle soit toujours de la même taille (grande ou petite).
		// -- calcul de la taille de l'image
		if ($extension !== 'svg') {
			[$largeur, $hauteur] = @getimagesize($source);

			include_spip('filtres/images_transforme');
			if (
				($largeur < $taille_max)
				and ($hauteur < $taille_max)
			) {
				// Image plus petite que celle par défaut :
				// --> Il faut insérer et recadrer l'image dans une image plus grande à la taille par défaut
				$source = extraire_attribut(
					image_recadre(
						$source,
						$taille_max,
						$taille_max,
						'center',
						'transparent'
					),
					'src'
				);
			} else {
				// Si l'image n'est pas carrée on la recadre
				if ($largeur !== $hauteur) {
					$source = extraire_attribut(
						image_recadre(
							$source,
							max($largeur, $hauteur),
							max($largeur, $hauteur),
							'center',
							'transparent'
						),
						'src'
					);
				}

				// Image plus grande que celle par défaut :
				// --> Il faut réduire l'image à la taille par défaut
				$source = extraire_attribut(
					image_reduire(
						$source,
						$taille_max
					),
					'src'
				);
			}
		}
	} else {
		include_spip('inc/utils');
		$source = find_in_path('themes/icone-vide.svg');
	}

	// On construit la balise img
	include_spip('inc/filtres');
	$classe = !empty($options['classe']) ? $options['classe'] : '';
	$imager = charger_filtre('balise_img');

	return inserer_attribut($imager($source, $texte, $classe, $taille_max), 'title', $texte);
}

/**
 * Affiche le résumé fourni par le service ou calculé à partir d'un item de langue.
 *
 * A partir de la version 4.1.0, le résumé est toujours une chaine, le code météo n'est plus passé à la fonction même
 * si le service ne fournit pas de résumé. Dans ce cas, c'est la fonction de complément du service qui calcule le résumé.
 *
 * @package RAINETTE/AFFICHAGE
 *
 * @api
 *
 * @filtre
 *
 * @param null|string $resume Résumé météo déjà fourni ou calculé par le service dans la langue de l'interface.
 *                            Peut-être null si le service ne renvoie aucun résumé affichable.
 *
 * @return string
 */
function rainette_afficher_resume(?string $resume = null) : string {
	if (null === $resume) {
		// le service ne renvoie rien, on affiche rien (il n'est pas indisponible)
		$texte = '';
	} else {
		$texte = $resume ?: _T('rainette:meteo_na');
	}

	return spip_ucfirst($texte);
}

/**
 * Convertit une indication de direction en une chaine traduite pour l'affichage dans les modèles.
 *
 * @package RAINETTE/AFFICHAGE
 *
 * @api
 *
 * @filtre
 *
 * @param string $direction La direction soit sous forme d'une valeur numérique entre 0 et 360, soit sous forme
 *                          d'une chaine. Certains services utilisent la chaine "V" pour indiquer une direction variable.
 *
 * @return string La chaine traduite indiquant la direction du vent.
 */
function rainette_afficher_direction($direction) : string {
	// Calcul de la direction abrégée si on passe un angle et pas une direction
	if (
		include_spip('inc/rainette_convertir')
		and ($direction = strtoupper($direction))
		and in_array($direction, lister_directions())
	) {
		$direction_texte = _T("rainette:direction_{$direction}");
	} else {
		$direction_texte = _T('rainette:valeur_indeterminee');
	}

	return $direction_texte;
}

/**
 * Affiche la tendance de pression selon la méthode demandée (texte en clair, symbole de flèche ou icone).
 *
 * @package RAINETTE/AFFICHAGE
 *
 * @api
 *
 * @filtre
 *
 * @param null|string $tendance_en Texte anglais représentant la tendance et récupérée par le service.
 * @param null|string $methode     Methode d'affichage de la tendance qui prend les valeurs:
 *                                 - `texte`   : pour afficher un texte en clair décrivant la tendance (méthode par défaut).
 *                                 - `symbole` : pour afficher un symbole de flèche (1 caractère) décrivant la tendance.
 *
 * @return string Texte de la tendance conforme à la méthode prescrite.
 */
function rainette_afficher_tendance(?string $tendance_en, ?string $methode = 'texte') : string {
	$tendance = '';

	// Certains textes sont composés de plusieurs mots comme "falling rapidly".
	// On en fait un texte unique en remplaçant les espaces par des underscores.
	$tendance_en = str_replace(' ', '_', trim((string) $tendance_en));

	if (
		$tendance_en
		 and ($texte = _T("rainette:tendance_texte_{$tendance_en}", [], ['force' => false]))
	) {
		if ($methode === 'texte') {
			$tendance = $texte;
		} else {
			$tendance = _T("rainette:tendance_symbole_{$tendance_en}");
		}
	}

	return $tendance;
}

/**
 * Affiche toute donnée météorologique au format numérique avec son unité.
 *
 * @package RAINETTE/AFFICHAGE
 *
 * @api
 *
 * @filtre
 *
 * @param null|float|int $valeur      La valeur à afficher
 * @param null|string    $type_donnee Type de données à afficher parmi 'temperature', 'pourcentage', 'angle', 'pression',
 *                                    'distance', 'vitesse', 'population', 'precipitation'.
 * @param null|int       $precision   Nombre de décimales à afficher pour les réels uniquement ou -1 pour utiliser le défaut.
 * @param null|string    $service     Identifiant du service utilisé. Si non fourni, on prend le service par défaut
 *
 * @return string La chaine calculée ou le texte désignant une valeur indéterminée ou vide si la valeur est null.
 */
function rainette_afficher_unite($valeur, ?string $type_donnee = '', ?int $precision = -1, ?string $service = '') : string {
	// Initialisations : en particulier on récupère le service par défaut si besoin
	static $precision_defaut = [
		'temperature'   => 0,
		'pression'      => 1,
		'distance'      => 1,
		'angle'         => 0,
		'pourcentage'   => 0,
		'population'    => 0,
		'precipitation' => 1,
		'vitesse'       => 0,
		'indice'        => 1
	];
	if (!$service) {
		$service = rainette_service_defaut();
	}
	include_spip('inc/config');
	$unite = lire_config("rainette/{$service}/unite", 'm');

	// On distingue la valeur '' qui indique que la donnée météo n'est pas fournie par le service avec
	// la valeur null qui indique que la valeur n'est pas disponible temporairement
	// Dans le cas '' on n'affiche pas la valeur, dans le cas null on affiche la non disponibilité
	if ($valeur === '') {
		$valeur_affichee = '';
	} else {
		$valeur_affichee = _T('rainette:valeur_indeterminee');
		if ($valeur !== null) {
			// Détermination de l'arrondi si la donnée est stockée sous format réel
			if (array_key_exists($type_donnee, $precision_defaut)) {
				$precision = ($precision < 0) ? $precision_defaut[$type_donnee] : $precision;
				$valeur = round($valeur, $precision);
			}

			// Construction de la valeur affichée.
			$valeur_affichee = (string) $valeur;
			if ($valeur_affichee === '-0') {
				$valeur_affichee = '0';
			}

			// Ajout de l'unité en fonction du type de valeur. Un indice ne possède pas d'unité.
			if ($type_donnee !== 'indice') {
				$suffixe = ($type_donnee === 'population')
					? ''
					: (($unite === 'm') ? 'metrique' : 'standard');
				$espace = in_array($type_donnee, ['temperature', 'pourcentage', 'angle']) ? '' : '&nbsp;';
				$item = 'rainette:unite_' . $type_donnee . ($suffixe ? '_' . $suffixe : '');
				$valeur_affichee .= $espace . _T($item);
			}
		}
	}

	return $valeur_affichee;
}

/**
 * Renvoie le nom littéral du service à partir de sa configuration stockée en meta.
 * Par défaut, renvoie le nom abrégé si pas de nom littéral.
 *
 * @package RAINETTE/AFFICHAGE
 *
 * @api
 *
 * @filtre
 *
 * @param string $service Le nom abrégé du service.
 *
 * @return string Texte correspodnant au nom du service
 */
function rainette_afficher_service(string $service) : string {
	// On renvoie le nom du service ou a défaut son id.
	include_spip('inc/config');

	return
		lire_config("rainette_{$service}/service/nom", $service);
}

/**
 * Renvoie le nom abrégé du service par défaut de Rainette.
 *
 * @return string Nom abrégé du service
 */
function rainette_service_defaut() : string {
	return _RAINETTE_SERVICE_DEFAUT;
}

/**
 * Retourne la disponibilité d'un service donné.
 *
 * @param string $service Le nom abrégé du service.
 *
 * @return string Identifiant de l'erreur ou chaine vide si disponible.
 */
function rainette_service_est_indisponible(string $service) : string {
	$services = rainette_lister_services('tableau', false);
	if (!array_key_exists($service, $services)) {
		// L'identifiant du service est erroné.
		$erreur = 'service_inconnu';
	} elseif (!$services[$service]['actif']) {
		// Le service est inactivé.
		$erreur = 'service_inactif';
	} else {
		$erreur = '';
	}

	return $erreur;
}

/**
 * Liste des services disponibles.
 * Il est possible de filtrer les services actifs uniquement.
 * La liste est retournée soit sous la forme d'n tableau soit sous la forme d'une chaine dont les services sont séparés
 * par une virgule.
 *
 * @param null|string $type_sortie      Sortie de type tableau ou liste séparée par une virgule
 * @param null|bool   $filtre_actif     Indique si la liste est filtrée sur les seuls services actifs
 * @param null|bool   $filtre_parametre Indique si la liste est filtrée sur les seuls services correctement paramétrés
 *
 * @return array|string Liste des services
 */
function rainette_lister_services(?string $type_sortie = 'tableau', ?bool $filtre_actif = true, ?bool $filtre_parametre = false) {
	static $services = [];

	if (!isset($service[$type_sortie][$filtre_actif][$filtre_parametre])) {
		// On lit les fichiers php dans répertoire services/ du plugin sachant que ce répertoire
		// contient exclusivement les api de chaque service dans un fichier unique appelé
		// alias_du_service.php
		include_spip('inc/rainette_normaliser');
		$liste = [];
		if ($fichiers_api = glob(_DIR_PLUGIN_RAINETTE . 'services/*.yaml')) {
			foreach ($fichiers_api as $_fichier) {
				// On détermine l'alias du service
				$service = strtolower(basename($_fichier, '.yaml'));

				// Acquérir la configuration statique du service et le paramétrage utilisateur.
				$configuration = configuration_service_lire($service, 'service');

				// Ajout du service dans la liste uniquement si celui-ci est encore actif ou si on demande tous
				// les services actifs ou pas.
				if (
					$configuration
					and (
						!$filtre_actif
						or $configuration['actif']
					)
					and (
						(
							$filtre_parametre
							and parametrage_service_est_valide($service)
						)
						or !$filtre_parametre
					)
				) {
					if (
						($type_sortie === 'tableau')
						and !$filtre_actif
					) {
						$liste[$service] = [
							'nom'   => $configuration['nom'],
							'actif' => $configuration['actif']
						];
					} else {
						$liste[$service] = $configuration['nom'];
					}
				}
			}
		}

		// Par défaut la liste est fournie comme un tableau.
		// Si le mode demandé est 'liste' on renvoie une chaîne énumérée des alias de service séparée par des virgules.
		$services[$type_sortie][$filtre_actif][$filtre_parametre] = ($type_sortie === 'tableau')
			? $liste
			: implode(',', array_keys($liste));
	}

	return $services[$type_sortie][$filtre_actif][$filtre_parametre];
}

/**
 * Renvoie la liste des modèles disponibles correspondant à un mode météo et une périodicité donnée.
 * La périodicité n'est disponible que pour les prévisions.
 *
 * @param null|string $mode        Le type de données météorologiques demandé.
 * @param null|int    $periodicite La périodicité horaire, pour les prévisions uniquement.
 *
 * @return array Liste des modèles
 */
function rainette_lister_modeles(?string $mode = 'conditions', ?int $periodicite = 24) : array {
	$modeles = [];

	// On lit les modèles suivant le mode choisi dans l'ensemble du site.
	// Ceux-ci sont toujours de la forme:
	// -- conditions_<complement>,
	// -- previsions_<periodicite>h_<complement>, la periodicité pouvant être vide et donc remplacé par un pattern décimal
	// -- infos_<complement>.
	if ($mode !== 'previsions') {
		$pattern = "{$mode}.*\\.html$";
	} else {
		$periodicite = $periodicite ?: '\\d+';
		$pattern = "{$mode}_{$periodicite}h.*\\.html$";
	}
	if ($fichiers = find_all_in_path('modeles/', $pattern)) {
		foreach ($fichiers as $_fichier) {
			if (strpos(basename($_fichier, '.html'), $mode) === 0) {
				$modeles[] = strtolower(basename($_fichier, '.html'));
			}
		}
	}

	return $modeles;
}

/**
 * Liste les thèmes disponibles pour un service donné.
 * Il est possible de filtrer selon la source des thèmes.
 *
 * @param string      $service Le nom abrégé du service.
 * @param null|string $source  Source des thèmes : `local` ou `api`.
 *
 * @return array Liste des thèmes
 */
function rainette_lister_themes(string $service, ?string $source = 'local') : array {
	static $themes = [];
	static $configuration = [];

	include_spip('inc/rainette_normaliser');
	if (!$configuration) {
		$configuration = configuration_service_lire($service, 'service');
	}

	if (!isset($themes[$service][$source])) {
		// La liste des thèmes n'a pas encore été enregistrée, il faut la recalculer.
		// On l'initialise à vide car il faut toujours avoir une liste.
		$themes[$service][$source] = [];

		if (strtolower($source) === 'api') {
			// Certains services comme OpenWeather proposent des thèmes d'icones accessibles via l'API.
			if ($configuration['icones']['themes_api']) {
				foreach ($configuration['icones']['themes_api'] as $_cle) {
					$themes[$service][$source][$_cle] = _T("rainette:label_theme_{$service}_{$_cle}");
				}
			}
		} else {
			// Les thèmes de Rainette sont toujours stockés dans l'arborescence themes/$service.
			// Chaque thème a un alias qui correspond à son dossier et un titre pour l'affichage.
			// On recherche les sous-dossiers themes/$service présents dans le path.
			include_spip('inc/utils');
			foreach (creer_chemin() as $_chemin) {
				$dossier_service = $_chemin . icone_normaliser_chemin('', $service);
				if (@is_dir($dossier_service)) {
					if ($dossiers_theme = glob("{$dossier_service}/*", GLOB_ONLYDIR)) {
						foreach ($dossiers_theme as $_theme) {
							$theme = strtolower(basename($_theme));
							// On ne garde que le premier dossier de même nom.
							if (!isset($themes[$theme])) {
								$themes[$service][$source][$theme] = $theme;
							}
						}
					}
				}
			}
		}
	}

	return $themes[$service][$source];
}

/**
 * Affiche avec un modèle choisi, en utilisant un service donné, les informations météorologiques d'un lieu.
 * L'affichage peut être modifié par des otpions supplémentaires.
 *
 * @param string      $lieu    Le lieu concerné par la méteo exprimé selon les critères requis par le service mais non normalisé.
 * @param null|string $mode    Le type de données météorologiques demandé.
 * @param null|string $service Le nom abrégé du service.
 * @param null|array  $options Options d'affichage du modèle : modèle, premier jour, nombre de jours, périodicité, etc.
 *
 * @return string Code HTML à afficher
 */
function rainette_coasser(string $lieu, ?string $mode = 'conditions', ?string $service = '', ?array $options = []) {
	// Initialisations : on récupère le service par défaut, le modèle et la langue si besoin
	include_spip('inc/rainette_normaliser');
	$service = $service ?: rainette_service_defaut();
	$modele = $options['modele'] ?? 'conditions_tempsreel';
	$langue = !empty($options['langue']) ? $options['langue'] : langue_spip_determiner();

	// Vérification du service et de la cohérence entre le mode, le modèle et la périodicité.
	$periodicite = 0;
	if (!$type_erreur = rainette_service_est_indisponible($service)) {
		// Détermination de la périodicité en fonction du mode et du modèle demandés
		if ($mode === 'previsions') {
			// Identification de la périodicité à partir du nom du modèle. Cela évite une configuration compliquée.
			if (preg_match(',_(1|12|24)h,i', $modele, $match)) {
				$type_modele = (int) ($match[1]);

				// On verifie que la périodicité demandée explicitement dans l'appel du modèle est ok
				if (isset($options['periodicite'])) {
					$periodicite_explicite = (int) ($options['periodicite']);
					if (periodicite_est_compatible($type_modele, $periodicite_explicite)) {
						$periodicite = $periodicite_explicite;
					} else {
						$type_erreur = 'modele_periodicite';
					}
				} else {
					// Dans ce cas, il faut choisir une périodicité en fonction du type du modèle et du service.
					$periodicite = periodicite_determiner($type_modele, $service);
					if (!$periodicite) {
						$type_erreur = 'modele_service';
					}
				}
			} elseif (isset($options['periodicite'])) {
				// On ne connait pas le type du modèle, donc sa compatibilité.
				// Si la périodicité est passée en argument on l'utilise sans se poser de question.
				$periodicite = (int) ($options['periodicite']);
			} else {
				// Sinon c'est une erreur car on ne sait pas quelle périodicité est requise
				$type_erreur = 'modele_inutilisable';
			}
		}
	}

	// Normalisation du lieu
	$lieu_normalise = lieu_normaliser($lieu);

	if ($type_erreur) {
		// On construit le tableau directement sans appeler la fonction de calcul des données météo.
		$erreur = [
			'type'    => $type_erreur,
			'service' => [
				'code'    => '',
				'message' => ''
			]
		];

		$tableau = [
			'donnees' => [],
			'extras'  => erreur_normaliser_extras($erreur, $lieu_normalise['id'], $mode, $service, $periodicite)
		];
	} else {
		// Récupération du tableau des données météo
		// -- on ne passe pas le lieu normalisé car la fonction de chargement peut-être appelée sans passer par rainette_coasser()
		$charger = charger_fonction('meteo_charger', 'inc');
		$tableau = $charger($lieu, $mode, $service, ['periodicite' => $periodicite, 'langue' => $langue]);

		// Séparation des données communes liées au service et au mode et des données météorologiques
		$type_erreur = $tableau['extras']['erreur']['type'];

		if (
			!$type_erreur
			and ($mode === 'previsions')
		) {
			// Adaptation des données en fonction de la demande et de la périodicité modèle-cache
			$nb_index = count($tableau['donnees']);

			$jour1 = 0;
			if (isset($options['premier_jour'])) {
				$jour1 = (int) ($options['premier_jour']) < $nb_index
					? (int) ($options['premier_jour'])
					: $nb_index - 1;
			}

			$nb_jours = $nb_index - $jour1;
			if (isset($options['nombre_jours'])) {
				$nb_jours = ($jour1 + (int) ($options['nombre_jours']) <= $nb_index)
					? (int) ($options['nombre_jours'])
					: $nb_index - $jour1;
			}

			$tableau['premier_jour'] = $jour1;
			$tableau['nombre_jours'] = $nb_jours;
		}
	}

	if ($type_erreur) {
		// Affichage du message d'erreur ou des données: seul l'index extras est utile
		$tableau['extras']['erreur']['texte'] = erreur_formater_texte(
			$tableau['extras']['erreur'],
			$lieu_normalise['id'],
			$mode,
			$service,
			$modele,
			$tableau['extras']['config']['nom_service']
		);
		$texte = recuperer_fond('modeles/erreur_rainette', $tableau['extras']);
	} else {
		// Appel du modèle avec le contexte complet
		$texte = recuperer_fond("modeles/{$modele}", $tableau);
	}

	return $texte;
}

include_spip('inc/rainette_debusquer');
