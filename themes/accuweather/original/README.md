# Original Theme

## License and Usage

No specific licence. Can be used for any purpose.

## Credits

See AccuWeather terms of use.

## Links

* [Weather Icons](https://developer.accuweather.com/weather-icons)
