# Original Theme

## License and Usage

No specific licence, provided freely by WeatherAPI. Can be used for any purpose.

These icons are provided in a [zip file](https://cdn.weatherapi.com/weather.zip).
These icons are also provided by the API in the JSON payload.

## Credits

See OpenWeather terms of use.

## Links

* [Weather Icons and Codes](https://www.weatherapi.com/docs/)
