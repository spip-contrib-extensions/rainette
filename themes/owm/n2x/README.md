# N2x Theme

## License and Usage

No specific licence, provided freely by OpenWeather. Can be used for any purpose.

These icons are also provided by the API in the JSON payload.
For example, URL to 10d icon is : [https://openweathermap.org/img/wn/10d@2x.png](https://openweathermap.org/img/wn/10d@2x.png).

## Credits

See OpenWeather terms of use.

## Links

* [Weather Icons](https://openweathermap.org/weather-conditions#Weather-Condition-Codes-2)
