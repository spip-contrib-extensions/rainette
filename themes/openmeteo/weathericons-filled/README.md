# Weathericons-filled Theme

## License and Usage

The symbols of code table 4677 are based on Wikimedia Commons, where
they are marked as released to the Public Domain (PD). They have been 
edited, especially colored. Missing symbols for code table 4680 I created 
by myself.

In non-commercial domain the symbols can be freely used. What you create
using these symbols is not required to be subject to the GPL (fonts
exclusion). However, the GPL applies to editing the symbols themselves.

## Credits

* Pat O'Brien for the [Belchertown skin icons](https://github.com/poblabs/weewx-belchertown)
* National Oceanic and Atmospheric Administration for creating the WMO symbol files and releasing them to the public domain

## Links

* [Present weather](https://github.com/roe-dl/weathericons)
