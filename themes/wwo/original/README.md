# Original Theme

## License and Usage

No specific licence, provided freely by World Weather Online. Can be used for any purpose.

These icons are provided by the API in the JSON payload.
This theme is the same as original theme of WeatherAPI.

## Credits

See OpenWeather terms of use.

## Links

* [Weather Icons and Conditions](https://www.worldweatheronline.com/weather-api/api/docs/weather-icons.aspx)
