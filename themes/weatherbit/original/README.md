# Original Theme

## License and Usage

No specific licence, provided freely by Weatherbit.io. Can be used for any purpose.

These icons are also provided by the API in the JSON payload.
For example, URL to t01d icon is : [https://cdn.weatherbit.io/static/img/icons/t01d.png](https://cdn.weatherbit.io/static/img/icons/t01d.png).

## Credits

See OpenWeather terms of use.

## Links

* [Weather API Codes/Icons](https://www.weatherbit.io/api/codes)
