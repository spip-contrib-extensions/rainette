<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Inserer la CSS par défaut pour styler les modèles par défaut de Rainette.
 *
 * @pipeline insert_head_css
 *
 * @param string $flux Code html des styles CSS à charger
 *
 * @return string Code html complété.
 */
function rainette_insert_head_css(string $flux) : string {
	static $done = false;
	if (!$done) {
		$done = true;
		$flux .= '<link rel="stylesheet" href="' . find_in_path('rainette.css') . '" type="text/css" media="all" />';
	}

	return $flux;
}

/**
 * Permet d’ajouter des contenus dans la partie `<head>` des pages de l’espace public.
 * Pour le plugin, il permet de charger les CSS.
 *
 * @pipeline insert_head
 *
 * @param string $flux Contenu HTML du header
 *
 * @return string Code html complété.
 */
function rainette_insert_head(string $flux) : string {
	// au cas ou il n'est pas implemente
	$flux .= rainette_insert_head_css($flux);

	return $flux;
}
