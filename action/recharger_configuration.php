<?php
/**
 * Ce fichier contient l'action `recharger_configuration` lancée par un utilisateur pour
 * recharger la configuration technique de Rainette et celle des services.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur de recharger la configuration technique de Rainette et celle des services
 * à partir de leur fichier YAML.
 *
 * Cette action est réservée aux utilisateurs pouvant accéder à la configuration.
 *
 * @return void
 */
function action_recharger_configuration_dist() : void {
	// Sécurisation.
	// -- Aucun argument attendu.

	// Verification des autorisations : pour recharger laconfiguration il suffit
	// d'avoir l'autorisation "configurer".
	if (!autoriser('configurer')) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Rechargement de touttes les configurations statiques de Rainette et des services.
	include_spip('rainette_administrations');
	rainette_configurer();
}
