<?php
/**
 * Ce fichier contient l'action `effacer_parametrage` lancée par un utilisateur pour
 * effacer le paramétrage utilisateur d'un service donné.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur d'effacer le paramétrage utioisateur d'un service.
 *
 * Cette action est réservée aux utilisateurs pouvant accéder à la configuration.
 * Elle ne nécessite comme argument uniquement l'id du service.
 *
 * @return void
 */
function action_effacer_parametrage_dist() : void {
	// Sécurisation.
	// L'argument attendu est l'id du service concerné.
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$service = $securiser_action();

	// Verification des autorisations : pour recharger laconfiguration il suffit
	// d'avoir l'autorisation "configurer".
	if (!autoriser('configurer')) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Rechargement de touttes les configurations statiques de Rainette et des services.
	include_spip('inc/config');
	effacer_config("rainette/{$service}");
}
